﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace FIMS.Modules
{
    /// <summary>
    /// Java진영에서 쓰이는 Properties를 읽고 쓸 수 있습니다.
    /// 아래 코드는 대부분 java 코드를 그대로 변형시킨것입니다.
    /// </summary>
    public class Properties : IEnumerable<KeyValuePair<string, string>>
    {
        static Encoding PROPERTIES_ENCODING;
        static Properties()
        {
            PROPERTIES_ENCODING = System.Text.Encoding.GetEncoding("iso-8859-1");
        }

        Dictionary<string, string> _datas;
        int loadedBufferSize = 1024;

        public Properties() { _datas = new Dictionary<string, string>(); }
        public Properties(string path) : this()
        {
            Load(path);
        }

        public void Load(string path)
        {
            if (!File.Exists(path))
            {
                Console.WriteLine($"[Properties Load] File Not Found '{path}'");
                return;
            }

            using (FileStream fs = FileHelper.OpenRead(path))
            {
                Load(fs);
            }
        }

        #region Load Helpers
        public void Load(Stream stream)
        {
            char[] convtBuf = new char[1024];
            int limit;
            int keyLen;
            int valueStart;
            char c;
            bool hasSep;
            bool precedingBackslash;
            string line;

            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
            {
                loadedBufferSize = 0;

                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    limit = line.Length;
                    loadedBufferSize += limit + 2;

                    c = (char)0;
                    keyLen = 0;
                    valueStart = limit;
                    hasSep = false;

                    //System.out.println("line=<" + new String(lineBuf, 0, limit) + ">");
                    precedingBackslash = false;
                    while (keyLen < limit)
                    {
                        c = line[keyLen];
                        //need check if escaped.
                        if ((c == '=' || c == ':') && !precedingBackslash)
                        {
                            valueStart = keyLen + 1;
                            hasSep = true;
                            break;
                        }
                        else if ((c == ' ' || c == '\t' || c == '\f') && !precedingBackslash)
                        {
                            valueStart = keyLen + 1;
                            break;
                        }
                        if (c == '\\')
                        {
                            precedingBackslash = !precedingBackslash;
                        }
                        else
                        {
                            precedingBackslash = false;
                        }
                        keyLen++;
                    }
                    while (valueStart < limit)
                    {
                        c = line[valueStart];
                        if (c != ' ' && c != '\t' && c != '\f')
                        {
                            if (!hasSep && (c == '=' || c == ':'))
                            {
                                hasSep = true;
                            }
                            else
                            {
                                break;
                            }
                        }
                        valueStart++;
                    }
                    String key = loadConvert(line, 0, keyLen, convtBuf);
                    String value = loadConvert(line, valueStart, limit - valueStart, convtBuf);

                    if (key.Length > 0 && key[0] != '#')
                    {
                        SetString(key, value);
                    }
                }
            }

        }
        private String loadConvert(string inarr, int off, int len, char[] convtBuf)
        {
            if (convtBuf.Length < len)
            {
                int newLen = len * 2;
                if (newLen < 0)
                {
                    newLen = int.MaxValue;
                }
                convtBuf = new char[newLen];
            }
            char aChar;
            char[] outarr = convtBuf;
            int outLen = 0;
            int end = off + len;

            while (off < end)
            {
                aChar = inarr[off++];
                if (aChar == '\\')
                {
                    aChar = inarr[off++];
                    if (aChar == 'u')
                    {
                        // Read the xxxx
                        int value = 0;
                        for (int i = 0; i < 4; i++)
                        {
                            aChar = inarr[off++];
                            switch (aChar)
                            {
                                case '0':
                                case '1':
                                case '2':
                                case '3':
                                case '4':
                                case '5':
                                case '6':
                                case '7':
                                case '8':
                                case '9':
                                    value = (value << 4) + aChar - '0';
                                    break;
                                case 'a':
                                case 'b':
                                case 'c':
                                case 'd':
                                case 'e':
                                case 'f':
                                    value = (value << 4) + 10 + aChar - 'a';
                                    break;
                                case 'A':
                                case 'B':
                                case 'C':
                                case 'D':
                                case 'E':
                                case 'F':
                                    value = (value << 4) + 10 + aChar - 'A';
                                    break;
                                default:
                                    throw new System.Exception("Malformed \\uxxxx encoding.");
                            }
                        }
                        outarr[outLen++] = (char)value;
                    }
                    else
                    {
                        if (aChar == 't')
                            aChar = '\t';
                        else if (aChar == 'r')
                            aChar = '\r';
                        else if (aChar == 'n')
                            aChar = '\n';
                        else if (aChar == 'f')
                            aChar = '\f';
                        outarr[outLen++] = aChar;
                    }
                }
                else
                {
                    outarr[outLen++] = aChar;
                }
            }
            return new string(outarr, 0, outLen);
        }
        #endregion Load Helpers

        public string SaveToString(string comment = "")
        {
            StringBuilder outBuff = new StringBuilder(loadedBufferSize);
            if (!comment.IsEmpty())
            {
                using (StringReader tr = new StringReader(comment))
                {
                    string line;
                    while ((line = tr.ReadLine()) != null)
                    {
                        if (line.IsEmpty())
                            continue;

                        outBuff.Append("#");
                        writeConvert(outBuff, line, false, true);
                        outBuff.AppendLine();
                    }
                }
            }

            outBuff.AppendLine("#" + DateTime.Now.ToString());
            for (var e = GetEnumerator(); e.MoveNext();)
            {
                writeConvert(outBuff, e.Current.Key, true, true);
                outBuff.Append('=');
                writeConvert(outBuff, e.Current.Value, true, true);
                outBuff.AppendLine();
            }

            return outBuff.ToString();
        }
        private String writeConvert(StringBuilder outBuffer, String theString, bool escapeSpace, bool escapeUnicode)
        {
            int len = theString.Length;
            int bufLen = len * 2;
            if (bufLen < 0)
            {
                bufLen = int.MaxValue;
            }

            for (int x = 0; x < len; x++)
            {
                char aChar = theString[x];
                // Handle common case first, selecting largest block that
                // avoids the specials below
                if ((aChar > 61) && (aChar < 127))
                {
                    if (aChar == '\\')
                    {
                        outBuffer.Append('\\');
                        outBuffer.Append('\\');
                        continue;
                    }
                    outBuffer.Append(aChar);
                    continue;
                }
                switch (aChar)
                {
                    case ' ':
                        if (x == 0 || escapeSpace)
                            outBuffer.Append('\\');
                        outBuffer.Append(' ');
                        break;
                    case '\t':
                        outBuffer.Append('\\');
                        outBuffer.Append('t');
                        break;
                    case '\n':
                        outBuffer.Append('\\');
                        outBuffer.Append('n');
                        break;
                    case '\r':
                        outBuffer.Append('\\');
                        outBuffer.Append('r');
                        break;
                    case '\f':
                        outBuffer.Append('\\');
                        outBuffer.Append('f');
                        break;
                    case '=': // Fall through
                    case ':': // Fall through
                    case '#': // Fall through
                    case '!':
                        outBuffer.Append('\\');
                        outBuffer.Append(aChar);
                        break;
                    default:
                        if (((aChar < 0x0020) || (aChar > 0x007e)) & escapeUnicode)
                        {
                            outBuffer.Append('\\');
                            outBuffer.Append('u');
                            outBuffer.Append(toHex((aChar >> 12) & 0xF));
                            outBuffer.Append(toHex((aChar >> 8) & 0xF));
                            outBuffer.Append(toHex((aChar >> 4) & 0xF));
                            outBuffer.Append(toHex(aChar & 0xF));
                        }
                        else
                        {
                            outBuffer.Append(aChar);
                        }
                        break;
                }
            }
            return outBuffer.ToString();
        }
        private static char toHex(int nibble)
        {
            const string hexDigit = "0123456789ABCDEF";
            return hexDigit[(nibble & 0xF)];
        }

        public void LoadToXML(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlElement root = doc.DocumentElement;
            XmlNodeList nodes = root.ChildNodes;

            for (int i = 0; i < nodes.Count; i++)
            {
                XmlNode node = nodes[i];
                SetString(node.Name, node.InnerText);
            }
        }
        public void SaveToXML(string path)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement properties = doc.CreateElement("Properties");

            var e = GetEnumerator();
            while (e.MoveNext())
            {
                var pair = e.Current;

                var elem = doc.CreateElement(pair.Key);
                elem.InnerText = pair.Value;
                properties.AppendChild(elem);
            }

            doc.AppendChild(properties);
            doc.Save(path);
        }

        public string this[string key]
        {
            get
            {
                return GetString(key);
            }
            set
            {
                SetString(key, value);
            }
        }
        public bool Contains(string key) { return _datas.ContainsKey(key); }
        public string GetString(string key) { return _datas[key]; }
        public string GetString(string key, string def = "") { string result; if (_datas.TryGetValue(key, out result)) return _datas[key]; else return def; }
        public void SetString(string key, string value) { _datas.Put(key, value); }
        public int GetInt(string key, int def = 0) { return _datas.GetInt(key, def); }
        public void SetInt(string key, int value) { SetString(key, value.ToString()); }
        public float GetFloat(string key, float def = 0) { return _datas.GetFloat(key, def); }
        public void SetFloat(string key, float value) { SetString(key, value.ToString()); }
        public bool GetBool(string key, bool def = false) { return _datas.GetInt(key, def ? 1 : 0) == 1; }
        public void SetBool(string key, bool value) { SetString(key, value ? "1" : "0"); }


        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _datas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _datas.GetEnumerator();
        }
    }
}