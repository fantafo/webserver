﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FIMS.Modules.ADB;

namespace FIMS.Modules
{
    public class ADBHelper
    {
        /*
        public static async void Connect(string address, Action<bool> completeCallback)
        {
            var reader = ADB.ExecuteReader($"connect {address}");
            string txt = await reader.ReadToEndAsync();

            bool isConnected = txt.StartsWith("connected", StringComparison.OrdinalIgnoreCase);
            completeCallback?.Invoke(isConnected);
        }
        public static async void MarkWiressADB(string address,  Action<DeviceConnectionState> resultCallback, Device device)
        {
            bool pass = false;

            // 얼마나 이전에 접속이 완료됐는지를 검사한다.
            // 10초 이내에 재접속한경우에 Wifi 마킹을 하지 않는다.
            {
                var reader = ADB.ExecuteReader($"-s {address} shell cat {REGIST_TIME_PATH}");
                var txt = (await reader.ReadToEndAsync()).Trim();
                Console.WriteLine(txt);

                // Wireless가 활성화된 적이 있으면서, 활성화된지 REGIST_TIMING(10초)가 지났다면 다시 활성화시키고
                // 지나지 않았다면 여기서 마킹을 멈춘다.
                long tick;
                if (long.TryParse(txt, out tick))
                {
                    if ((DateTime.Now - new DateTime(tick)).TotalSeconds < REREGIST_TIMING)
                    {
                        pass = true;
                    }
                }
            }

            // 인증을 시도한다.
            if (!pass)
            {
                // 현재 틱을 기록한다.
                Execute($"-s {address} shell \"echo '{DateTime.Now.Ticks}' > {REGIST_TIME_PATH}\"");

                // 인증 요청
                var reader = ADB.ExecuteReader($"-s {address} tcpip {REMOTE_PORT_S}");
                var txt = (await reader.ReadToEndAsync()).Trim();
            }
        }

        //public static async void ReadDevices()
        //{
        //    var reader = ExecuteReader("devices -l", false);

        //    // 모든 디바이스를 종료해제시켜둔다.
        //    foreach (var d in Device.List)
        //        d.IsConnected = false;

        //    while (!reader.EndOfStream)
        //    {
        //        string line = await reader.ReadLineAsync();
        //        if (line != null)
        //        {
        //            line = line.Trim();
        //            if (line.Length > 5 && line != "List of devices attached")
        //            {
        //                var datas = line.Split(new[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

        //                string deviceID = datas[0];
        //                Device device = Device.GetDevice(deviceID);
        //                bool isNew = false;
        //                if (device == null)
        //                {
        //                    device = new Device();
        //                    device.DeviceID = deviceID;
        //                    isNew = true;
        //                }
        //                for (int i = 0; i < datas.Length; i++)
        //                {
        //                    if (datas[i].Equals("host", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        device.Reconnect();
        //                        isNew = false;
        //                    }
        //                    else if (datas[i].Equals("device", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        device.IsConnected = true;
        //                    }
        //                    else if (datas[i].Equals("offline", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        device.Disconnect();
        //                        isNew = false;
        //                    }
        //                    else if (datas[i].StartsWith("product:"))
        //                    {
        //                        device.Product = datas[i].Substring("product:".Length);
        //                    }
        //                    else if (datas[i].StartsWith("model:"))
        //                    {
        //                        device.Model = datas[i].Substring("model:".Length);
        //                    }
        //                }

        //                if (isNew)
        //                {
        //                    Device.Add(device);
        //                    if (OnDeviceConnect != null)
        //                        OnDeviceConnect(device);
        //                }
        //            }
        //        }
        //    }

        //    for (int i = 0; i < Device.List.Count; i++)
        //    {
        //        var d = Device.List[i];
        //        if (!d.IsConnected)
        //        {
        //            Device.List.RemoveAt(i--);
        //            if (OnDeviceDisconnect != null)
        //                OnDeviceDisconnect(d);
        //        }
        //    }

        //    for (int i = 0; i < Device.List.Count; i++)
        //    {
        //        var d = Device.List[i];
        //        if (d.DeviceID.Contains("mulator"))
        //        {
        //            d.Reconnect();
        //        }
        //    }
        //}

        public static async void InstallAPK(string address, string file, Action<bool> callback)
        {
            var p = ExecuteProcess($"-s {address} install -r \"{file}\"");

            while (!p.HasExited)
                await Task.Delay(1000);
            bool result = false;

            string outTxt = (p.StandardOutput.ReadToEnd()).Trim();
            result |= outTxt.Equals("Success", StringComparison.OrdinalIgnoreCase);

            outTxt = (p.StandardError.ReadToEnd()).Trim();
            result |= outTxt.Equals("Success", StringComparison.OrdinalIgnoreCase);

            callback?.Invoke(result);
        }*/
    }
}
