﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIMS.Modules
{
    public class ADB
    {
        public const int REMOTE_PORT = 7777;
        public const string REMOTE_PORT_S = "7777";
        public const string REMOTE_PORT_SQ = ":" + REMOTE_PORT_S;

        public const string FANTAFO_PATH = "/sdcard/Fantafo/";
        public const string REGIST_TIME_PATH = FANTAFO_PATH + "RegistTime";
        public const int REREGIST_TIMING = 10;

        public static string ANDROID_SDK = Environment.GetEnvironmentVariable("ANDROID_SDK");
        public static string ADB_PATH = Environment.GetEnvironmentVariable("ANDROID_SDK") + @"\platform-tools\adb.exe";

        public static void ExecuteWait(string query)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = ADB_PATH,
                Arguments = query,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            using (Process p = new Process { StartInfo = psi })
            {
                p.Start();
                p.WaitForExit();
            }
        }
        public static StreamReader ExecuteReader(string query)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = ADB_PATH,
                Arguments = query,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            Process p = new Process { StartInfo = psi };
            if (p.Start())
            {
                return p.StandardOutput;
            }
            else
            {
                Console.WriteLine("알 수 없는 이유로 ADB를 실행하지 못했습니다. :" + p.ExitCode);
                return null;
            }
        }
        public static string ExecuteRead(string query)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = ADB_PATH,
                Arguments = query,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            using (Process p = new Process { StartInfo = psi })
            {
                if (p.Start())
                {
                    p.WaitForExit();
                    return p.StandardOutput.ReadToEnd();
                }
                else
                {
                    Console.WriteLine("알 수 없는 이유로 ADB를 실행하지 못했습니다. :" + p.ExitCode);
                    return null;
                }
            }
        }
        public static Process ExecuteProcess(string query)
        {
            ProcessStartInfo psi = new ProcessStartInfo
            {
                FileName = ADB_PATH,
                Arguments = query,
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            Process p = new Process { StartInfo = psi };
            if (p.Start())
            {
                return p;
            }
            else
            {
                Console.WriteLine("알 수 없는 이유로 ADB를 실행하지 못했습니다. :" + p.ExitCode);
                return null;
            }
        }
    }
}
