﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

public static class ArrayExtension
{

    public static T[] Add<T>(this T[] array, T value)
    {
        System.Array.Resize(ref array, array.Length + 1);
        array[array.Length - 1] = value;
        return array;
    }

    public static T[] AddRange<T>(this T[] array, T[] dest)
    {
        System.Array.Resize(ref array, array.Length + dest.Length);
        System.Array.Copy(dest, 0, array, array.Length, dest.Length);
        return array;
    }

    public static T[] Insert<T>(this T[] array, int index, T value)
    {
        T[] result = new T[array.Length + 1];
        System.Array.Copy(array, 0, result, 0, index);
        int len = array.Length - index;
        if (len > 0) System.Array.Copy(array, index, result, index + 1, len);
        result[index] = value;

        return result;
    }

    public static T[] InsertRange<T>(this T[] array, int index, T[] dest)
    {
        T[] result = new T[array.Length + dest.Length];
        System.Array.Copy(array, 0, result, 0, index);
        System.Array.Copy(dest, 0, result, index, dest.Length);
        int len = result.Length - dest.Length - index;
        if (len > 0) System.Array.Copy(array, index, result, index + dest.Length, len);
        return result;
    }

    public static T[] Remove<T>(this T[] array, T target)
        where T : class
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == target)
            {
                return array.RemoveAt(i);
            }
        }
        return array;
    }

    public static T[] Remove<T>(this T[] array, T target, out bool result)
        where T : class
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == target)
            {
                result = true;
                return array.RemoveAt(i);
            }
        }
        result = false;
        return array;
    }

    public static T[] RemoveAt<T>(this T[] array, int index)
    {
        T[] result = new T[array.Length - 1];
        System.Array.Copy(array, 0, result, 0, index);
        int len = array.Length - index - 1;
        if (len > 0) System.Array.Copy(array, index + 1, result, index, len);
        return result;
    }

    public static T[] RemoveRange<T>(this T[] array, int index, int count)
    {
        T[] result = new T[array.Length - count];
        System.Array.Copy(array, 0, result, 0, index);
        int len = array.Length - index - count - 1;
        if (len > 0) System.Array.Copy(array, index + count, result, index, len);
        return result;
    }

    public static T TryGet<T>(this T[] src, int index)
        where T : class
    {
        return src.TryGet(index, null);
    }

    public static T TryGet<T>(this T[] src, int index, T def)
    {
        if (src != null && index >= 0 && index < src.Length)
        {
            return src[index];
        }
        else
        {
            return def;
        }
    }

    public static int ReferenceIndexOf<T>(this T[] array, T target)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (ReferenceEquals(array[i], target))
            {
                return i;
            }
        }
        return -1;
    }

    public static T[] Reverse<T>(this T[] array)
    {
        System.Array.Reverse(array);
        return array;
    }

    public static T[] Set<T>(this T[] array, T value)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = value;
        }
        return array;
    }

    public static int IndexOf<T>(this T[] array, T value)
    {
        return System.Array.IndexOf(array, value);
    }

    public static int IndexOf<T>(this T[] array, T value, int startIndex)
    {
        return System.Array.IndexOf(array, value, startIndex);
    }

    public static int IndexOf<T>(this T[] array, T value, int startIndex, int count)
    {
        return System.Array.IndexOf(array, value, startIndex, count);
    }

    public static int LastIndexOf<T>(this T[] array, T value)
    {
        return System.Array.LastIndexOf(array, value);
    }

    public static int LastIndexOf<T>(this T[] array, T value, int startIndex)
    {
        return System.Array.LastIndexOf(array, value);
    }

    public static int LastIndexOf<T>(this T[] array, T value, int startIndex, int count)
    {
        return System.Array.LastIndexOf(array, value);
    }
}
