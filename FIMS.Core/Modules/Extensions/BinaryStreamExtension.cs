﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

public static class BinaryStreamExtension
{
    public static void WriteB(this BinaryWriter self, bool value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, byte value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, int value) { self.Write((byte)value); }
    public static void WriteH(this BinaryWriter self, short value) { self.Write(value); }
    public static void WriteH(this BinaryWriter self, int value) { self.Write((short)value); }
    public static void WriteD(this BinaryWriter self, int value) { self.Write(value); }
    public static void WriteL(this BinaryWriter self, long value) { self.Write(value); }
    public static void WriteF(this BinaryWriter self, float value) { self.Write(value); }
    public static void WriteS(this BinaryWriter self, string value) { self.Write(value); }

    public static bool ReadB(this BinaryReader self) { return self.ReadBoolean(); }
    public static byte ReadC(this BinaryReader self) { return self.ReadByte(); }
    public static short ReadH(this BinaryReader self) { return self.ReadInt16(); }
    public static int ReadD(this BinaryReader self) { return self.ReadInt32(); }
    public static long ReadL(this BinaryReader self) { return self.ReadInt64(); }
    public static float ReadF(this BinaryReader self) { return self.ReadSingle(); }
    public static string ReadS(this BinaryReader self) { return self.ReadString(); }
}
