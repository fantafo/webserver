﻿using log4net;
using NHibernate;
using NHibernate.Cfg;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIMS
{
    /// <summary>
    /// DB 세션을 열기 위해 필요한 유틸입니다.
    /// Database.Open()으로 세션을 열 수 있습니다.
    /// </summary>
    public class Database
    {
        public static Configuration configuration;
        public static ISessionFactory sessionFactory;
        static ILog log = LogManager.GetLogger(typeof(Database).Name);

        public static void Initialize()
        {
            if (sessionFactory == null || sessionFactory.IsClosed)
            {
                var starttime = DateTime.Now;

                // Setup Configuration
                configuration = new Configuration();
                configuration.Configure(Fims.CONFIG_PATH + "/hibernate.cfg.xml");

                // Mapping
                configuration.AddFile(Fims.CONFIG_PATH + "/hibernate.hbm.xml");
                sessionFactory = configuration.BuildSessionFactory();

                log.Info($"Initialized   ({(DateTime.Now - starttime).TotalMilliseconds:f0}ms)");
            }
        }

        public static ISession Open()
        {
            Initialize();
            return sessionFactory.OpenSession();
        }
    }
}
