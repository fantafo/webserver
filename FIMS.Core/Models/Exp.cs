﻿using System;
using FIMS.Modules;
using NHibernate.Collection.Generic;
using FIMS.Manager;
using log4net;

namespace FIMS
{
    /// <summary>
    /// FIMS가 게임용으로 제작됐을 때, 유저의 경험치를 판별할
    /// 데이터 테이블을 위해 제작됐다.
    /// </summary>
    public class Exp
    {
        private static ILog log = LogManager.GetLogger("Exp");
        private static int[] exps;

        public static void Load()
        {
            using (var db = Database.Open())
            {
                var list = db.QueryOver<Exp>().List();
                exps = new int[list.Count * 2];

                foreach (var exp in list)
                {
                    exps[exp.Level] = exp.Val;
                }
                log.Debug($"Load Data - Exp ({list.Count})");
            }
        }
        public static int Get(int level)
        {
            return exps[level];
        }

        public virtual short Level { get; set; }
        public virtual int Val { get; set; }
    }
}
