﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using NHibernate.Collection.Generic;

namespace FIMS
{
    public class GuestName
    {
        //db 값읽기위한 변수
        public virtual int SN { get; set; }
        public virtual string Name { get; set; }
        public virtual GenderType Gender { get; set; }
    }
}
