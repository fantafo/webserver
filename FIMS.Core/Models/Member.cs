﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using NHibernate.Collection.Generic;

namespace FIMS
{
    public enum GenderType : byte
    {
        Male = 0,
        Female = 1
    }

    public class Member
    {
        public static string ConvertPassword(string text)
        {
            const string SALT = "aslkrj32s#@RSDVZXA#!@";
            byte[] bytes = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(SALT + text));
            return BitConverter.ToString(bytes);
        }
        
        //랜덤
        static public Random random = new Random();
        static private IList<GuestName> setGuestNames;
        //게스트 닉네임
        static public IList<GuestName> guestNames
        {
            get
            {
                if(setGuestNames.Count < 0)
                {
                    using (var db = Database.Open())
                    {
                        setGuestNames = GuestNameDao.Get.ToList();
                    }
                }
                return setGuestNames;
            }
        }

        public virtual long SN { get; set; }
        public virtual string Password { get; set; }
        public virtual string Name { get; set; } = "";
        public virtual string NickName { get; set; }
        public virtual string Mobile { get; set; }
        public virtual string Email { get; set; }
        public virtual short Level { get; set; } = 1;
        public virtual int Exp { get; set; }
        public virtual GenderType Gender { get; set; } = 0;
        public virtual DateTime RegistDate { get; set; }
        public virtual DateTime LastConnectDate { get; set; }
        public virtual short AvatarHead { get; set; } = 1;
        public virtual short AvatarBody { get; set; } = 1;
        public virtual IDictionary<string, string> Attributes { get; set; }
        public virtual IList<Coupon> Coupons { get; set; }
        public virtual int PlayTime { get; set; }
        public virtual int PlayCount { get; set; }
        public virtual int TotalScore { get; set; }
        public virtual int TopScore { get; set; }
        public virtual string Id { get; set; }
        public virtual bool Login { get; set; }
        public virtual string Major { get; set; }
        public virtual string Professor { get; set; }
        public virtual IList<Completion> Completions { get; set; }

        public virtual int CalcAverageScore => PlayCount == 0 ? 0 : (TotalScore / PlayCount);
        //public virtual string ToExpPercent => (Exp * 100) / FIMS.Exp.Get(Level) + "%";
        public virtual string ToPlayTime
        {
            get
            {
                // 60초 미만인경우
                if(PlayTime < 60)
                {
                    return "0분";
                }
                else if (PlayTime < 3600)
                {
                    return $"{PlayTime / 60}분 {PlayTime % 60:00}초";
                }
                else
                {
                    return $"{PlayTime / 3600}시간 {(PlayTime % 3600)/60:00}분";
                }
            }
        }

        public override string ToString()
            => $"Member(SN:{SN}/PW:{Password}/Name:{Name}/Gender:{Gender}/Email:{Email}/Mobile:{Mobile}/Regist:{RegistDate::yyyy-MM-dd})";
        public virtual object ToSerializeObject(bool fast = false, string game = "")
        {
            if (fast)
            {
                int index = random.Next(0, guestNames.Count);
                Name = guestNames[index].Name;
                Gender = guestNames[index].Gender;
                switch (game)
                {
                    case "Safety":
                        if (Gender == GenderType.Male)
                        {
                            AvatarHead = 3;
                            AvatarBody = 1;
                        }
                        else if (Gender == GenderType.Female)
                        {
                            AvatarHead = 1;
                            AvatarBody = 2;
                        }
                        break;

                    case "Earth":
                    default:
                        if (Gender == GenderType.Male)
                        {
                            AvatarHead = 1;
                            AvatarBody = 1;
                        }
                        else if (Gender == GenderType.Female)
                        {
                            AvatarHead = 2;
                            AvatarBody = 2;
                        }
                        break;
                }
            }
            dynamic result = new
            {
                SN = SN,
                Email = Email,
                Level = Level,
                Exp = Exp,
                NickName = NickName,
                Name = Name,
                Gender = Gender,
                Mobile = Mobile,
                RegistDate = RegistDate,
                LastConnectDate = LastConnectDate,
                AvatarHead = AvatarHead,
                AvatarBody = AvatarBody,
                Id = Id,
                Login = Login,
                Major = Major,
                Professor = Professor
            };
            return result;
        }
        /*
        public virtual miniMember MiniToSerializeObject()
            => new miniMember
            {
                SN = SN,
                Id = Id,
                Name = Name,
                Major = Major,
                Professor = Professor
            };

        public class miniMember
        {
            public long SN;
            public string Id;
            public string Name;
            public string Major;
            public string Professor;
        };
        */
    }

    public class Coupon
    {
        public virtual string Name { get; set; }
        public virtual int Discount { get; set; }
        public virtual DateTime RegistDate { get; set; }
        public virtual DateTime UseDate { get; set; }
    }
    
    public class Completion
    {
        //public virtual Member member { get; set; }
        public virtual int Ord { get; set; }
        public virtual int Score { get; set; }
        public virtual DateTime Date { get; set; } = DateTime.Now;
        public virtual int PlayTime { get; set; }
        //public virtual int Ord { get; set; }
        //public virtual int MemberSN { get; set; }
        //public static implicit operator Member(Completion m) { return m.member; }
        //public static implicit operator Device(GameSessionUser m) { return m.device; }
        //public static implicit operator Completion(Member m) { return new Completion { member = m }; }
    }

    //mini는 테이블표에서 보여주기 위한 정보만 담기위해서
    public class miniMember
    {
        public long SN;
        public string Id;
        public string Name;
        public string Major;
        public string Professor;
        public string Gender;

        public static miniMember ToSerializeDetail(Member member)
        {
            /*
            string gender;
            if (member.Gender == GenderType.Male)
            {
                gender = "남성";
            }
            else if (member.Gender == GenderType.Female)
            {
                gender = "여성";
            }
            else
            {
                gender = "??";
            }
            */
            miniMember result = new miniMember
            {
                SN = member.SN,
                Id = member.Id,
                Name = member.Name,
                Major = member.Major,
                //Professor = member.Professor,
                //Gender = gender
            };
            return result;
        }
    }

    public class miniCompletion
    {
        public long SN;
        public string Id;
        public string Name;
        public string Major;
        //public string Professor;
        //public int Ord;
        public int PlayTime;
        public string Date;
        public int Score;

        public static miniCompletion ToSerializeDetail(Member member, Completion completion)
        {
            miniCompletion result = new miniCompletion
            {
                SN = member.SN,
                Id = member.Id,
                Name = member.Name,
                Major = member.Major,
                //Professor = member.Professor,
                //Ord = completion.Ord,
                PlayTime = completion.PlayTime,
                //Date = completion.Date,
                Date = completion.Date.ToString("yyyy-MM-dd HH:mm"),
                Score = completion.Score
            };
            return result;
        }
    }

    public class miniStats
    {
        public string Major;
        public int Total;
        public int Completion;
        public string Percent;
    }

    //수료증 출력에 들어가는 문구
    public enum Certificate
    {
        Major =0,
        Id =1,
        Name,
        Date,
        Today
    }



}
