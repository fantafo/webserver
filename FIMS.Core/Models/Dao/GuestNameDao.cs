﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using System;

namespace FIMS
{
    public struct GuestNameDao
    {
        //public static GuestNameDao Get(ISession sess) => new GuestNameDao { session = sess };

        public static GuestNameDao Get => new GuestNameDao();

        private ISession session;

        //게스트 이름은 변경이 없기 때문에 메모리에 저장
        private List<GuestName> guestNameList
        {
            get
            {
                if (_guestNameList == null)
                {
                    using (var db = Database.Open())
                    {
                        session = db;
                        _guestNameList = new List<GuestName>(session.CreateCriteria<GuestName>().List<GuestName>());
                    }
                }
                return _guestNameList;
            }
        }
        private static List<GuestName> _guestNameList;

        public IList<GuestName> ToList()
            => guestNameList;
        // => session.CreateCriteria<GuestName>()
        //     .List<GuestName>();

        //GenderType에 따라 모든 게스트 이름을 가져온다.
        public IList<GuestName> ToList(GenderType gender)
            => guestNameList.FindAll(x => x.Gender == gender);
        //=> session.CreateCriteria<GuestName>()
        //    .Add(Expression.Eq("Gender", gender))
        //    .List<GuestName>();

        //게스트의 이름으로 게스트 정보를 찾는다.
        public IList<GuestName> FindName(string name)
            => guestNameList.FindAll(x => x.Name == name);
        //=> session.CreateCriteria<GuestName>()
        //        .Add(Expression.Eq("Name", name))
        //        //.UniqueResult<GuestName>();
        //        .List<GuestName>();
    }
}
