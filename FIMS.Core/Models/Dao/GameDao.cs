﻿using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;

namespace FIMS
{
    public struct GameDao
    {
        public static GameDao Get(ISession sess) => new GameDao { session = sess };
        private ISession session;


        /// <summary>
        /// 게임을 가져옵니다.
        /// </summary>
        public Game Get(int SN)
            => session.Get<Game>(SN);

        /// <summary>
        /// 게임이름으로 검색합니다.
        /// </summary>
        public Game GetByName(string name)
            => session.CreateCriteria<Game>()
                .Add(Expression.Eq("Name", name))
                .UniqueResult<Game>();

        /// <summary>
        /// 게임이름으로 검색합니다.
        /// </summary>
        public Game GetByPackageName(string pkgName)
            => session.CreateCriteria<Game>()
                .Add(Expression.Eq("PackageName", pkgName))
                .UniqueResult<Game>();

        /// <summary>
        /// CommonServer를 사용하고있는 게임 목록을 가져옵니다.
        /// </summary>
        public IList<Game> GetUseCommon()
            => session.CreateCriteria<Game>()
                .Add(Expression.Eq("Enabled", true))
                .Add(Expression.Eq("IsCommon", true))
                .List<Game>();

        public IList<Game> ToList()
            => session.CreateCriteria<Game>()
                .AddOrder(Order.Desc("SN"))
                .List<Game>();

        public IList<Game> ToList(int skip, int size)
            => session.CreateCriteria<Game>()
                .AddOrder(Order.Desc("SN"))
                .SetFirstResult(skip)
                .SetMaxResults(size)
                .List<Game>();

        public void Update(Game Game)
        {
            session.SaveOrUpdate(Game);
        }
        public void Save(Game Game)
        {
            session.Save(Game);
        }
    }
}
