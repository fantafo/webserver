﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using System;

namespace FIMS
{
    public struct GameSessionDao
    {
        public static GameSessionDao Get(ISession sess) => new GameSessionDao { session = sess };
        private ISession session;

        /// <summary>
        /// 세션을 가져옵니다.
        /// </summary>
        public GameSession Get(long SN)
            => session.Get<GameSession>(SN);

        public void Update(GameSession sess)
        {
            session.SaveOrUpdate(sess);
        }
        public void Save(GameSession sess)
        {
            session.Save(sess);
        }

        /// <summary>
        /// 전체 세션을 가져옵니다.
        /// </summary>
        public IList<GameSession> ToList()
            => session.QueryOver<GameSession>()
                .List<GameSession>();

        /// <summary>
        /// 세션 중 일부를 가져옵니다.
        /// </summary>
        public IList<GameSession> ToList(int skip, int size)
            => session.CreateCriteria<GameSession>()
                .AddOrder(Order.Desc("sn"))
                .SetFirstResult(skip)
                .SetMaxResults(size)
                .List<GameSession>();

        /// <summary>
        /// 반납되지 않은 세션들을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public IList<GameSession> ToActiveList()
            => session.CreateCriteria<GameSession>()
                .Add(Expression.Not(Expression.Eq("State", SessionState.Returned)))
                .List<GameSession>();

        /// <summary>
        /// 할당중인 세션을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public IList<GameSession> ToAllocatingList()
            => session.CreateCriteria<GameSession>()
                .Add(Expression.Eq("State", SessionState.Allocating))
                .List<GameSession>();

        //SessionState.GameLaunch 세션을 찾는다
        public IList<GameSession> ToGameLaunchList()
           => session.CreateCriteria<GameSession>()
               .Add(Expression.Eq("State", SessionState.GameLaunch))
               .List<GameSession>();

        //로그인이 모두 완료된 세션을 가져옵니다.
        public IList<GameSession> ToGameReadyList()
            => session.CreateCriteria<GameSession>()
                .Add(Expression.Eq("State", SessionState.GameReady))
                .List<GameSession>();


        /// <summary>
        /// 게임중인 세션을 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public IList<GameSession> ToRunningList()
            => session.CreateCriteria<GameSession>()
                .Add(Expression.Ge("State", SessionState.GameLaunch))
                .Add(Expression.Le("State", SessionState.GameRun))
                .List<GameSession>();


        /// <summary>
        /// 반환되지 않은 세션들 중에서 deviceID에 해당하는 세션을 찾아줍니다.
        /// </summary>
        public GameSession FindCurrentSession(int deviceID)
        {
            var s = session.CreateCriteria<GameSession>("gs")
                .Add(Expression.Not(Expression.Eq("State", SessionState.Returned)))
                .AddOrder(Order.Desc("RegistDate"))
                .CreateCriteria("gs.members", NHibernate.SqlCommand.JoinType.InnerJoin)
                .Add(Expression.Eq("device.SN", deviceID))
                .List<GameSession>();

            if (s.Count > 0)
            {
                return s[0];
            }
            return null;
        }

        /// <summary>
        /// 반환되지 않았으며 할당이 끝난 세션을 가져옵니다.
        /// </summary>
        public GameSession FindCurrentLaunchedSession(int deviceID)
        {
            var s = session.CreateCriteria<GameSession>("gs")
                .Add(Expression.Ge("State", SessionState.Standby))
                .Add(Expression.Lt("State", SessionState.Returned))
                .AddOrder(Order.Desc("RegistDate"))
                .CreateCriteria("gs.members", NHibernate.SqlCommand.JoinType.InnerJoin)
                .Add(Expression.Eq("device.SN", deviceID))
                .List<GameSession>();

            if (s.Count > 0)
            {
                return s[0];
            }
            return null;
        }
    }
}
