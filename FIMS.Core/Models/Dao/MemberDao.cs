﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using System;
using NHibernate.Transform;

namespace FIMS
{
    public struct MemberDao
    {
        public static MemberDao Get(ISession sess) => new MemberDao { session = sess };
        private ISession session;
        
        /// <summary>
        /// 멤버를 가져옵니다.
        /// </summary>
        public Member Get(long SN)
            => SN > 0 ? session.Get<Member>(SN) : null;

        /// <summary>
        /// 이메일이 중복되는지 검사합니다.
        /// </summary>
        public bool ContainsByEmail(string email)
            => session.CreateCriteria<Member>()
                      .Add(Expression.Eq("Email", email))
                      .SetProjection(Projections.RowCount())
                      .UniqueResult<int>() > 0;

        /// <summary>
        /// 매니저상에서 유저를 추가할때 사용한다.
        /// </summary>
        public IList<Member> FindName(string name)
        {
            string find = "%" + name + "%";
            
            return session.CreateCriteria<Member>()
                      .Add(Expression.Or(Expression.Or(
                          Expression.Like("Name", find),
                          Expression.Like("NickName", find)),
                          Expression.Like("Mobile", find)))
                      .List<Member>();
                     
        }

        public Member FindID(string id)
            =>session.CreateCriteria<Member>()
                    .Add(Expression.Eq("Id", id))
                    .UniqueResult<Member>();

        /// <summary>
        /// 로그인할때 사용한다.
        /// </summary>
        public Member GetByLoginID(string id, string password)
            => session.CreateCriteria<Member>()
                      .Add(Expression.And(Expression.Eq("Password", password),
                          Expression.Or(Expression.Or(Expression.Or(
                          Expression.Eq("Email", id).IgnoreCase(),
                          Expression.Eq("Mobile", id).IgnoreCase()),
                          Expression.Eq("Name", id).IgnoreCase()),
                          Expression.Eq("NickName", id).IgnoreCase())))
                      .UniqueResult<Member>();


        public Member GameLoginID(string id)
            => session.CreateCriteria<Member>()
                    .Add(Expression.Eq("Id", id))
                    .Add(Expression.Eq("Login", false))
                    .UniqueResult<Member>();

        public void Update(Member member)
        {
            session.SaveOrUpdate(member);
        }
        public void Save(Member member)
        {
            session.Save(member);
        }

        public IList<Member> ToList()
            => session.CreateCriteria<Member>()
                .List<Member>();

        public IList<Member> ToList(int skip, int size)
            => session.CreateCriteria<Member>()
                .AddOrder(Order.Desc("sn"))
                .SetFirstResult(skip)
                .SetMaxResults(size)
                .List<Member>();
        
    }   
}
