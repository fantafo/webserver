﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using System;
using NHibernate.Transform;

namespace FIMS
{
    public struct AdminDao
    {
        public static AdminDao Get(ISession sess) => new AdminDao { session = sess };
        private ISession session;

        public Admin FindID(string id)
            => session.CreateCriteria<Admin>()
                    .Add(Expression.Eq("Id", id))
                    .UniqueResult<Admin>();

        public Admin FindPassword(string password)
             => session.CreateCriteria<Admin>()
                    .Add(Expression.Eq("Password", password))
                    .UniqueResult<Admin>();
        


        public IList<Admin> ToList()
            => session.CreateCriteria<Admin>()
                .List<Admin>();

    }
}
