﻿using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIMS
{
    public class UpdateFileInfoDao
    {
        public static UpdateFileInfoDao Get(ISession sess)
            => new UpdateFileInfoDao { session = sess };

        private ISession session;


        ///updatefileinfo 정보 가져오기
        public List<FileInfo> GetList()
            => session.CreateCriteria<FileInfo>()
                .List<FileInfo>().ToList<FileInfo>();

        //파일명으로 찾기
        public FileInfo GetByName(string name)
            => session.CreateCriteria<FileInfo>()
                .Add(Expression.Eq("FileName", name))
                .UniqueResult<FileInfo>();

        //업데이트가 true인 값가져오기
        public List<FileInfo> GetUpdateCheckList()
            => session.CreateCriteria<FileInfo>()
                .Add(Expression.Eq("UpdateCheck", true))
                .List<FileInfo>().ToList<FileInfo>();

        //모든 데이터 지우기
        public void Clear()
            => session.CreateQuery("delete from FileInfo")
                .ExecuteUpdate();
        
        
    }
}
