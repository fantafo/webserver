﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;

namespace FIMS
{
    public class CompletionDao
    {
        public static CompletionDao Get(ISession sess) => new CompletionDao { session = sess };
        private ISession session;

        public IList<Completion> ToList()
            => session.CreateCriteria<Completion>()
                .List<Completion>();
    }
}
