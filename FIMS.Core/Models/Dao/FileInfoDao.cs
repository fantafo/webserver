﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;

namespace FIMS
{
    //디비 저장x
    public struct FileInfoDao
    {
        public static FileInfoDao Get => new FileInfoDao();

        private ISession session;

        private List<FileInfo> FileInfoList
        {
            get
            {
                if (_fileInfoList == null)
                {
                    _fileInfoList = new List<FileInfo>();
                }
                return _fileInfoList;
            }
        }
        private static List<FileInfo> _fileInfoList;

        //fileinfolist 정보 가져오기
        public List<FileInfo> GetList()
            => FileInfoList;
        //=> session.CreateCriteria<FileInfo>()
        //   .List<FileInfo>();

        public void Add(FileInfo file)
        {
            if (FileInfoList != null)
                _fileInfoList.Add(file);
        }

        public void RemoveByName(string fileName)
        {
            if (FileInfoList != null && !fileName.IsEmpty() && GetByName(fileName) != null)
            {
                FileInfoList.Remove(GetByName(fileName));
            }
        }

        //public FileInfo GetBypkgName(string pkg)
        //    => session.CreateCriteria<FileInfo>()
        //        .Add(Expression.Eq("pkg", pkg))
        //        .UniqueResult<FileInfo>();

        //public FileInfo GetByName(string name)
        //    => session.CreateCriteria<FileInfo>()
        //        .Add(Expression.Eq("FileName", name))
        //        .UniqueResult<FileInfo>();

        public List<FileInfo> GetUpdateList()
            => FileInfoList.FindAll(x => x.UpdateCheck == true);
        //=> session.CreateCriteria<FileInfo>()
        //    .Add(Expression.Eq("UpdateCheck", true))
        //    .List<FileInfo>();

        public FileInfo GetByName(string name)
            => FileInfoList.Find(x => x.FileName.Equals(name));

        public void UpdateCheckChange(string FileName, bool updateCheck)
        {
            GetByName(FileName).UpdateCheck = updateCheck;
        }

        public void UpdateCategory(string FileName, FIMS.LargeCategory category)
        {
            GetByName(FileName).Category = category;
        }

        public void RemoveCheckChange(string FileName, bool Check)
        {
            GetByName(FileName).RemoveCheck = Check;
        }

        public void Clear()
        {
            FileInfoList.Clear();
        }

    }
}
