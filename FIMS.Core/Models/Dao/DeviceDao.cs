﻿using NHibernate;
using NHibernate.Criterion;
using System.Collections.Generic;
using System;
using log4net;

namespace FIMS
{
    public struct DeviceDao
    {
        //public static DeviceDao Get(ISession sess) => new DeviceDao { session = sess };

        public static DeviceDao Get => new DeviceDao();

        private ISession session;
        
        private List<Device> deviceList
        {
            get
            {
                if(_deviceList == null)
                {
                    using (var db = Database.Open())
                    {
                        session = db;
                        _deviceList = new List<Device>(session.CreateCriteria<Device>().List<Device>());
                    }
                }
                return _deviceList;
            }
        }
        private static List<Device> _deviceList;
        

        public void Add(Device dev)
        {
            if(deviceList != null)
                _deviceList.Add(dev);
        }

        public void ChangeName(Device dev, string name)
        {
            DeviceGet(dev.SN).Name = name;
        }


        /// <summary>
        /// SN에 해당되는 Device를 가져옵니다.
        /// </summary>
        public Device DeviceGet(int SN)
            => deviceList.Find(x => x.SN == SN);
            //=> session.Get<Device>(SN);

        /// <summary>
        /// Enabled가 true인 Device 목록을 가져옵니다.
        /// </summary>
        public IList<Device> ToEnableList()
            => deviceList.FindAll(x => x.Enabled == true);

        /// <summary>
        /// 모든 Device를 가져옵니다.
        /// </summary>
        /// <returns></returns>
        public IList<Device> ToList()
            => deviceList;

        /// <summary>
        /// 세션에 할당이 가능한 Device들을 가져옵니다.
        /// </summary>
        public IList<Device> FindPossibleDevice(int count)
            => deviceList.FindAll(x => (x.ConnectionState == DeviceConnectionState.Offline)
            && (x.State == DeviceState.Standby));
        /*
        => session.CreateCriteria<Device>()
        .Add(Expression.Gt("ConnectionState", DeviceConnectionState.Offline))
        .Add(Expression.Eq("State", DeviceState.Standby))
        .SetMaxResults(count)
        .List<Device>();
        */

        /// <summary>
        /// UUID가 일치하는 Device를 가져옵니다.
        /// </summary>
        public Device FindByUUID(string uuid)
            => deviceList.Find(x => x.UUID == uuid);
        //=> session.CreateCriteria<Device>()
        //.Add(Expression.Eq("UUID", uuid))
        //.UniqueResult<Device>();

        public Device FindByUUIDInDB(string uuid)
        {
            ILog log = LogManager.GetLogger(typeof(DeviceDao).Name);
            using (var db = Database.Open())
            {
                session = db;

                return session.CreateCriteria<Device>()
                .Add(Expression.Eq("UUID", uuid))
                .UniqueResult<Device>();
            }
        }



        // 서버와 연결된 기기를 가져온다.
        public IList<Device> FindConnectDeviceName()
        {
            List<Device> list = deviceList.FindAll(x => (x.ConnectionState == DeviceConnectionState.Agent)
               && (x.Pick == false));

            list.Sort((a, b) => a.Name.CompareTo(b.Name));
            return list;

            return session.CreateCriteria<Device>()
                .AddOrder(Order.Asc("Name"))
                .Add(Expression.Eq("ConnectionState", DeviceConnectionState.Agent))
                .Add(Expression.Eq("Pick", false ))
                .List<Device>();
        }

        //업데이트가 가능한 기기를 가져온다
        public IList<Device> FindUpdateableDevice()
        {
            return deviceList.FindAll(x => (x.ConnectionState == DeviceConnectionState.Agent)
            && (x.State == DeviceState.Standby));

            return session.CreateCriteria<Device>()
                .Add(Expression.Eq("ConnectionState", DeviceConnectionState.Agent))
                .Add(Expression.Eq("State", DeviceState.Standby))
                .List<Device>();
        }

        // 서버와 연결된 기기를 가져온다.
        public int FindConnectDeviceCount()
        {
            return deviceList.FindAll(x => (x.ConnectionState == DeviceConnectionState.Agent)).Count;

            return session.CreateCriteria<Device>()
                .Add(Expression.Eq("ConnectionState", DeviceConnectionState.Agent))
                .List<Device>().Count;
        }
    }
}
