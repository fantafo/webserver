﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIMS
{
    public struct UpdateDeviceDao
    {
        public static UpdateDeviceDao Get => new UpdateDeviceDao();

        private static List<UpdateDevice> updateDeviceList
        {
            get
            {
                if (_updateDeviceList == null)
                    _updateDeviceList = new List<UpdateDevice>();

                return _updateDeviceList;
            }
        }

        private static List<UpdateDevice> _updateDeviceList;

        public void Add(UpdateDevice dev)
        {
            if (updateDeviceList != null)
                _updateDeviceList.Add(dev);
        }

        public void Remove(int sn)
        {
            if (updateDeviceList != null)
            {
                var dev = FindBySN(sn);
                if(dev != null)
                    _updateDeviceList.Remove(FindBySN(sn));
            }
        }

        public List<UpdateDevice> GetList()
            => updateDeviceList;

        public UpdateDevice FindBySN(int sn)
            => updateDeviceList.Find(x => x.SN == sn);

        public void UpdateCheckChange(int sn, bool check)
            => updateDeviceList.Find(x => x.SN == sn).updateCheck = check;
        
    }
}
