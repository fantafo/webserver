﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIMS
{
    public enum LargeCategory : int
    {
        None = 0,
        Engineering = 1,
        agrochemicalOceanography = 2,
        ComplexScience,
        SocialScience,
        ArtAndPhysicalEducation,
        Medicine,
        humanities,
        naturalScience,
        KT_SuperVR
    }

    public class FileInfo
    {
        public virtual string FileName { get; set; }
        public virtual string Version { get; set; }
        public virtual string pkg { get; set; }
        public virtual DateTime UpdateDate { get; set; }
        public virtual long Size { get; set; }
        public virtual string Path { get; set; }
        public virtual string InstallPath { get; set; }
        public virtual Boolean UpdateCheck { get; set; }
        public virtual Boolean RemoveCheck { get; set; }
        public virtual LargeCategory Category { get; set; } = LargeCategory.None;
        
    }
}
