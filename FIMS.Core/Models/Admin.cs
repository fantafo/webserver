﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using System.Threading;

namespace FIMS
{
    public class Admin
    {
        //db 값읽기위한 변수
        public virtual string Id { get; set; }
        public virtual string Password { get; set; }
        public virtual DateTime Timelimit { get; set; }
        public virtual bool Login { get; set; }
        
    }
}
