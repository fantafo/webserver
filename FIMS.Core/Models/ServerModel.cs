﻿using System;
using FIMS.Modules;
using NHibernate.Collection.Generic;
using FIMS.Manager;

namespace FIMS
{
    public class ServerModel
    {
        public virtual byte SN { get; set; }
        public virtual string Ip { get; set; }
        public virtual short Port { get; set; }
    }
}
