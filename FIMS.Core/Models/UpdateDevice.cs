﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIMS
{
    public class UpdateDevice
    {
        public int SN { get; set; } //키값
        public string name { get; set; }    //기기명
        public bool updateCheck { get; set; }   //업데이트 여부
        public int downloadCount { get; set; }  //다운받은 파일수
        public int totalDownloadCount { get; set; } //다운받아야하는 총 파일수
        public bool completed { get; set; }   //완료여부
        /*error 오류 여부
         * 0 오류없음
         * -1 오류발생
         * -2 중지
         */
        public int error { get; set; }     //실패여부
        public bool isDownloadStart { get; set; }   //다운 시작여부
        public long storge { get; set; }    //저장공간
        public long totalStorageSize { get; set; }    //총 용량
        public long availableStorageSize { get; set; }    //사용 가능 용량


        public List<UpdateFileDetailInfo> detail { get; set; } = new List<UpdateFileDetailInfo>();

        public virtual object ToSerializeObject()
        {
            dynamic result = new
            {
                SN = SN,
                updateCheck = updateCheck,
                name = name,
                progress = downloadCount.ToString() + '#' + totalDownloadCount.ToString() + '#' + completed.ToString(),
                state = isDownloadStart.ToString() + '#' + completed.ToString() + '#' + error.ToString(),
                storge = totalStorageSize.ToString() + '#' + availableStorageSize.ToString(),
                detail = new object[detail.Count]
            };
            for(int i = 0; i < detail.Count; ++i)
            {
                int stop = 0;
                stop = (detail[i].error == 0 || detail[i].error == -2 )? 0 : 2;
                result.detail[i] = new
                {
                    sn = SN,
                    childName = detail[i].name,
                    //진행바 = 다운사이즈 + # + 다운받는 파일 사이즈 + # 중지여부(2 중지)
                    childProgress = detail[i].downloadsize.ToString() + '#' + detail[i].totalsize.ToString() + '#' + stop.ToString(),
                    childState = detail[i].isDownloadStart.ToString() + '#'
                    + detail[i].success.ToString() + '#' + detail[i].error.ToString()
                };
            }
            return result;
        }
    }

    public class UpdateFileDetailInfo
    {
        public string name { get; set; }
        /* downloadsize, totalsize
         * datatable의 진행도를 보여주기 위해 존재
         * 
         * 삭제하는 파일의 진행도일 경우
         * totalsize = 1 , downloadsize = 0 으로 설정
         * 삭제 이후, downloadsize = 1로 변경
         */
        public long downloadsize { get; set; }
        public long totalsize { get; set; }
        /* error 오류여부
         * 0 : 오류 없음
         * -2 : 이미 존재하는 파일
         * -3 : 저장공간 부족
         * -4 : 업데이트 중지 
         */
        public int error { get; set; }
        //다운완료시 true
        public bool success { get; set; }
        //다운 시작시 true, 종료시 false
        public bool isDownloadStart { get; set; }
    }


}
