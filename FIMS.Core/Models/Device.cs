﻿using System;
using FIMS.Modules;
using NHibernate.Collection.Generic;
using FIMS.Manager;
using log4net;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Diagnostics;
using System.Reflection;
using System.Collections;

namespace FIMS
{
    [Flags]
    public enum DeviceConnectionState : byte
    {
        Offline = 0,
        Agent = 2,
    }

    public enum DeviceState : byte
    {
        Standby, // 할당될 수 있는 상태
        IdCheck, // 로그인 이름 확인
        Allocated, //로그인이 완료된 상태 (과거: 할당된 상태)
        Ready,  //게임에 들어와 관리자가 시작누르기 기다리는상태
        Play,   //게임 플레이
        Heated, // 발열이 심하여 사용할 수 없는 상태
        Update,  //업데이트로 사용할 수 없는 상태
    }

    /// <summary>
    /// 단말기(핸드폰)을 가리키는 객체로서,
    /// DeviceDao를 통해서 획득할 수 있다.
    /// 
    /// 단말기에 대한 간단한 정보뿐만 아니라, 단말기의 상태까지 포함돼 있으며,
    /// AgentClient를 통한 단말기에 대한 명령을 수행할 수 있습니다.
    /// Alloc, Unalloc, Launch, Stop 등의 명령으로 단말기를 컨트롤 할 수 있습니다.
    /// </summary>
    public class Device
    {
        public const string PATH_CONNECT_PROP = "/sdcard/Fantafo/connect.properties";
        public const string PATH_SCORE_PROP = "/sdcard/Fantafo/score.properties";
        public const string PATH_ALLOC = "/sdcard/Fantafo/Alloc";


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                          MEMBER VARIABLES
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual int SN { get; set; } 
        public virtual string UUID { get; set; }
        public virtual bool Enabled { get; set; } = true;   //agent On/Off값(on = true)
        public virtual string IPAddress { get; set; }
        public virtual string Name { get; set; }
        public virtual string Model { get; set; }
        public virtual DateTime RegistDate { get; set; } = DateTime.Now;
        public virtual DateTime AcceptTime { get; set; } = DateTime.Now;
        public virtual DeviceConnectionState ConnectionState { get; set; } = DeviceConnectionState.Offline;
        public virtual DeviceState State { get; set; } = DeviceState.Standby;
        public virtual byte Thermal { get; set; }
        public virtual byte Battery { get; set; }
        public virtual bool Cont { get; set; } // Controller
        public virtual bool Live { get; set; }
        public virtual string User { get; set; }
        public virtual string UserName { get; set; }
        public virtual bool Pick { get; set; }

        private ILog _log;
        private ILog log
        {
            get
            {
                if(_log == null)
                {
                    _log = LogManager.GetLogger($"Dev({SN}/{IPAddress})");
                }
                return _log;
            }
        }
            


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                          CONNECTION METHODS
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual bool IsConnected
        {
            get { return ConnectionState != DeviceConnectionState.Offline; }
        }
        public virtual bool IsAgent
        {
            get { return (ConnectionState & DeviceConnectionState.Agent) != 0; }
            set
            {
                ConnectionState = (ConnectionState & (DeviceConnectionState)(0xFF ^ (int)DeviceConnectionState.Agent)) | (value ? DeviceConnectionState.Agent : 0);
            }
        }

        public virtual AgentClient GetAgentClient()
            => Fims.agentServer != null
            ? Fims.agentServer.GetClient(SN)
            : null;


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                           COMMAND METHODS
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual void Alloc(GameSession session)
        {
            const string TAG = "Allocate";
            if (IsAgent)
            {
                var client = GetAgentClient();

                if (client != null)
                {
                    int fast = session.Fast ? 1 : 0;
                    string msg = $"alloc pkg={session.game.PackageName} ip={session.game.ServerConvertIP} rname={session.Name} rpass={session.Name} "
                        + $"port={session.game.ServerPort} num={session.RoomNum} level={session.Level} stage={session.Stage} user={session.members.Count} "
                        + $"usn={SN} uname={Name} launchid={session.SN} fast={fast}";
                    
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("Alloc/ Device disabled");
            return;
        }

        public virtual void Unalloc()
        {       
            const string TAG = "Unallocate";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "unalloc";
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("Unalloc/ Device disabled");
        }

        public virtual void DisplayLock()
        {
            const string TAG = "DisplayLock";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = $"DisplayLock value={SettingDao.BlackLockScreenApp}";
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("DisplayLock/ Device disabled");
        }

        public virtual bool Launch(GameSession session, GameSessionUser user)
        {
            const string TAG = "Launch";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = $"launch pkg={session.game.PackageName} ip={session.game.ServerConvertIP} rname={session.Name} rpass={session.Name} "
                        + $"port={session.game.ServerPort} num={session.RoomNum} level={session.Level} stage={session.Stage} user={session.members.Count} "
                        + $"usn={user.device.SN} uname={user.device.User} launchid={session.SN}";

                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                }
            }
            log.Warn("{TAG}/ Device disabled");
            return false;
        }

        public virtual void LaunchStop(GameSession session, bool directStop = true)
        {
            const string TAG = "Stop";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = $"stop";
                    if (directStop)
                        msg += " direct=1";
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("{TAG}/ Device disabled");
        }

        public virtual void ChangeName(GameSession session, GameSessionUser user)
        {
            const string TAG = "Name";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = $"name";
                    msg += user.device.User;
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("{TAG}/ Device disabled");
        }

        public virtual void Start(int time)
        {
            const string TAG = "Start";
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = $"start time={time}";
                    client.Command(msg);
                    log.Debug($"{TAG}/ '{msg}'");
                    return;
                }
            }
            log.Warn("Alloc/ Device disabled");
        }

        //사용안함
        //Item1 FileName, Item2 FileSize, Item3 FileInstallPath
        public virtual void CheckStorage(List<Tuple<string, long, string>> updatefileList)
        {
            const string TAG = "CheckStorage";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "CheckStorage fileName=";
                    
                    foreach (var file in updatefileList)
                    {
                        msg += $"{file.Item1}\\";
                    }

                    msg += " fileSize=";

                    foreach (var file in updatefileList)
                    {
                        msg += $"{file.Item2}\\";
                    }

                    msg += " fileInstallPath=";

                    foreach (var file in updatefileList)
                    {
                        msg += $"{file.Item3}\\";
                    }

                    client.Command(msg);
                }
                return;
            }

        }

        //다운 받을 파일 정보를 전송한다.
        //Item1 Path - 경로는 파일명 포함, Item2 Size Item3 installPath
        public virtual void SendFile(List<Tuple<string, long, string>> updatefileList)
        {
            const string TAG = "SendFile";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    //버퍼크기 1024 나눠서 보낼것
                    int maxsize = 250;
                    int start = 0;
                    int count = 0;

                    while (count < updatefileList.Count)
                    {
                        string msg = "DownloadFile path=";
                        for (int i = start; i < updatefileList.Count; ++i)
                        {
                            //공백의 문자열일 경우 @@@으로 변환하여 전송
                            //해도 의미 없음. ftp 공백있으면 url 주소로 접근 못함
                            string path = $"{updatefileList[i].Item1.Replace(" ", "@@@")}\\";
                            if (msg.Length + path.Length > 250)
                            {
                                count = i;
                                break;
                            }
                            else
                            {
                                count++;
                                msg += path;
                            }
                        }

                        msg += " size=";
                        for (int i=start; i<count; ++i)
                        {
                            msg += $"{updatefileList[i].Item2}\\";
                        }

                        msg += " installPath=";
                        for (int i = start; i < count; ++i)
                        {
                            msg += $"{updatefileList[i].Item3}\\";
                        }
                        start = count;
                        client.Command(msg);
                        log.Debug(msg);
                    }
                }
                return;
            }
            //log.Warn("{TAG}/ Device Send APK files");
            
        }

        //삭제할 파일 정보를 보낸다.
        public virtual void DeleteFile(List<Tuple<string, string>> deletefileList)
        {
            const string TAG = "DeleteFile";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "DeleteFile name=";

                    foreach (var file in deletefileList)
                    {
                        msg += $"{file.Item1}\\";
                    }

                    msg += " installPath=";

                    foreach (var file in deletefileList)
                    {
                        msg += $"{file.Item2}\\";
                    }
                    client.Command(msg);
                }
                return;
            }

        }

        //모든 파일 정보를 보낸다.
        public virtual void DeleteAllFiles()
        {
            const string TAG = "DeleteAllFiles";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "DeleteAllFiles";
                    //TODO.. 하드 코딩 교체.. 현재 사용중인 파일 mp4
                    msg += " installPath=mp4";
                    client.Command(msg);
                }
                return;
            }

        }


        public virtual void StopUpdate()
        {
            const string TAG = "StopUpdate";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "StopUpdate";
                    client.Command(msg);
                }
                return;
            }
        }


        //디바이스가 가진 콘텐츠 세부정보를 가져온다.
        public virtual void GetDeviceDetails()
        {
            const string TAG = "GetDeviceDetails";
            log.Debug($"{TAG}/ Start");
            if (IsAgent)
            {
                var client = GetAgentClient();
                if (client != null)
                {
                    string msg = "DeviceDetails path=";

                    foreach(string ext in FimsSetting.UpdateExtension)
                    {
                        msg += $"{ext.Replace(".", "")}\\";
                    }
                    client.Command(msg);
                }
                return;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                          OVERRIDE AND SERIALIZE
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public override string ToString() => Name;

        public virtual object ToSerializeObject()
            => new
            {
                SN = SN,
                Name = Name,
                Thermal = Thermal,
                Battery = Battery,
                Cont = Cont,
                Live = Live,
                User = "",
                Pick = Pick,
                State = State
            };

        //public virtual object ToSerializeUpdateInfoObejct()
        //    => new
        //    {
        //        SN = SN,
        //        list = new object[downloadProgress.Count]
        //        {
        //            for(int )
        //        }
        //    };


        public virtual Device Clone()
        {
            return (Device) base.MemberwiseClone();
        }
    }

    //public class DeviceInstall
    //{
    //    public virtual Game game { get; set; }
    //    public virtual int Version { get; set; }
    //}
}
