﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Criterion;

namespace FIMS
{
    public class Guest
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
    }

    public struct GuestDao
    {
        public static GuestDao Get(ISession sess) => new GuestDao { session = sess };
        private ISession session;
        
        public Guest FindID(string id)
            => session.CreateCriteria<Guest>()
                    .Add(Expression.Eq("Id", id))
                    .UniqueResult<Guest>();
    }
}
