﻿using NHibernate.Collection.Generic;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace FIMS
{
    public enum SessionState : byte
    {
        None, //세션 생성중
        Allocating, // 수정. 사용안함 (과거: 사용가능한 단말기를 찾고 있습니다.)
        Standby, // 수정. 사용안함 (과거: 모든 단말기가 할당됐습니다. 게임을 실행할 수 있습니다.)
        GameLaunch, //수정. 로그인씬진행중 (과거: 게임이 실행중입니다.)
        GameReady, //수정. 모든 유저가 로그인 완료(과거: 모든 유저가 게임에 접속했으며, 게임을 실행 할 수 있습니다.)
        GameRun, // 게임이 실행중입니다.
        GameEnding, //추가. 콘텐츠가 종료
        GameEnd, // 게임의 종료처리
        Returned // 사용되지 않습니다.
    }

    /// <summary>
    /// 한개의 방단위를 가리키며 세션이라 부른다.
    /// 
    /// 한개의 방에는 여러명의 유저가 포함 될 수 있습니다.
    /// 또한 어떤 맵을 할것인지에 대해서도 정의할 수 있습니다.
    /// 
    /// 또한 실제로 서버와 통신을 하기도 합니다.
    /// </summary>
    public class GameSession
    {

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                          MEMBER VARIABLES
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual long SN { get; set; }
        public virtual string Name { get; set; }
        public virtual short Level { get; set; }
        public virtual string Detail { get; set; }
        public virtual short Stage { get; set; }
        public virtual int Score { get; set; }
        public virtual int RoomNum { get; set; }
        //true일시 로그인 생략하고 바로 진행.
        public virtual bool Fast { get; set; }
        public virtual SessionState State { get; set; }
        public virtual DateTime RegistDate { get; set; }
        public virtual DateTime LaunchDate { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual DateTime ReturnDate { get; set; }

        public virtual Game game { get; set; }
        public virtual IList<GameSessionUser> members { get; set; }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                          Constructor
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public GameSession()
        {
            members = new List<GameSessionUser>();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                Communication Methods about Server
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual dynamic CreateRoomToServer(bool observer)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string body = serializer.Serialize(ToSerializeDetail(observer));
            try
            {
                HttpHelper.RecordLog = true;
                return HttpHelper.PostJSON(this, $"Create", body);
            }
            finally
            {
                HttpHelper.RecordLog = false;
            }
        }
        public virtual dynamic StartToRoom()
        {
            return HttpHelper.GetJSON(this, $"Start?num={RoomNum}");
        }
        public virtual dynamic StopToRoom()
        {
            return HttpHelper.GetJSON(this, $"Stop?num={RoomNum}");
        }
        public virtual dynamic InfoToRoom()
        {
            return HttpHelper.GetJSON(this, $"Info?num={RoomNum}");
        }

        public virtual dynamic LoginFailed(Device device)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result = serializer.Serialize(ToSerializeDevice(device));

            return HttpHelper.PostJSON(this, $"LoginFailed", result);
        }


        //한명 감소(강제 제거)
        public virtual dynamic RemoveMember(Device device)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string body = serializer.Serialize(ToSerializeDevice(device));
            try
            {
                HttpHelper.RecordLog = true;
                return HttpHelper.PostJSON(this, $"Reduce", body);
            }
            finally
            {
                HttpHelper.RecordLog = false;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                        Serialize Methods
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public virtual object ToSerializeObject()
        {
            dynamic result = new
            {
                SN = SN,
                Name = Name,
                State = State,
                Level = Level,
                Stage = Stage,
                Score = Score,
                RegistDate = RegistDate,
                StartDate = StartDate,
                EndDate = EndDate,
                ReturnDate = ReturnDate,
                game = game != null ? game.ToSerializeObject() : null,
                Detail = Detail.IsEmpty() ? "" : Detail,
                members = new object[members.Count]
            };
            for (int i = 0; i < members.Count; i++)
            {
                Device dev = DeviceDao.Get.DeviceGet(members[i].device.SN);
                // 수정
                result.members[i] = new
                {
                    member = new
                    {                       
                        SN = dev.SN,
                        Name = dev.Name,
                        Cont = dev.Cont,
                        Live = dev.Live,
                        User = dev.User,
                        UserName = dev.UserName
                        //UserName = MemberDao.Get(db)
                    },
                    Name = members[i].Name,
                    Score = members[i].Score,
                    Connected = members[i].Connected
                };
            }
            return result;
        }

        public virtual object ToSerializeDetail(bool observer)
        {
            dynamic result = new
            {
                SN = SN,
                Name = Name,
                State = State,
                Map = (short)2, //난이도에 따라 실행 시킬 씬이 다르다면 해당 값을 조절
                Level = Level,
                Stage = Stage,
                Score = Score,
                Fast = Fast,
                RegistDate = RegistDate,
                StartDate = StartDate,
                EndDate = EndDate,
                ReturnDate = ReturnDate,
                Observer = observer,
                game = game != null ? game.ToSerializeObject() : null,
                Detail = Detail.IsEmpty() ? "" : Detail,
                members = new object[members.Count]
            };
            for (int i = 0; i < members.Count; i++)
            {
                result.members[i] = new
                {
                    device = members[i].device.ToSerializeObject(),
                    Name = members[i].Name,
                    Score = members[i].Score,
                    Connected = members[i].Connected,
                };

                /*
                if (members[i].member != null)
                {
                    result.members[i] = new
                    {
                        //수정
                        member = members[i].member.ToSerializeObject(Fast,game.Name),
                        device = members[i].device.ToSerializeObject(),
                        Name = members[i].Name,
                        Score = members[i].Score,
                        Connected = members[i].Connected,
                    };
                }
                else
                {
                    result.members[i] = new
                    {
                        //수정
                        //member = members[i].member.ToSerializeObject(),
                        device = members[i].device.ToSerializeObject(),
                        Name = members[i].Name,
                        Score = members[i].Score,
                        Connected = members[i].Connected
                    };
                }*/

            }
            return result;
        }

        public virtual object ToSerializeDevice(Device device)
        {
            dynamic result = new
            {
                RoomNum = RoomNum,
                DeviceSN = device.SN
            };
            return result;
        }
    }

    public class GameSessionUser
    {
        public virtual Member member { get; set; }
        public virtual Device device { get; set; }
        public virtual string Name { get; set; }
        public virtual bool Connected { get; set; }
        public virtual int Score { get; set; }
        public virtual int Ord { get; set; }
        public virtual long SessionSN { get; set; }

        public static implicit operator Member(GameSessionUser m) { return m.member; }
        public static implicit operator Device(GameSessionUser m) { return m.device; }
        public static implicit operator GameSessionUser(Member m) { return new GameSessionUser { member = m }; }
    }
}
