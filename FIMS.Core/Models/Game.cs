﻿using System;
using System.Collections.Generic;

namespace FIMS
{
    /// <summary>
    /// 여러개의 게임 혹은 컨탠츠에 대한 정보를 나타냅니다.
    /// 패키지는 어떤것인지, 게임 버전은 어떻게 되는지 등의 정보들을 포함하고있습니다.
    /// 이 클래스는 단순한 데이터 테이블일 뿐, 역할을 수행하지는 않습니다.
    /// </summary>
    public class Game
    {
        public virtual int SN { get; set; }
        public virtual bool Enabled { get; set; }
        public virtual string Name { get; set; }
        public virtual string PackageName { get; set; }
        public virtual int Version { get; set; }
        public virtual byte MaxUser { get; set; }
        public virtual short MaxLevel { get; set; }
        public virtual string[] SafetyType { get; set; } = { "화학", "전기" , "생물" };
        //세부사항을 저장하기 위해. 현재 360 비디오 파일명을 저장
        public virtual List<string> DetailInfo { get; set; } = new List<string>();
        public virtual string ServerIP { get; set; }
        public virtual int ServerPort { get; set; } //agent가 commonserver에 연결시 사용하는 포트
        public virtual int ServerComsPort { get; set; } //web과 commonserver 연결 포트
        public virtual DateTime LastCheckTime { get; set; }

        //GameServer 연결 여부
        public virtual bool IsCommon { get; set; }

        //Observer 사용가능 여부
        public virtual bool IsObserver { get; set; }

        public virtual bool IsCheckedServerOn => ((DateTime.Now - LastCheckTime).TotalSeconds < 5);
        public virtual string ServerConvertIP => IpHelper.GetIpFromDomain(ServerIP);

        public virtual object ToSerializeObject()
        {
            return new
            {
                SN = SN,
                Enabled = Enabled,
                Name = Name,
                PackageName = PackageName,
                Version = Version,
                MaxUser = MaxUser,
                MaxLevel = MaxLevel,
                LastCheckTime = LastCheckTime,
                IsObserver = IsObserver
            };
        }

        public virtual dynamic GetVersionFromServer()
        {
            return HttpHelper.GetHTTP(this, "Version", 300);
        }
    }
}