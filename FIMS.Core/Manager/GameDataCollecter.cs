﻿using FIMS.Modules;
using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FIMS
{
    /// <summary>
    /// 게임서버에서 데이터를 읽어와
    /// 현재 게임세션에 정보를 동기화시키는 역할을 합니다.
    /// </summary>
    public class GameDataCollecter
    {
        #region General Methods
        static ILog log = LogManager.GetLogger(typeof(GameDataCollecter).Name);
        public bool IsRun { get; private set; }
        private Thread thread;

        public void Start()
        {
            if (!IsRun)
            {
                IsRun = true;
                thread = new Thread(Run);
                thread.Name = "GameDataCol";
                thread.Start();
            }
        }

        public void Stop()
        {
            if (IsRun)
            {
                IsRun = false;
                thread.Join();
                thread = null;
            }
        }
        #endregion General Methods

        private void Run()
        {
            log.Info("Start Thread");
            while (IsRun)
            {
                Thread.Sleep(1000);
                try
                {
                    using (var db = Database.Open())
                    {
                        // 이미 시작된 게임에 대한 정보를 가져온다.
                        foreach (var sess in Fims.gameSessionManager.GetRunningSessions(db))
                        {
                            if (sess.game != null && sess.RoomNum != 0)
                            {
                                try
                                {
                                    if (sess.RoomNum == 0)
                                        continue;

                                    // 서버에서 데이터를 가져온다.
                                    dynamic result = sess.InfoToRoom();

                                    //log.Debug("GameDataCollecter "+ result);

                                    using (var db2 = Database.Open())
                                    using (var trans = db2.BeginTransaction())
                                    {
                                        try
                                        {
                                            GameSession session = GameSessionDao.Get(db2).Get(sess.SN);
                                            int stage = result["stage"];
                                            int totalScore = 0;
                                            int maxUser = result["users"].Length;
                                            int currUser = 0;

                                            // 유저 접속목록 초기화
                                            for (int j = 0; j < session.members.Count; j++)
                                            {
                                                session.members[j].Connected = false;
                                            }

                                            // 데이터 조합
                                            for (int j = 0; j < maxUser; j++)
                                            {
                                                long sn;
                                                try
                                                {
                                                    sn = result["users"][j]["sn"];
                                                }
                                                catch (KeyNotFoundException)
                                                {
                                                    sn = session.members[j].member.SN;
                                                }
                                                int score = result["users"][j]["score"];
                                                totalScore += score;
                                                
                                                var user = (from usr in session.members where usr.device.SN == sn select usr).FirstOrDefault();
                                                if (user != null)
                                                {
                                                    user.Score = score;
                                                    //log.Debug("GameDataCollecter user: " + user + " user.Score: " + user.Score);

                                                    user.Connected = result["users"][j]["isConnected"];
                                                    if (user.Connected)
                                                    {
                                                        currUser++;
                                                    }

                                                }
                                            }
                                            /*
                                            switch (session.State)
                                            {
                                                case SessionState.GameLaunch:
                                                    //중간에 오류로 인해 제거한 유저를 위해서
                                                    if (currUser == maxUser || currUser == session.members.Count)
                                                    {
                                                        session.State = SessionState.GameReady;
                                                    }
                                                    break;
                                                case SessionState.GameReady:
                                                    if (currUser != maxUser)
                                                    {
                                                        session.State = SessionState.GameLaunch;
                                                    }
                                                    break;
                                            }
                                            */
                                            db2.Update(session);
                                            trans.Commit();
                                        }
                                        catch(Exception e)
                                        {
                                            trans.Rollback();
                                            throw e;
                                        }
                                    }
                                }
                                catch (HttpServerException)
                                {
                                    switch (sess.State)
                                    {
                                        case SessionState.GameLaunch:
                                        case SessionState.GameReady:
                                        case SessionState.GameRun:
                                            {
                                                using(var trans = db.BeginTransaction())
                                                {
                                                    sess.State = SessionState.GameEnd;
                                                    db.Update(sess);
                                                    trans.Commit();
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                //외부 apk를 실행할 경우 sess.RoomNum = 0 으로 됩니다.
                                //log.Debug("GameDataCollecter Thread: sess.game null or sess.RoomNum = 0");
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Fatal(e);
                }
            }
            log.Info("Stop Thread");
        }
    }
}
