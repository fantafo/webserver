﻿using FIMS.Modules;
using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FIMS
{
    /// <summary>
    /// 게임서버에 지속적으로 접속하여 게임이 살아있는지 검사합니다.
    /// </summary>
    public class GameServerManager
    {
        static ILog log = LogManager.GetLogger(typeof(GameServerManager).Name);

        public bool IsRun { get; private set; }
        private Thread thread;

        public void Start()
        {
            if (!IsRun)
            {
                IsRun = true;
                thread = new Thread(Runnable);
                thread.Name = GetType().Name;
                thread.Start();
            }
        }

        public void Stop()
        {
            if (IsRun)
            {
                IsRun = false;
                thread.Join();
                thread = null;
            }
        }

        private void Runnable()
        {
            log.Info("Start Thread");
            while (IsRun)
            {
                Thread.Sleep(1000);
                using (var db = Database.Open())
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        bool isDirty = false;
                        IList<Game> list = GameDao.Get(db).GetUseCommon();
                        foreach (var game in list)
                        {
                            try
                            {
                                game.GetVersionFromServer();
                                game.LastCheckTime = DateTime.Now;
                                db.Update(game);
                                isDirty = true;
                            }
                            catch { }
                        }

                        if(isDirty)
                            trans.Commit();
                    }
                    catch (Exception e)
                    {
                        trans.Rollback();
                        e.PrintStackTrace();
                    }
                }
            }
            log.Info("Stop Thread");
        }
    }
}
