﻿using FIMS.Modules;
using log4net;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Web.Script.Serialization;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace FIMS.Manager
{
    /// <summary>
    /// 게임 세션을 가져오거나,
    /// 게임세션에 관한 갖가지 유틸들을 가지고 있습니다.
    /// </summary>
    public class GameSessionManager
    {
        public static ILog log = LogManager.GetLogger(typeof(GameSessionManager).Name);


        ////////////////////////////////////////////////////////////////////////////////////////
        //
        //                  Get Sessions
        //
        ////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 반환되지 않은, 활성화된 세션정보를 가져온다.
        /// </summary>
        public IList<GameSession> GetActiveSessions()
        {
            using (var db = Database.Open())
            {
                return GetActiveSessions(db);
            }
        }
        public IList<GameSession> GetActiveSessions(ISession db)
        {
            return GameSessionDao.Get(db).ToActiveList();
        }

        /// <summary>
        /// 할당이 필요한 세션 목록을 가져옵니다.
        /// </summary>
        public IList<GameSession> GetAllocatingSessions()
        {
            using (var db = Database.Open())
            {
                return GetAllocatingSessions(db);
            }
        }
        public IList<GameSession> GetAllocatingSessions(ISession db)
        {
            return GameSessionDao.Get(db).ToAllocatingList();
        }

        public IList<GameSession> GetGameLaunchSessions(ISession db)
        {
            return GameSessionDao.Get(db).ToGameLaunchList();
        }


        public IList<GameSession> GetGameReadySessions(ISession db)
        {
            return GameSessionDao.Get(db).ToGameReadyList();
        }

        /// <summary>
        /// 어플리케이션을 실행한(STATE.LAUNCH) 이상의 세션정보를 가져온다 
        /// </summary>
        public IList<GameSession> GetRunningSessions(ISession db)
        {
            return GameSessionDao.Get(db).ToRunningList();
        }



        ////////////////////////////////////////////////////////////////////////////////////////
        //
        //                  Game Cycle Logic
        //
        ////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 세션의 정보를 저장한다.
        /// Apply가 되는 경우는 세션이 최초에 접속될 때가 있으며,
        /// Launch되기 전까지만 Apply를 할 수 있다.
        /// </summary>
        public void Apply(ISession db, ITransaction trans, GameSession session, bool observer)
        {
            if (session.State > SessionState.GameLaunch)
                throw new Exception("이미 게임이 실행되어 세션을 수정할 수 없습니다.");
            
            List<int> error = new List<int>();
            try
            {
                //-- 게임서버에게 방 생성 요청을 한다.
                if (session.game.IsCommon)
                {
                    dynamic result = session.CreateRoomToServer(observer);
                    session.RoomNum = result["num"];
                    session.State = SessionState.GameLaunch;
                }
                else
                {
                    session.RoomNum = 0;
                    session.State = SessionState.GameRun;
                }
                session.LaunchDate = DateTime.Now;
                session.StartDate = session.LaunchDate;
                db.SaveOrUpdate(session);
                trans.Commit();

                //GameServer를 사용하지 않는경우.
                if (!session.game.IsCommon)
                {
                    for (int i = 0; i < session.members.Count; ++i)
                    {
                        GameSessionUser user = new GameSessionUser();
                        user.device = new Device();
                        user.device.SN = session.members[i].device.SN;
                        user.device.User = "";
                        session.members[i].device.Alloc(session);
                    }
                }
                else
                {
                    for (int i = 0; i < session.members.Count; ++i)
                    {
                        session.members[i].device.Alloc(session);
                    }
                }
                
                log.Debug($"Apply/{session.SN}: {session.Name} /RoomNum:{session.RoomNum} /{session.game.Name}({session.game.SN}) /Member:{session.members.Count} /Level:{session.Level}");
                //에러가 있다면 작동 중지.

                if (!error.IsEmpty())
                {
                    string msg = "";
                    foreach (int num in error)
                    {
                        msg += $"{session.members[num].device.Name}, ";
                    }
                    log.Debug($"{msg}에 문제가 생겼습니다 해당 기기를 제외 해주세요.");
                    throw new System.ArgumentException($"{msg}에 문제가 생겼습니다 해당 기기를 제외 해주세요.");
                }
            }
            catch (Exception e)
            {
                log.Error($"Apply/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 수정. 더이상 사용안함. 일부를 Apply로 옮김(방생성부분)
        /// 세션을 어플 실행상태로 만듭니다.
        /// 미리 서버에 '접속할것이니 방을 생성하고 방번호를 내놔라'라고 얘기한 뒤,
        /// 각 단말기에 '게임을 실행하고 이 방에 접속해라'라고 전달한다.
        /// </summary>
        public void Launch(ISession db, GameSession session)
        {
            if (session.State != SessionState.Standby)
                throw new Exception("모든 HMD가 할당되지 않은 상태입니다.");

            try
            {
                //수정. 일부를 Apply로 옮김(방생성부분)

                log.Info($"Launch/{session.SN}: ({session.game.Name})");
                //-- 할당 돼 있는 단말기를 실행시킨다.
                for (int i = 0; i < session.members.Count; i++)
                {
                    var user = session.members[i];
                    if (user != null && user.member != null && user.device != null)
                    {
                        user.device.Launch(session, user);
                    }
                    else
                    {
                        throw new NullReferenceException($"{session.SN}({session.game.Name})의 {i}번째 유저에 대한 정보가 누락됐습니다. (member:{user.member != null}/device:{user.device != null})");
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Launch/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 대기실에 있는 유저들을 대상으로 게임을 시작한다.
        /// 모든 유저가 게임에 들어온 뒤에 대기방에서 대기하고 있을때만 사용할 수 있다.
        /// </summary>
        public void Start(ISession db, GameSession session)
        {
            if (session.State != SessionState.GameReady)
                throw new Exception("아직 모든 유저가 접속하지 않았습니다.");

            try
            {
                //RoomNum은 게임서버로 부터 받고, 시작값이 1
                //게임서버와 연결이 안될경우 기본값 0이 들어간다.
                if (session.RoomNum != 0)
                {
                    session.StartToRoom();
                }
                log.Debug($"Start/{session.SN}({session.Name})");

                using (var t = db.BeginTransaction())
                {
                    session.StartDate = DateTime.Now;
                    session.State = SessionState.GameRun;
                    db.Update(session);
                    t.Commit();
                }
                
                
            }
            catch (Exception e)
            {
                log.Error($"Start/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 대기실에 모든 유저가 안모여도 강제로 시작
        /// </summary>
        public void ForcedStart(ISession db, GameSession session)
        {
            if ( session.State <= SessionState.None && session.State >= SessionState.GameRun)
                throw new Exception("방 상태가 강제 시작을 진행 할 수 없습니다.");

            try
            {   
                //RoomNum은 게임서버로 부터 받고, 시작값이 1
                //게임서버와 연결이 안될경우 기본값 0이 들어간다.
                if (session.RoomNum != 0)
                {
                    session.StartToRoom();
                }
                log.Debug($"Start/{session.SN}({session.Name})");

                using (var t = db.BeginTransaction())
                {
                    session.StartDate = DateTime.Now;
                    session.State = SessionState.GameRun;
                    db.Update(session);
                    t.Commit();
                }
            }
            catch (Exception e)
            {
                log.Error($"Start/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 특정 유저를 제거합니다.
        /// </summary>
        public void RemoveMember(ISession db, GameSession session, int deviceSN)
        {
            try
            {
                Device device = DeviceDao.Get.DeviceGet(deviceSN);
                if (device == null)
                {
                    throw new Exception("해당 기기를 찾을 수 없습니다.");
                }
                Member member = MemberDao.Get(db).FindID(device.User);
                if(member != null)
                    member.Login = false;
                
                device.Unalloc();
                for(int i=0; i<session.members.Count; ++i)
                {
                    if (session.members[i].device.SN == deviceSN)
                    {
                        //게임서버에 유저 1명 감소 전달
                        //RoomNum은 게임서버로 부터 받고, 시작값이 1
                        //게임서버와 연결이 안될경우 기본값 0이 들어간다.
                        if (session.RoomNum != 0)
                        {
                            session.RemoveMember(session.members[i].device);
                        }
                        session.members.RemoveAt(i);
                        break;
                    }
                }

                using (var t = db.BeginTransaction())
                {
                    device.State = DeviceState.Standby;

                    if (member != null)
                        db.Update(member);

                    //db.Update(device);
                    db.Update(session);
                    t.Commit();
                }
            }
            catch (Exception e)
            {
                log.Error($"RemoveMember/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 게임을 종료합니다.
        /// stopServer가 true일 경우, 서버에서부터 게임을 종료하게하여서 클라이언트 자체에서 처리할 기회를 준다.
        /// stopServer가 false이거나 서버에 stop신호를 날렸음에도 반송됐을경우 클라이언트 자체를 강제로 종료하게합니다.
        /// 강제종료시에는 방이 삭제되지 않을 가능성이 크니 주의해야합니다.
        /// </summary>`
        public void Stop(ISession db, GameSession session, bool stopServer, int state)
        {
            log.Debug($"Stop 진행");
            //if (session.State >= SessionState.GameEnd)
            //    return;

            try
            {
                using (var t = db.BeginTransaction())
                {
                    var pastState = session.State;
                    //session.State = SessionState.GameEnd;
                    session.State = (SessionState)state;
                    db.Update(session);
                                        
                    //점수 기록
                    DateTime nowTime = DateTime.Now;
                    //현재 StartToRoom 시간이 0001-01-01로 기록됨.. 향후 문제 없을 시 아래로 변경
                    //int completionTime = (int)(nowTime - session.StartToRoom).TotalMinutes;
                    int completionTime = (int)(nowTime - session.LaunchDate).TotalMinutes;

                    foreach (GameSessionUser user in session.members)
                    {
                        if (user != null)
                        {
                            //디바이스에 기록된 id 지움
                            var dev = DeviceDao.Get.DeviceGet(user.device.SN);
                            //var ID = dev.User;
                            //dev.User = System.String.Empty;
                            dev.UserName = System.String.Empty;
                            dev.Live = false;
                            //db.Update(user.device);
                            //먼저 게스트인지 확인.. 게스트는 점수저장 안함
                            Guest guest = GuestDao.Get(db).FindID(dev.User);
                            if (guest != null)
                            {
                                dev.User = System.String.Empty;
                                continue;
                            }
                            //회원은 점수 저장
                            Member member = MemberDao.Get(db).FindID(dev.User);
                            dev.User = System.String.Empty;
                            if (member != null)
                            {
                                log.Debug("STOP  member is not null    Name:" + member.Name);
                                member.Login = false;
                                
                                //이용 정보 기록
                                Completion num = new Completion();
                                num.Score = user.Score;
                                num.Date = nowTime;
                                //int time = (int)(num.Date - session.StartDate).TotalMinutes;
                                //Int32.TryParse((num.Date - session.StartDate).TotalMinutes.ToString(),out time);
                                num.PlayTime = completionTime;
                                member.Completions.Add(num);
                                db.Update(member);
                            }
                        }
                    }
                    t.Commit();
                }

                // 서버에 종료요청을 한다
                bool directStop = !stopServer;
                if (stopServer)
                {
                    try
                    {
                        if(session.RoomNum !=0)
                            session.StopToRoom();
                        
                    }
                    catch (HttpServerException)
                    {
                        log.Warn($"Stop/FailedServerStop: {session.SN}({session.RoomNum})");
                        directStop = true;
                    }
                }

                // 종료 요청에 실패했을경우에는 단말기상에서
                // 게임을 강제종료시키게 한다.
                if (directStop)
                {
                    for (int i = 0; i < session.members.Count; i++)
                    {
                        var user = session.members[i];
                        user?.device?.LaunchStop(session, directStop);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error($"Stop/{session.SN}: {e.Message}", e);
                throw e;
            }
        }

        /// <summary>
        /// 사용되고있는 게임세션을 해제합니다.
        /// 세션의 현재 상태에 맞게 데이터를 처리한 뒤, 
        /// 현재 상태를 조건으로 데이터베이스에 보관합니다.
        /// </summary>
        public void Release(ISession db, GameSession session)
        {
            using (var trans = db.BeginTransaction())
            {
                try
                {
                    log.Debug($"Release/Pick: {session.members.Count}");
                    for (int i=0; i<session.members.Count; ++i)
                    {
                        Device dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                        dev.Pick = false;
                        dev.State = DeviceState.Standby;
                        //db.Update(session.members[i].device);
                    }
                    
                    switch (session.State)
                    {
                        case SessionState.None:
                        case SessionState.Allocating:
                        case SessionState.Standby:
                            {
                                ForeachMemberDevices(db, session, u => DeviceDao.Get.DeviceGet(u.SN).Unalloc());
                                db.Delete(session);
                                trans.Commit();
                            }
                            break;

                        case SessionState.GameLaunch:
                        case SessionState.GameReady:
                            {
                                ForeachMemberDevices(db, session, u => DeviceDao.Get.DeviceGet(u.SN).LaunchStop(session));
                                session.State = SessionState.GameEnd;
                                session.EndDate = DateTime.Now;

                                db.Update(session);
                                trans.Commit();// 이미 게임을 실행시킨 전적은 로그에 남겨둔다.
                            }
                            break;

                        case SessionState.GameEnd:
                            {
                                ForeachMemberDevices(db, session, u => DeviceDao.Get.DeviceGet(u.SN).Unalloc());
                                session.State = SessionState.Returned;
                                session.ReturnDate = DateTime.Now;

                                db.Update(session);
                                trans.Commit();// 이미 게임을 실행시킨 전적은 로그에 남겨둔다.
                            }
                            break;
                    }

                    // 게임 서버에 해당 방을 종료하라고 알린다
                    // 이미 종료되어 있을수도 있지만 다시한번 알려서 종료해준다.
                    if (session.RoomNum != 0)
                    {
                        try
                        {
                            if (session.RoomNum == 0)
                                return;

                            session.StopToRoom();
                            log.Info($"StopServer/{session.SN}({session.game.Name})");
                        }
                        catch (HttpServerDontFoundException) { }
                        catch (HttpServerBadResponseException) { }
                    }

                    log.Info($"Release/{session.SN}: ({session.game.Name})");
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    log.Error($"Release/{session.SN}: {e.Message}", e);
                    throw e;
                }
            }
        }
        /// <summary>
        /// Release 메서드에서 사용하기 위한 인스턴스 함수로서
        /// 유효한 유저와 디바이스 정보를 foreach시켜준다.
        /// </summary>
        void ForeachMemberDevices(ISession db, GameSession session, Action<Device> act)
        {
            GameSessionUser user;
            if (session.members != null)
            {
                for (int i = 0; i < session.members.Count; i++)
                {
                    
                    user = session.members[i];
                    if (user.device != null)
                    {
                        //user.device.State = DeviceState.Standby;
                        //db.Update(user.device);
                        log.Debug($"Delete Connect Properteis {session.SN}/{user.device.Name}");

                        act?.Invoke(user);
                    }
                    else
                    {
                        log.Debug($"Delete Connect Properteis Failed {session.SN}/{user.Name}");
                    }
                }
            }
        }

        /// <summary>
        /// 방의 모든 콘텐츠가 종료 됨을 알림
        /// </summary>
        public void RoomEnd(ISession db, GameSession session)
        {
            using (var t = db.BeginTransaction())
            {
                session.State = SessionState.GameEnding;
                db.Update(session);
                t.Commit();
            }
        }
    }
}
