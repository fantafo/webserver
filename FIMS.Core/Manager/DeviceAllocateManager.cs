﻿using FIMS.Modules;
using log4net;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;  //JavaScriptSerializer 사용

namespace FIMS
{
    /// <summary>
    /// 단말기가 할당되지 않은 게임세션을 찾아서 단말기를 자동으로 할당해줍니다.
    /// 만약 필요한 단말기가 4대인데, 가용 단말기가 3대라면 할당하지 않습니다.
    /// </summary>
    public class DeviceAllocater
    {
        #region General Methods
        static ILog log = LogManager.GetLogger(typeof(DeviceAllocater).Name);
        public bool IsRun { get; private set; }
        private Thread thread;
        //랜덤
        static public Random random = new Random();

        public void Start()
        {
            if (!IsRun)
            {
                IsRun = true;
                thread = new Thread(RunableDeviceConnector);
                thread.Name = "DeviceAllocate";
                thread.Start();
            }
        }

        public void Stop()
        {
            if (IsRun)
            {
                IsRun = false;
                thread.Join();
                thread = null;
            }
        }
        #endregion General Methods

        private void RunableDeviceConnector()
        {
            log.Info("Start DeviceAllocate Thread");
            while (IsRun)
            {
                Thread.Sleep(1000);
                try
                {
                    using (var db = Database.Open())
                    {
                        //게임이 진행되지 않은세션을 찾아서 작업
                        foreach (var sess in Fims.gameSessionManager.GetGameLaunchSessions(db))
                        {
                            AllocateDevices(db, sess);
                        }

                        foreach (var sess in Fims.gameSessionManager.GetGameReadySessions(db))
                        {
                            GameStartDevices(db, sess);
                        }

                    }
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
            log.Info("Stop DeviceAllocate Thread");
        }

        /// <summary>
        /// 해당 세션의 모든 유저가 로그인이 완료되었는지 확인.
        /// * 게임세션이 GameLaunch상태가 아닐 경우에 진행할 수 없습니다.
        /// </summary>
        public void AllocateDevices(ISession db, GameSession session)
        {
            if (session.State != SessionState.GameLaunch)
                return;

            using (var trans = db.BeginTransaction())
            {
                try
                {
                    //모든 디바이스의 로그인이 완료 되었으면, 세션을 GameReady 만든다.
                    for (int i = 0; i < session.members.Count; i++)
                    {
                        var dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);

                        if (dev.State == DeviceState.Allocated)
                        {
                            log.Debug($"Allocate/{session.SN} {session.members[i].device} 캐릭터 생성");
                            Member member = new Member();
                            member.Gender = (GenderType)Member.random.Next(0, Enum.GetNames(typeof(GenderType)).Length + 1);

                            //현재 선생님 캐릭터는 남성뿐.
                            if (i == 0)
                                member.Gender = GenderType.Male;

                            //RoomNum은 게임서버로 부터 받고, 시작값이 1
                            //게임서버와 연결이 안될경우 기본값 0이 들어간다.
                            if(session.RoomNum != 0)
                                ChangeName(session, dev, member);

                            dev.State = DeviceState.Ready;
                            //db.Update(session.members[i].device);
                        }
                    }

                    for (int i = 0; i < session.members.Count; i++)
                    {
                        var dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                        if (dev.State != DeviceState.Ready)
                            return;
                    }

                    session.State = SessionState.GameReady;
                    db.Update(session);
                    trans.Commit();
                    log.Debug($"Allocate/{session.SN}: 이 준비됐습니다.");
                    return;
                }
                catch (Exception e)
                {
                    log.Error($"Allocate/{session.SN}: {e.Message}", e);
                    trans.Rollback();
                    return;
                }
                
            }
        }


        public void GameStartDevices(ISession db, GameSession session)
        {
            //모든 디바이스가 count 룸이라면.. (ReadyRoom)
            for (int i = 0; i < session.members.Count; i++)
            {
                Device dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                //앱이 실행중이지만 Live 파일 생성 날짜가 1970년으로 잡혀 Live값이 false로 온다.
                //if (!dev.Live || !session.members[i].Connected)
                if (!session.members[i].Connected)
                {
                    return;
                }
            }

            Fims.gameSessionManager.Start(db, session);
            for (int i = 0; i < session.members.Count; i++)
            {
                Device dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                dev.State = DeviceState.Play;
            }
        }

        // 이름을 Common서버로 전달
        public virtual dynamic ChangeName(GameSession session, Device device, Member member)
        {
            switch (session.game.Name)
            {
                //head, body값이 클라에서 이렇게 정해놔서.. 나중에 기회되면 바꾸세요
                //member.Gender는 랜덤값이고 AllocateDevices에서 설정해줌
                case "Safety":
                    if(member.Gender == GenderType.Male)
                    {
                        member.AvatarHead = 3;
                        member.AvatarBody = 1;
                    }else if(member.Gender == GenderType.Female)
                    {
                        member.AvatarHead = 1;
                        member.AvatarBody = 2;
                    }   
                    break;

                case "Earth":
                default:
                    if (member.Gender == GenderType.Male)
                    {
                        member.AvatarHead = 1;
                        member.AvatarBody = 1;
                    }
                    else if (member.Gender == GenderType.Female)
                    {
                        member.AvatarHead = 2;
                        member.AvatarBody = 2;
                    }
                    break;

            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string body = serializer.Serialize(ToSerializeName(session, device, member));
            try
            {
                log.Debug($"ChangeName/ Gender : {member.Gender} , head : { member.AvatarHead} , body : {member.AvatarBody} ");
                HttpHelper.RecordLog = true;
                return HttpHelper.PostJSON(session, $"ChangeName", body);
            }
            finally
            {
                HttpHelper.RecordLog = false;
            }
        }

        public virtual object ToSerializeName(GameSession session, Device device, Member member)
        {
            dynamic result = new
            {
                RoomNum = session.RoomNum,
                DeviceSN = device.SN,
                DeviceName = device.Name,
                Name = device.UserName,
                //Head = (short)member.Gender,
                //Body = (short)member.Gender
                Head = member.AvatarHead,
                Body = member.AvatarBody
            };
            return result;
        }
    }
}
