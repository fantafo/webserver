﻿using FIMS.Manager;
using FIMS.Modules;
using log4net;
using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace FIMS
{
    /// <summary>
    /// 시스템 내부의 각 시스템에서 사용할 변수들입니다.
    /// </summary>
    public static class FimsSetting
    {
        static Properties setting;
        public static void Load(Properties prop)
        {
            setting = prop;

            // Properties에서 정보를 불러와 입력합니다.
            UseAgent = setting.GetBool("cmd.enable", UseAgent);
            UseAgentPublicIP = setting.GetBool("cmd.publish", UseAgentPublicIP);
            AgentServerPort = setting.GetInt("cmd.port", AgentServerPort);
            AgentServerPortRange = setting.GetInt("cmd.port.range", AgentServerPortRange);

            AgentRegistTimeout = setting.GetInt("cmd.regist.timeout", AgentRegistTimeout);
            AgentReceiveTimeout = setting.GetInt("cmd.receive.timeout", AgentReceiveTimeout);
            AgentSendTimeout = setting.GetInt("cmd.send.timeout", AgentSendTimeout);
            AgentPingSkipError = setting.GetInt("cmd.pingSkipError", AgentPingSkipError);
            AgentPingTiming = setting.GetInt("cmd.pingTiming", AgentPingTiming);

            HttpTimeout = setting.GetInt("http.timeout", HttpTimeout);
            HttpTimeoutScale = setting.GetFloat("http.timeoutScale", HttpTimeoutScale);

            LogAutoFlush = setting.GetBool("log.flush", LogAutoFlush);

            //업데이트 가능한 확장자, 새로운 확장자 추가시 iis 확장자 등록해야됨
            string extStr = setting.GetString("update.ext");
            string[] ext = extStr.Split(' ');
            for(int i=0; i< ext.Length; i++)
            {
                UpdateExtension.Add(ext[i]);
            }
            extStr = string.Join("  ", UpdateExtension.ToArray());

            //업데이트시 설치되는 경로
            InstallPath = setting.GetString("update.path", InstallPath);
            isftpLocalServer = setting.GetBool("update.local", isftpLocalServer);

            // 불러온 정보들을 로그에 기록합니다.
            ILog log = LogManager.GetLogger("FimsSetting");
            log.Debug($"LogAutoFlush         : {LogAutoFlush}");
            log.Debug($"UseAgent             : {UseAgent}");
            log.Debug($"UseAgentPublicIP     : {UseAgentPublicIP}");
            log.Debug($"AgentServerPort      : {AgentServerPort}");
            log.Debug($"AgentServerPortRange : {AgentServerPortRange}");
            log.Debug($"AgentRegistTimeout   : {AgentRegistTimeout}");
            log.Debug($"AgentReceiveTimeout  : {AgentReceiveTimeout}");
            log.Debug($"AgentSendTimeout     : {AgentSendTimeout}");
            log.Debug($"AgentPingSkipError   : {AgentPingSkipError}");
            log.Debug($"AgentPingTiming      : {AgentPingTiming}");
            log.Debug($"HttpTimeout          : {HttpTimeout}");
            log.Debug($"HttpTimeoutScale     : {HttpTimeoutScale}");
            log.Debug($"UpdateExtension      : {extStr}");
            log.Debug($"UpdateInstallPath    : {InstallPath}");
            log.Debug($"isftpLocalServer     : {isftpLocalServer}");

        }

        public static bool LogAutoFlush = true;

        public static bool UseAgent = true;
        //클라우드 true, 로컬 false
        public static bool UseAgentPublicIP = false;

        // AgentServer를 어느정도 여유포트를 가질지에 대해서 정의합니다. AgentServerPort(40000)일 경우 40000~41000까지의 여유포트를 검색하여 할당합니다.
        // FIMS에서 구동되는 모든 서비스들은 40000~41000포트를 사용하고있으며, Web시스템 디버그 종료시 제대로 소켓이 종료되지않아서 단일 포트가 유효포트로 작동하지 않을 가능성이 많아 설계하게됐습니다.
        public static int AgentServerPort = 40000;
        public static int AgentServerPortRange = 1000;

        // 에이전트가 서버에 인증을 받기까지의 Timeout 시간입니다. 이 시간을 넘겼다면 네트워크상의 전송속도 문제일 가능성이 큽니다. (이걸 늘린다고해서 안전성이 늘어날 가능성은 크게 없습니다)
        public static int AgentRegistTimeout = 5000;
        // 에이전트가 Timeout까지 아무것도 반환하지 않는다면 에이전트는 종료된것으로 판정합니다. (이걸 늘린다고해서 안전성이 늘어날 가능성은 크게 없습니다)
        public static int AgentReceiveTimeout = 30000;
        // 에이전트에게 데이터를 전송하는데 Timeout까지 전송할수없었다면 에이전트가 종료된것으로 판정합니다. (이걸 늘린다고해서 안전성이 늘어날 가능성은 크게 없습니다)
        public static int AgentSendTimeout = 30000;
        // 에이전트에서 PingPacket이 연속으로 10번 이상 돌아오지 않으면 에이전트가 종료된것으로 판정합니다. (이걸 늘린다고해서 안전성이 늘어날 가능성은 크게 없습니다)
        public static int AgentPingSkipError = 10;
        // 에이전트에게 PingPacket을 몇초마다 보낼 것인지에 대한 ms 값입니다.
        public static int AgentPingTiming = 3000;
        
        // 시스템과 게임서버와의 통신시간이 Timeout을 초과할경우 응답불가로 판정합니다.
        // 이 옵션은 Timeout을 직접입력하지 않았을경우의 기본 값입니다.
        public static int HttpTimeout = 1000;
        // 기본값을 포함한 모든 서버와의 통신 Timeout값을 scale합니다.
        // int overflow를 염두에두고 지정하십시오.
        public static float HttpTimeoutScale = 1;

        //옵저버 자동 시작
        public static bool ObserverAutoStart = false;

        //웹서버에 등록되는 확장자 리스트
        public static List<String> UpdateExtension = new List<string>();

        //파일 다운시, 경로
        public static string InstallPath = "/storage/emulated/0/Fantafo/";

        //파일서버 로걸 여부
        public static bool isftpLocalServer = true;
    }
}