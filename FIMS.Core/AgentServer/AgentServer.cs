﻿using FIMS.Utils;
using FreeNet;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FIMS.Manager
{
    /// <summary>
    /// 안드로이듸 Agent가 접속할 수 있는 서버입니다.
    /// 접속된 Agent는 AgentClient로 재가공됩니다.
    /// 
    /// AgentClient역시 AgentServer클래스에서 관리하고있습니다.
    ///
    /// 단말기가 접속하면 저장된 UUID를 보내옵니다.
    /// UUID로 저장된 디바이스 정보를 읽어오게되며 
    /// </summary>
    public class AgentServer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                    STATIC VARIABLES
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        static ILog log = LogManager.GetLogger(typeof(AgentServer).Name);



        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                    MEMBER VARIABLES
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // 커맨드 서버 포트
        public int Port { get; private set; }

        //접속된 클라이언트 관리
        static public List<AgentClient> clientList = new List<AgentClient>();

        // 커맨드 목록.. 해당 키값으로 여러가지 함..
        public Dictionary<int, AgentClient> clients = new Dictionary<int, AgentClient>();

        // 활동 플래그
        public bool IsRun { get; private set; }

        /// <summary>
        /// SN을 기준으로 클라이언트를 가져온다.
        /// </summary>
        public AgentClient GetClient(int SN)
        {
            AgentClient client;
            lock (clients)
            {
                if (clients.TryGetValue(SN, out client))
                {
                    return clients[SN];
                }
            }
            return null;
        }

        /// <summary>
        /// 서버 IP주소를 가져온다.
        /// </summary>
        public string IpAddress
        {
            get
            {
                if (this.IsRun)
                {
                    if (FimsSetting.UseAgentPublicIP)
                    {
                        return $"{IpHelper.PublicIP}:{Fims.agentServer.Port}";
                    }
                    else
                    {
                        return $"{IpHelper.InnerIP}:{Fims.agentServer.Port}";
                    }
                }

                return "x";
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                     MANAGER LIFECYCLE METHODS
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////


        public void Start()
        {
            log.Info("Force Start");

            // 전체 에이전트/클라이언트 초기화
            ClearAgents();
          
            if (!IsRun)
            {
                //FreeNet
                CNetworkService service = new CNetworkService(false);
                // 콜백 매소드 설정.
                service.session_created_callback += on_session_created;
                service.session_Ready_callback += on_request_uuid;
                // 초기화.
                service.initialize(10000, 1024);

                // 세팅에서 포트를 가져온다.
                // 바인드할 수 있는 Tcp서버포트를 검색해오고
                // 에러가 발생할 시에는 다시 포트를 잡는다.
                lock (IpHelper.SERVER_BIND_LOCK_OBJECT)
                {
                    while (true)
                    {
                        try
                        {
                            Port = IpHelper.AvailablePortRange(FimsSetting.AgentServerPort, FimsSetting.AgentServerPort + FimsSetting.AgentServerPortRange);

                            service.listen("0.0.0.0", Port, 100);
                            break;
                        }
                        catch { }
                    }
                    log.Info($"Bind CommandLine Listener *:{Port}");
                }
                IsRun = true;
            }
        }

        public void Stop()
        {
            if (IsRun)
            {
                log.Info("Force Stop");
                IsRun = false;

                // 서버 종료
                try
                {
                    //TODO..start에서 FreeNet설정한것 해제해주기
                    //service
                }
                catch { }

                // 클라이언트 해제
                ClearAgents(true);

                log.Debug("Force Stop Done");
            }
        }



        /// <summary>
		/// 클라이언트가 접속 완료 하였을 때 호출됩니다.
		/// n개의 워커 스레드에서 호출될 수 있으므로 공유 자원 접근시 동기화 처리를 해줘야 합니다.
		/// </summary>
		/// <returns></returns>
		void on_session_created(CUserToken token, Socket socket)
        {
            AgentClient client = new AgentClient(token, socket);
            lock (clientList)
            {
                clientList.Add(client);
            }

        }

        void on_request_uuid(CUserToken token)
        {
            //uuid 요청
            CPacket msg = CPacket.create((short)PROTOCOL.S_DEVICE_REGISTRATION);
            token.send(msg);
        }

        public static void remove_user(AgentClient client)
        {
            lock (clientList)
            {
                clientList.Remove(client);
            }
        }

        /// <summary>
        /// 매니저가 시작되기 전이나 매니저가 종료될때 데이터 초기화를 위해 사용합니다.
        /// DB의 접속정보나 
        /// </summary>
        public void ClearAgents(bool join = false)
        {
            lock (clients)
            {
                log.Debug("Clear Agents");
                {
                    var list = DeviceDao.Get.ToEnableList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].IsAgent = false;
                    }
                }

                for (var e = clients.GetEnumerator(); e.MoveNext();)
                {
                    e.Current.Value.Close(join);
                }
                clients.Clear();
                log.Debug("Clear Agents Done");
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                     Network Lifecycle
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>
        /// 현재 방에 접속해 있는 상태일경우
        /// 방에 따른 행동을 취한다.
        /// </summary>
        public static void ApplyClientState(AgentClient client, ref Device dev)
        {
            using (var db = Database.Open())
            {
                var session = GameSessionDao.Get(db).FindCurrentLaunchedSession(dev.SN);
                if (session != null)
                {
                    int sn = dev.SN;
                    var user = (from mem in session.members
                                where mem.device.SN == sn
                                select mem).First();

                    switch (session.State)
                    {
                        case SessionState.Allocating:
                        case SessionState.Standby:
                            dev.Alloc(session);
                            break;
                        case SessionState.GameLaunch:
                        case SessionState.GameReady:
                        case SessionState.GameRun:
                            dev.Launch(session, user);
                            break;
                        case SessionState.GameEnd:
                        case SessionState.Returned:
                            dev.Unalloc();
                            break;
                    }
                }
                else
                {
                    dev.Unalloc();
                    dev.DisplayLock();
                    dev = DeviceDao.Get.DeviceGet(dev.SN);
                    dev.State = DeviceState.Standby;
                }
            }
        }
    }
}
