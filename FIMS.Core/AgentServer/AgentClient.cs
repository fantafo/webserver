﻿using FIMS.Utils;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FreeNet;

namespace FIMS.Manager
{
    //S_ 서버가 보내는 메세지, C_ 클라가 보내는 메세지
    public enum PROTOCOL : short
    {
        BEGIN = 0,
        
        S_STRING_MSG = 1,

        S_DEVICE_REGISTRATION = 2,

        S_DEVICE_INFO = 3,

        C_STRING_MSG = 101,

        END
    }

    //ftp 업데이트 오류코드
    public enum UPDATE_ERROR_CODE : int
    {
        NONE = 0, //오류 없음
        ERROR = -1,    //오류 발생
        STOP = -2,   //업데이트 중지 이벤트
    }

    //ftp 파일서버로 다운시 오류코드
    public enum DOWNLOAD_FILE_ERROR_CODE : int
    {
        EXIST = -2, //이미 존재하는 파일
        NOT_ENOUGH_STORAGE = -3,    //저장공간 부족
        STOP = -4,   //업데이트 중지 이벤트
    }

    /// <summary>
    /// 에이전트 클라이언트와 연결 돼 있는 클래스.
    /// 에이전트에게 명령을 내리거나 
    /// 에이전트에서의 핑, 서버에서의 핑을 체크하여 연결가능 상태를 지속적으로 확인한다.
    /// 
    /// 에이전트 클라이언트와 에이전트 서버는 지속적으로 서로에게 핑을 보낸다.
    /// 0.5~1초 사이에 한개씩 전달하게되며,
    /// 서버에서는 Agent로부터 10초이상 핑을 받지 못하면 연결이 끊긴것으로 간주한다.
    /// </summary>
    public class AgentClient : IPeer
    {
        public class SocketDisconnectedExcpeion : IOException
        {
            public SocketDisconnectedExcpeion(string msg) : base(msg) { }
        }
        static int CLIENT_ID_GEN = 0;
        public int ClientID = ++CLIENT_ID_GEN;

        // 필수 변수
        public int SN { get; private set; }
        public string IPAddress { get; private set; }
        public string Name { get; private set; }

  
        // 네트워크 변수
        //FreeNet
        CUserToken token;
        public ILog log;

        // 데이터 변수
        ResultData resultData = new ResultData();
        Queue<string> buffer = new Queue<string>();

        //랜덤
        static public Random random = new Random();
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                             CONSTRUCTOR
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        public AgentClient(CUserToken token, Socket socket)
        {
            string ip = ((IPEndPoint)socket.RemoteEndPoint).Address.MapToIPv4().ToString();
            if (!ip.IsEmpty())
                this.IPAddress = ip;

            this.token = token;
            this.token.set_peer(this);

            this.log = LogManager.GetLogger($"Cmd/{IPAddress}/{ClientID}");
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                      LIFE-TIME MANAGE METHODS
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public void Init(int sn)
        {
            this.SN = sn;
            this.log = LogManager.GetLogger($"Cmd{SN}/{IPAddress}/{ClientID}");
        }

        public void Close(bool join = false, bool bLog = true)
        {
            Disconnect();
            if (bLog)
                log?.Info($"Close");
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                      OVERRIDE NETWORK METHOD
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public ResultData Command(string cmd)
        {
            CPacket msg = CPacket.create((short)PROTOCOL.S_STRING_MSG);
            msg.push(cmd);
            send(msg);
            return null;
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                              THREADS                
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        void IPeer.on_message(CPacket msg)
        {
            PROTOCOL protocol = (PROTOCOL)msg.protocol_id;
            switch (protocol)
            {
                case PROTOCOL.C_STRING_MSG:
                    {
                        //데이터 인코딩
                        string line = Encoding.UTF8.GetString(msg.buffer, 6, msg.buffer.Length-6);
                        resultData.Parse(line);
                        string title = resultData.Title;
                        if (title == "device_registration")
                        {
                            var dev = DeviceRegistration(resultData);
                            if (dev == null)
                                return;
                            RequestDeviceInfo();
                            AgentServer.ApplyClientState(this, ref dev);
                        }
                        else if(title == "device_info")
                        {
                            DeviceInfo(resultData);
                        }
                        //다운로드 경과 전달
                        else if(title == "updateFile_info")
                        {
                            DownloadFileInfo(resultData);
                        }
                        //기기가 가지고 있는 파일 정보
                        else if(title == "deviceDetail_info")
                        {
                            DeviceDetailInfo(resultData);                            
                        }
                        //업데이트 중지
                        else if(title == "StopUpdate")
                        {
                            StopUpdateInfo();
                        }
                        //업데이트 작업 완료
                        else if(title == "EndUpdate")
                        {
                            EndUpdateInfo();
                        }
                        //삭제한 파일
                        else if(title == "DeleteFile_info")
                        {
                            DeleteFileInfo(resultData);
                        }
                        break;
                    }
                default:
                    log?.Error("on_message PROTOCOL default : " + protocol);
                    break;
            }
            
        }

        void IPeer.on_removed()
        {
            AgentServer.remove_user(this);
            if(SN != 0)
            {
                Device dev = DeviceDao.Get.DeviceGet(SN);
                dev.IsAgent = false;
                log.Debug("agent false");
                Fims.agentServer.clients.Remove(SN);
            }
        }

        public void Disconnect()
        {
            //token.close 진행시, 서버가 클라에게 -1 패킷을 받은것처럼 꾸며서 처리한다.
            //IPeer.on_removed() 진행
            token.close();
        }

        public void send(CPacket msg)
        {
            msg.record_size();
            this.token.send(new ArraySegment<byte>(msg.buffer, 0, msg.position));
        }

        void IPeer.disconnect()
        {
            this.token.ban();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        ///
        //    클라이언트 메세지 해석
        /// 
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //디바이스 등록
        public Device DeviceRegistration(ResultData resultData)
        {
            string uuid = resultData.GetString("uuid", "");
            if (uuid.IsEmpty())
                return null;
            Device dev = DeviceDao.Get.FindByUUID(uuid);
            
            if (dev == null)
            {
                //http AgentConnect를 통해 이미 uuid를 등록 하기 때문에
                //없을 이유가 없다.
                return null;
            }
            if(dev.IsAgent)
            {
                log.Debug("dev IsAgent True.. disconnect()");
                Disconnect();
            }

            dev.IsAgent = true;
            Init(dev.SN);
            lock (Fims.agentServer.clients)
            {
                if (Fims.agentServer.clients.Get(dev.SN) != null)
                    Fims.agentServer.clients.Remove(dev.SN);

                Fims.agentServer.clients.Add(dev.SN, this);
            }
            return dev;

        }


        //DeviceInfo 정보 요청
        public void RequestDeviceInfo()
        {
            CPacket msg = CPacket.create((short)PROTOCOL.S_DEVICE_INFO);
            //30초 마다 정보 요청
            byte send_interval = 30;
            msg.push(send_interval);
            token.send(msg);
        }

        //디바이스 정보, 해당 정보는 일정 주기로 받는다.
        public void DeviceInfo(ResultData resultData)
        {
            if (SN == 0)
            {
                log?.Error("device_info packet : sn = 0");
                return;
            }

            byte Battery = (byte)Math.Max(resultData.GetInt("bat"), 0);
            byte Thermal = (byte)Math.Max(resultData.GetInt("therm"), 0);
            bool Cont = (resultData.GetInt("cont", 0) == 1);
            bool Live = (resultData.GetInt("live", 0) == 1);
            var dev = DeviceDao.Get.DeviceGet(SN);
            dev.Battery = Battery;
            dev.Thermal = Thermal;
            dev.Cont = Cont;
            dev.Live = Live;
        }

        //다운로드 정보, 해당 정보는 다운로드시 일정 주기로 받는다.
        public void DownloadFileInfo(ResultData resultData)
        {
            if (SN == 0)
            {
                log?.Error("download_FileInfo packet : sn = 0");
                return;
            }

            string name = resultData.GetString("name", "");
            if (name.IsEmpty())
            {
                log?.Error("download_File name Empty");
                return;
            }

            /* downloadsize 상태값
             * -2 : 이미 존재하는 파일
             * -3 : 저장공간 부족으로 실패
             */
            long downloadsize = Convert.ToInt64(resultData.GetString("size", ""));

            var updev = UpdateDeviceDao.Get.FindBySN(SN);
            int index = updev.detail.FindIndex(x => x.name == name);
            if(index != -1)
            {
                updev.isDownloadStart = true;
                updev.detail[index].downloadsize = downloadsize;
                //다운완료시
                if (updev.detail[index].totalsize == downloadsize)
                {
                    updev.detail[index].success = true;
                    updev.detail[index].isDownloadStart = false;
                    updev.downloadCount++;
                }
                else if(downloadsize > 0)
                {
                    updev.detail[index].isDownloadStart = true;
                }
                //음수일 경우 상태값
                else if( downloadsize < 0)
                {
                    //진행도를 100%로 변경하기 위해서.
                    updev.detail[index].downloadsize = 1;
                    updev.detail[index].totalsize = 1;
                    
                    updev.downloadCount++;
                    //진행도를 변경한다.
                    updev.detail[index].totalsize = updev.detail[index].downloadsize = 1;

                    switch (downloadsize)
                    {
                        case (long)DOWNLOAD_FILE_ERROR_CODE.EXIST:
                            {
                                updev.detail[index].error = (int)DOWNLOAD_FILE_ERROR_CODE.EXIST;
                                break;
                            }
                        case (long)DOWNLOAD_FILE_ERROR_CODE.NOT_ENOUGH_STORAGE:
                            {
                                updev.error = (int)UPDATE_ERROR_CODE.ERROR;
                                updev.detail[index].error = (int)DOWNLOAD_FILE_ERROR_CODE.NOT_ENOUGH_STORAGE;
                                break;
                            }

                    }
                }
            }
        }

        //디바이스가 가진 세부 정보를 받는다.
        //가지고 있는 파일, 총용량, 사용가능용량
        public void DeviceDetailInfo(ResultData resultData)
        {
            if (SN == 0)
            {
                log?.Error("download_FileInfo packet : sn = 0");
                return;
            }

            //result 형식
            //name=파일명\파일명2\파일명3
            string[] name = resultData.GetString("name", "").Split('\\');
            string totalStorageSize = resultData.GetString("totalSize", "");
            string availableStorageSize = resultData.GetString("availableSize", "");
            
            if (name.IsEmpty())
            {
                return;
            }
            
            var updev = UpdateDeviceDao.Get.FindBySN(SN);
            updev.totalStorageSize = Int64.Parse(totalStorageSize);
            updev.availableStorageSize = Int64.Parse(availableStorageSize);

            for (int i=0; i< name.Count(); ++i)
            {
                if (name[i].IsEmpty())
                    continue;

                UpdateFileDetailInfo fileinfo = updev.detail.Find(x => x.name == name[i]);
                if (fileinfo != null)
                    continue;

                fileinfo = new UpdateFileDetailInfo();
                fileinfo.name = name[i];
                fileinfo.downloadsize = 1;
                fileinfo.totalsize = 1;
                fileinfo.error = 0;
                fileinfo.success = true;
                fileinfo.isDownloadStart = false;

                updev.detail.Add(fileinfo);
            }
        }

        //다운 진행, 대기 상태파일들을 모두 멈춤상태로 변경
        public void StopUpdateInfo()
        {
            var updev = UpdateDeviceDao.Get.FindBySN(SN);
            //다운중, 대기중인 파일은 모두 중지로 변경. 오류는 유지
            for(int i=0; i<updev.detail.Count; ++i)
            {
                if(updev.detail[i].error > 0)
                {
                    continue;
                }
                else if(updev.detail[i].success == false)
                {
                    updev.detail[i].error = (int)DOWNLOAD_FILE_ERROR_CODE.STOP;
                    updev.downloadCount++;
                }
            }
            updev.error = (int)UPDATE_ERROR_CODE.STOP;

        }

        //업데이트 종료
        public void EndUpdateInfo()
        {
            var updev = UpdateDeviceDao.Get.FindBySN(SN);
            updev.completed = true;
            //전체 진행도를 100으로 변경
            updev.downloadCount = updev.totalDownloadCount;

        }

        //삭제한 파일 정보
        public void DeleteFileInfo(ResultData resultData)
        {
            string name = resultData.GetString("name", "");
            var updev = UpdateDeviceDao.Get.FindBySN(SN);
            
            if(updev != null)
            {
                updev.detail.RemoveAll(x => x.name == name);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        ///
        //    ResultData
        /// 
        /// 커맨드를 보내고 받은데이터를 언팩한 데이터 컨테이너.
        /// 
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        public class ResultData : Dictionary<string, string>
        {
            public string RawData { get; private set; }
            public string Title { get; private set; }
            public bool IsSuccess => (Title == "OK");

            public ResultData Parse(string text)
            {
                Clear();
                RawData = text;

                int titleIndexOf = text.IndexOf(' ');
                if (titleIndexOf == -1)
                {
                    Title = text;
                }
                else
                {
                    Title = text.Substring(0, titleIndexOf);
                    text = text.Substring(titleIndexOf + 1);

                    string[] args = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var arg in args)
                    {
                        if (!string.IsNullOrWhiteSpace(arg))
                        {
                            int splitIndex = arg.IndexOf('=');
                            if (splitIndex == -1)
                            {
                                Add(arg, null);
                            }
                            else
                            {
                                Add(arg.Substring(0, splitIndex), arg.Substring(splitIndex + 1));
                            }
                        }
                    }
                }
                return this;
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder(Title);
                sb.Append("(");
                foreach (var pair in this)
                {
                    sb.Append(pair.Key).Append(":").Append(pair.Value).Append(", ");
                }
                sb.Append(")");
                return sb.ToString();
            }
        }
    }
}
