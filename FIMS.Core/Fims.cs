﻿using FIMS.Manager;
using FIMS.Modules;
using log4net;
using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace FIMS
{
    /// <summary>
    /// FIMS 시스템의 싱글톤을 관리하고 있는 클래스다.
    /// Fims.cs에서 모든것을 시작하고 관리하게된다.
    /// 
    /// Fims.cs는 FIMS.Core의 핵심클래스입니다.
    /// FIMS.Core는 컨트롤 형태(Web, Window)에서 명령을 내릴때 실행을 하게되는 주체입니다.
    /// </summary>
    public static class Fims
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                    STATIC VARIABLES
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // logically constraints
        public static string ROOT_PATH = "D:/Publish"; // FIMS 시스템의 메인 폴더
        public static string CONFIG_PATH = ROOT_PATH + "/config"; // 설정 폴더

        // private
        private static ILog log;

        // public
        public static bool IsRun;
        public static Properties setting; // CONFIG_PATH/config.properties 에서 읽어온 옵션

        public static DeviceAllocater allocater;
        public static AgentServer agentServer;

        public static GameSessionManager gameSessionManager;
        public static GameDataCollecter gameDataCollecter;
        public static GameServerManager gameServerManager;



        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                    Constructor
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// 핌스 시스템을 시작합니다.
        /// 이곳에서 핌스의 기반기능들이 전체 동작하게됩니다.
        /// </summary>
        /// <param name="path"></param>
        public static void Start(string path)
        {
            if (IsRun) return;
            IsRun = true;

#if DEBUG
            // 메인 쓰레드 이름 재지정
            try
            {
                var thread = System.Threading.Thread.CurrentThread;
                if (thread != null)
                    thread.Name = "Main";
            }
            catch { }
#endif

            // 핌스 시스템 시작
            try
            {
                var beginTime = DateTime.Now;
                {
                    SetupPath(path);
                    SetupDefault();
                    SetupServices();
                    StartServices();
                }
                log.Info($"Load Complete {(DateTime.Now - beginTime).TotalSeconds:f3}ms");
            }
            catch (Exception e)
            {
                if (log != null)
                    log.Fatal(e);
                else
                    System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public static void Stop()
        {
            if (IsRun)
            {
                IsRun = false;
                log.Debug("###### Dispose ######");
                agentServer?.Stop();
                allocater?.Stop();
                gameDataCollecter?.Stop();

                log.Debug("###### Dispose-Complete ######");
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //
        //                                    SETUP METHODS 
        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void SetupPath(string path)
        {
            ROOT_PATH = path.Replace('\\', '/');
            CONFIG_PATH = path + "/config";
        }

        /// <summary>
        /// 시스템의 기반을 설정합니다.
        /// 로그, 설정, DB 등의 기본 정보를 설정하고,
        /// 미리 입력되거나 계산돼야 할 정보들을 설정하게됩니다.
        /// </summary>
        public static void SetupDefault()
        {
            // Setup Base Modules
            {
                // setup Log
                log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo($"{CONFIG_PATH}/log4net.xml"));
                log = LogManager.GetLogger(typeof(Fims));
                log.Info("############################################");
                log.Info("#                                          #");
                log.Info("#    FANTAFO INTERACTION MANAGED SYSTEM    #");
                log.Info("#                                          #");
                log.Info("############################################");
                log.Info($"ROOT_PATH: {ROOT_PATH}");

                // Setup Properties
                setting = new Properties();
                setting.Load(CONFIG_PATH + "/config.properties");
                FimsSetting.Load(setting);

                // Setup Hibernate
                Database.Initialize();

                //Exp.Load();
                log.Debug("Load 'Exp'");
            }
        }

        /// <summary>
        /// 시스템에서 동작할 백그라운드 서비스들을 생성하고
        /// 기본 설정들을 반영합니다.
        /// </summary>
        public static void SetupServices()
        {
            // 에이전트 서비스를 실행합니다.
            log.Debug("###### StartServices ######");
            if (FimsSetting.UseAgent)
            {
                agentServer = new AgentServer();
                log.Debug("Enable 'AgentService'");
            }
            else
            {
                log.Debug("Disable 'AgentService'");
                //using (var db = Database.Open())
                //using (var trans = db.BeginTransaction())
                {
                    foreach (var device in DeviceDao.Get.ToList())
                    {
                        device.IsAgent = false;
                        //db.Update(device);
                    }
                    //trans.Commit();
                }
            }

            gameSessionManager = new GameSessionManager();
            allocater = new DeviceAllocater();
            gameDataCollecter = new GameDataCollecter();
            gameServerManager = new GameServerManager();
        }

        /// <summary>
        /// 시스템의 서비스들을 실제로 시작합니다.
        /// </summary>
        public static void StartServices()
        {
            log.Debug("###### StartServices ######");
            agentServer?.Start();
            allocater?.Start();
            gameDataCollecter?.Start();
            gameServerManager?.Start();

            // Auto Log Flush
            if (FimsSetting.LogAutoFlush)
            {
                AutoLogFlush.Start();
                log.Debug("Enable 'LogFlush'");
            }
            else
            {
                log.Debug("Disable 'LogFlush'");
            }
        }

    }
}