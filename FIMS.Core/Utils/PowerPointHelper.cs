﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging;
using Drawing = DocumentFormat.OpenXml.Drawing;
using System.IO;
using System.Drawing;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using DocumentFormat.OpenXml.Validation;

namespace FIMS
{
    public class PowerPointHelper
    {
        public static List<miniCompletion> data;
        private static uint uniqueId;
        private static uint _uniqueId;
        public static string Test(string path, List<miniCompletion> datatable)
        {
            data = datatable;
            //FileStream file = File.Open(@"C:/Users/leon/Desktop/YunKyu/FIMS_v0.4/FIMS/FIMS.Web/file/certificate.pptx", FileMode.Open);
            //var presentationFile = @"C:/Users/leon/Desktop/YunKyu/FIMS_v0.4/FIMS/FIMS.Web/file/certificate.pptx";
            var presentationFile = path;
            //string saveFilepath = @"C:/Users/leon/Desktop/YunKyu/FIMS_v0.4/FIMS/FIMS.Web/file/" + DateTime.Now.ToString("yyyy년MM월dd일HH시mm분_") + "수료증.pptx";
            string saveFilepath = @"C:/Users/leon/Desktop/YunKyu/FIMS_v0.4/FIMS/FIMS.Web/file/" + DateTime.Now.ToString("yyyy년MM월dd일HH시mm분_") + "수료증.pptx";

            SaveAs(presentationFile, saveFilepath);
            replaceTextInSlide(saveFilepath);

            return saveFilepath;
            /*
            for(int i =0; i<data.Count-1; ++i)
            {
                Copy3(presentationFile, 1, saveFilepath);
                
            }*/
            //AddForm(saveFilepath, 100);
        }

        public static void SaveAs(string filePath, string new_filePath)
        {
            using (var destDoc = PresentationDocument.Open(filePath, false))
            {
                destDoc.SaveAs(new_filePath).Close();
            }
        }

        public static void AddForm(string new_filePath, int count)
        {

            using (var destDoc = PresentationDocument.Open(new_filePath, true))
            {
                PresentationPart presentationPart = destDoc.PresentationPart;
                Presentation presentation = presentationPart.Presentation;

                // Verify that the presentation is not empty.
                if (presentationPart == null)
                {
                    throw new InvalidOperationException("The presentation document is empty.");
                }

                var slideIds = presentationPart.Presentation.SlideIdList.ChildElements;
                string slidePartRelationshipId = (slideIds[0] as SlideId).RelationshipId;
                SlidePart slidePart = (SlidePart)presentationPart.GetPartById(slidePartRelationshipId);

                presentationPart.AddPart<SlidePart>(slidePart);
                for (uint i = 1; i < 10; ++i)
                {
                    SlideId slideId = new SlideId
                    {
                        Id = i,
                        RelationshipId = destDoc.PresentationPart.GetIdOfPart(slidePart)
                    };
                    presentation.SlideIdList.Append(slideId);
                }
                destDoc.PresentationPart.Presentation.Save();
            }
        }

        public static void Copy2(string sourcePresentationStream, uint copiedSlidePosition, string destPresentationStream)
        {
            copiedSlidePosition = 1;
            using (var destDoc = PresentationDocument.Open(destPresentationStream, true))
            {
                //var sourceDoc = PresentationDocument.Open(sourcePresentationStream, false);
                var destPresentationPart = destDoc.PresentationPart;
                var destPresentation = destPresentationPart.Presentation;

                //var sourcePresentationPart = sourceDoc.PresentationPart;
                //var sourcePresentation = sourcePresentationPart.Presentation;

                int copiedSlideIndex = (int)--copiedSlidePosition;

                //int countSlidesInSourcePresentation = sourcePresentation.SlideIdList.Count();
                int countSlidesInSourcePresentation = destPresentation.SlideIdList.Count();

                if (copiedSlideIndex < 0 || copiedSlideIndex >= countSlidesInSourcePresentation)
                    throw new ArgumentOutOfRangeException(nameof(copiedSlidePosition));

                SlideId copiedSlideId = destPresentationPart.Presentation.SlideIdList.ChildElements[copiedSlideIndex] as SlideId;
                SlidePart copiedSlidePart = destPresentationPart.GetPartById(copiedSlideId.RelationshipId) as SlidePart;
                SlidePart addedSlidePart = destPresentationPart.AddPart<SlidePart>(copiedSlidePart);

                //SlideMasterPart addedSlideMasterPart = destPresentationPart.AddPart(addedSlidePart.SlideLayoutPart.SlideMasterPart);

                SlideId slideId = new SlideId
                {
                    Id = 1,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlidePart)
                };

                SlideId aa = new SlideId((SlideId)copiedSlideId.Clone());
                // aa.InnerText = copiedSlideId.InnerText;
                destPresentation.SlideIdList.Append(aa);
                //destPresentation.SlideIdList.AppendChild(copiedSlideId);
                destDoc.PresentationPart.Presentation.Save();
            }
        }


        public static void Copy3(string sourcePresentationStream, uint copiedSlidePosition, string destPresentationStream)
        {
            copiedSlidePosition = 1;
            using (var destDoc = PresentationDocument.Open(destPresentationStream, true))
            {
                var sourceDoc = PresentationDocument.Open(sourcePresentationStream, false);
                var destPresentationPart = destDoc.PresentationPart;
                var destPresentation = destPresentationPart.Presentation;

                var sourcePresentationPart = sourceDoc.PresentationPart;
                var sourcePresentation = sourcePresentationPart.Presentation;

                int copiedSlideIndex = (int)--copiedSlidePosition;

                int countSlidesInSourcePresentation = sourcePresentation.SlideIdList.Count();
                if (copiedSlideIndex < 0 || copiedSlideIndex >= countSlidesInSourcePresentation)
                    throw new ArgumentOutOfRangeException(nameof(copiedSlidePosition));

                SlideId copiedSlideId = sourcePresentationPart.Presentation.SlideIdList.ChildElements[copiedSlideIndex] as SlideId;
                SlidePart copiedSlidePart = sourcePresentationPart.GetPartById(copiedSlideId.RelationshipId) as SlidePart;

                SlidePart addedSlidePart = destPresentationPart.AddPart<SlidePart>(copiedSlidePart);

                SlideMasterPart addedSlideMasterPart = destPresentationPart.AddPart(addedSlidePart.SlideLayoutPart.SlideMasterPart);

                SlideId slideId = new SlideId
                {
                    Id = 2,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlidePart)
                };
                destPresentation.SlideIdList.Append(slideId);

                destDoc.PresentationPart.Presentation.Save();
            }
        }
        

        public static void Copy6(string sourcePresentationStream, uint copiedSlidePosition, string destPresentationStream)
        {
            copiedSlidePosition = 1;

            using (var destDoc = PresentationDocument.Open(destPresentationStream, true))
            {
                var sourceDoc = PresentationDocument.Open(sourcePresentationStream, false);
                var destPresentationPart = destDoc.PresentationPart;
                var destPresentation = destPresentationPart.Presentation;

                _uniqueId = GetMaxIdFromChild(destPresentation.SlideMasterIdList);
                uint maxId = GetMaxIdFromChild(destPresentation.SlideIdList);

                var sourcePresentationPart = sourceDoc.PresentationPart;
                var sourcePresentation = sourcePresentationPart.Presentation;

                uint test = GetMaxIdFromChild(sourcePresentation.SlideMasterIdList);

                int copiedSlideIndex = (int)--copiedSlidePosition;

                int countSlidesInSourcePresentation = sourcePresentation.SlideIdList.Count();
                if (copiedSlideIndex < 0 || copiedSlideIndex >= countSlidesInSourcePresentation)
                    throw new ArgumentOutOfRangeException(nameof(copiedSlidePosition));

                SlideId copiedSlideId = sourcePresentationPart.Presentation.SlideIdList.ChildElements[copiedSlideIndex] as SlideId;
                SlidePart copiedSlidePart = sourcePresentationPart.GetPartById(copiedSlideId.RelationshipId) as SlidePart;

                SlidePart addedSlidePart = destPresentationPart.AddPart<SlidePart>(copiedSlidePart);

                SlideMasterPart addedSlideMasterPart = destPresentationPart.AddPart(addedSlidePart.SlideLayoutPart.SlideMasterPart);


                // Create new slide ID
                maxId++;
                SlideId slideId = new SlideId
                {
                    Id = maxId,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlidePart)
                };
                destPresentation.SlideIdList.Append(slideId);

                // Create new master slide ID
                _uniqueId++;
                SlideMasterId slideMaterId = new SlideMasterId
                {
                    Id = _uniqueId,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlideMasterPart)
                };
                destDoc.PresentationPart.Presentation.SlideMasterIdList.Append(slideMaterId);

                // change slide layout ID
                FixSlideLayoutIds(destDoc.PresentationPart);

                destDoc.PresentationPart.Presentation.Save();
            }
        }

        public static void Copy8(string destPresentationStream)
        {
            uint copiedSlidePosition = 1;

            using (var destDoc = PresentationDocument.Open(destPresentationStream, true))
            {
                var destPresentationPart = destDoc.PresentationPart;
                var destPresentation = destPresentationPart.Presentation;

                _uniqueId = GetMaxIdFromChild(destPresentation.SlideMasterIdList);
                uint maxId = GetMaxIdFromChild(destPresentation.SlideIdList);

                //var sourcePresentationPart = sourceDoc.PresentationPart;
                //var sourcePresentation = sourcePresentationPart.Presentation;


                int copiedSlideIndex = (int)--copiedSlidePosition;

                int countSlidesInSourcePresentation = destPresentation.SlideIdList.Count();
                if (copiedSlideIndex < 0 || copiedSlideIndex >= countSlidesInSourcePresentation)
                    throw new ArgumentOutOfRangeException(nameof(copiedSlidePosition));

                SlideId copiedSlideId = destPresentationPart.Presentation.SlideIdList.ChildElements[copiedSlideIndex] as SlideId;
                SlidePart copiedSlidePart = destPresentationPart.GetPartById(copiedSlideId.RelationshipId) as SlidePart;

                SlidePart addedSlidePart = destPresentationPart.AddPart<SlidePart>(copiedSlidePart);

                SlideMasterPart addedSlideMasterPart = destPresentationPart.AddPart(addedSlidePart.SlideLayoutPart.SlideMasterPart);


                // Create new slide ID
                maxId++;
                SlideId slideId = new SlideId
                {
                    Id = maxId,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlidePart)
                };
                destPresentation.SlideIdList.Append(slideId);

                // Create new master slide ID
                _uniqueId++;
                SlideMasterId slideMaterId = new SlideMasterId
                {
                    Id = _uniqueId,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlideMasterPart)
                };
                destDoc.PresentationPart.Presentation.SlideMasterIdList.Append(slideMaterId);

                // change slide layout ID
                FixSlideLayoutIds(destDoc.PresentationPart);

                destDoc.PresentationPart.Presentation.Save();
            }
        }

        private static uint GetMaxIdFromChild(OpenXmlElement el)
        {
            uint max = 1;
            foreach (var child in el.ChildElements)
            {
                var attribute = child.GetAttribute("id", "");
                var id = uint.Parse(attribute.Value);
                if (id > max) max = id;
            }
            return max;
        }


        

        
        
        static uint GetMaxSlideId(SlideIdList slideIdList)
        {
            // Slide identifiers have a minimum value of greater than or
            // equal to 256 and a maximum value of less than 2147483648. 
            uint max = 256;

            if (slideIdList != null)
                // Get the maximum id value from the current set of children.
                foreach (SlideId child in slideIdList.Elements<SlideId>())
                {
                    uint id = child.Id;

                    if (id > max)
                        max = id;
                }

            return max;
        }


        public static void replaceTextInSlide(string path)
        {
            using (var destDoc = PresentationDocument.Open(path, true))
            {
                PresentationPart presPart = destDoc.PresentationPart;
                int slideID = 0;
                Presentation presentation = presPart.Presentation;
                if (presentation.SlideIdList != null)
                {
                    // Get the collection of slide IDs from the slide ID list.
                    var slideIds = presentation.SlideIdList.ChildElements;
                    for (int i = 0; i < data.Count; ++i)
                    {
                        string slidePartRelationshipId = (slideIds[i] as SlideId).RelationshipId;
                        SlidePart slidePart = (SlidePart)presPart.GetPartById(slidePartRelationshipId);
                        string nowTime = DateTime.Now.ToString("yyyy년 MM월 dd일");
                        foreach (var paragraph in slidePart.Slide.Descendants<DocumentFormat.OpenXml.Drawing.Paragraph>())
                        {
                            string oldText, newText = null;

                            for (int j = 0; j < Enum.GetNames(typeof(Certificate)).Length; ++j)
                            {
                                oldText = Enum.GetName(typeof(Certificate), j);
                                //하드코드
                                switch (j)
                                {
                                    case 0:
                                        newText = data[i].Major;
                                        break;
                                    case 1:
                                        newText = data[i].Id;
                                        break;
                                    case 2:
                                        newText = data[i].Name;
                                        break;
                                    case 3:
                                        var date = Convert.ToDateTime(data[i].Date);
                                        newText = string.Format("{0}년 {1}월 {2}일", date.Year, date.Month, date.Day);

                                        break;
                                    case 4:
                                        newText = nowTime;
                                        break;
                                }
                                /*
                                miniCompletion changedobject = (miniCompletion)Activator.CreateInstance(typeof(miniCompletion));
                                changedobject.GetType().GetProperty(oldText).SetValue
                                var aa = changedobject.GetType().GetProperty(oldText).GetValue(data[i]);
                                newText = aa as string;
                                */
                                if (paragraph.InnerText.Contains(oldText))
                                {
                                    foreach (var child in paragraph.ChildElements)
                                    {
                                        if (child.InnerText.Equals(oldText))
                                        {
                                            DocumentFormat.OpenXml.Drawing.RunProperties styleRun = (DocumentFormat.OpenXml.Drawing.RunProperties)child.ChildElements[0].Clone();
                                            child.RemoveAllChildren();
                                            child.Append(styleRun);
                                            child.Append(new DocumentFormat.OpenXml.Drawing.Text(newText));
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }

        public static SlidePart CloneSlidePart(PresentationPart presentationPart, SlidePart slideTemplate)

        {

            //Create a new slide part in the presentation

            SlidePart newSlidePart = presentationPart.AddNewPart<SlidePart>("newSlide" + 1);
            
            //Add the slide template content into the new slide

            newSlidePart.FeedData(slideTemplate.GetStream(FileMode.Open));

            //make sure the new slide references the proper slide layout

            newSlidePart.AddPart(slideTemplate.SlideLayoutPart);

            //Get the list of slide ids

            SlideIdList slideIdList = presentationPart.Presentation.SlideIdList;

            //Figure out where to add the next slide (find max slide)

            uint maxSlideId = 1;

            SlideId prevSlideId = null;

            foreach (SlideId slideId in slideIdList.ChildElements)

            {

                if (slideId.Id > maxSlideId)

                {

                    maxSlideId = slideId.Id;

                    prevSlideId = slideId;

                }

            }

            maxSlideId++;

            //Add new slide at the end of the deck

            SlideId newSlideId = slideIdList.InsertAfter(new SlideId(), prevSlideId);

            //Make sure id and relid is set appropriately

            newSlideId.Id = maxSlideId;

            newSlideId.RelationshipId = presentationPart.GetIdOfPart(newSlidePart);

            return newSlidePart;

        }

        public static void Copy(string sourcePresentationStream, uint copiedSlidePosition, string destPresentationStream)
        {
            using (var destDoc = PresentationDocument.Open(destPresentationStream, true))
            {
                var sourceDoc = PresentationDocument.Open(sourcePresentationStream, false);
                var destPresentationPart = destDoc.PresentationPart;
                var destPresentation = destPresentationPart.Presentation;

                

                var sourcePresentationPart = sourceDoc.PresentationPart;
                var sourcePresentation = sourcePresentationPart.Presentation;

                int copiedSlideIndex = (int)--copiedSlidePosition;

                int countSlidesInSourcePresentation = sourcePresentation.SlideIdList.Count();
                if (copiedSlideIndex < 0 || copiedSlideIndex >= countSlidesInSourcePresentation)
                    throw new ArgumentOutOfRangeException(nameof(copiedSlidePosition));

                SlideId copiedSlideId = sourcePresentationPart.Presentation.SlideIdList.ChildElements[copiedSlideIndex] as SlideId;
                SlidePart copiedSlidePart = sourcePresentationPart.GetPartById(copiedSlideId.RelationshipId) as SlidePart;

                SlidePart addedSlidePart = destPresentationPart.AddPart<SlidePart>(copiedSlidePart);
                
                SlideId slideId = new SlideId
                {
                    Id = 1,
                    RelationshipId = destDoc.PresentationPart.GetIdOfPart(addedSlidePart)
                };
                destPresentation.SlideIdList.Append(slideId);

                // change slide layout ID
                FixSlideLayoutIds(destDoc.PresentationPart);

                destDoc.PresentationPart.Presentation.Save();

            }
            //sourcePresentationStream.Close();
            //destPresentationStream.Close();
        }

        static void FixSlideLayoutIds(PresentationPart presPart)
        {
            // Make sure that all slide layouts have unique ids.
            foreach (SlideMasterPart slideMasterPart in presPart.SlideMasterParts)
            {
                foreach (SlideLayoutId slideLayoutId in slideMasterPart.SlideMaster.SlideLayoutIdList)
                {
                    _uniqueId++;
                    slideLayoutId.Id = (uint)_uniqueId;
                }
                
                slideMasterPart.SlideMaster.Save();
            }
        }
        
        public static void CopySlideTo(string sourcePresentationPath, int slidePosition, string destPresentationPath)
        {
            using (PresentationDocument sourcePresentationDocument = PresentationDocument.Open(sourcePresentationPath, false))
            {
                var sourcePresentationPart = sourcePresentationDocument.PresentationPart;
                var sourcePresentation = sourcePresentationPart.Presentation;
                SlideIdList sourceSlideIdList = sourcePresentation.SlideIdList;
                SlideId slideIdSelectedSlide = sourceSlideIdList.ChildElements[slidePosition - 1] as SlideId;
                SlidePart sourceSlidePart = sourcePresentationPart.GetPartById(slideIdSelectedSlide.RelationshipId) as SlidePart;

                SlidePart destSlidePart = null;
                SlideIdList destSlideIdList = null;
                PresentationPart destPresentationPart = null;
                using (PresentationDocument destPresentationDocument = PresentationDocument.Open(destPresentationPath, true))
                {
                    var addedSlidePart = destPresentationDocument.PresentationPart.AddPart(sourceSlidePart);

                    destSlideIdList = destPresentationDocument.PresentationPart.Presentation.SlideIdList;

                    destPresentationPart = destPresentationDocument.PresentationPart;

                    SlideId lastSlideIdInDestPresentation = destSlideIdList.ChildElements.Last() as SlideId;

                    // Insert the new slide into the slide list after last slide 
                    SlideId addedSlideId = destSlideIdList.InsertAfter(new SlideId(), lastSlideIdInDestPresentation);
                    addedSlideId.Id = lastSlideIdInDestPresentation.Id++;
                    addedSlideId.RelationshipId = destPresentationPart.GetIdOfPart(addedSlidePart);

                    destPresentationPart.Presentation.Save();
                }

            }
        }
        
        // Insert a slide into the specified presentation.
        public static void InsertNewSlide(string presentationFile, int position)
        {
            // Open the source document as read/write. 
            using (PresentationDocument presentationDocument = PresentationDocument.Open(presentationFile, true))
            {
                // Pass the source document and the position and title of the slide to be inserted to the next method.
                InsertNewSlide(presentationDocument, position);
            }
        }

        // Insert the specified slide into the presentation at the specified position.
        public static void InsertNewSlide(PresentationDocument presentationDocument, int position)
        {
            //슬라이드 생성 및 속성 정의

            if (presentationDocument == null)
            {
                throw new ArgumentNullException("presentationDocument");
            }

            PresentationPart presentationPart = presentationDocument.PresentationPart;

            // Verify that the presentation is not empty.
            if (presentationPart == null)
            {
                throw new InvalidOperationException("The presentation document is empty.");
            }

            // Declare and instantiate a new slide.
            Slide slide = new Slide(new CommonSlideData(new ShapeTree()));
            var aa = presentationPart.SlideParts;


            uint drawingObjectId = 1;

            // Construct the slide content.            
            // Specify the non-visual properties of the new slide.
            P.NonVisualGroupShapeProperties nonVisualProperties = slide.CommonSlideData.ShapeTree.AppendChild(new P.NonVisualGroupShapeProperties());
            nonVisualProperties.NonVisualDrawingProperties = new P.NonVisualDrawingProperties() { Id = 1, Name = "" };
            nonVisualProperties.NonVisualGroupShapeDrawingProperties = new P.NonVisualGroupShapeDrawingProperties();
            nonVisualProperties.ApplicationNonVisualDrawingProperties = new ApplicationNonVisualDrawingProperties();

            // Specify the group shape properties of the new slide.
            slide.CommonSlideData.ShapeTree.AppendChild(new GroupShapeProperties());

            //새 슬라이드 파트를 만들고 슬라이드를 삽입 할 지정된 인덱스 위치를 찾은 다음 삽입하여 수정 된 프레젠테이션을 저장

            // Create the slide part for the new slide.
            SlidePart slidePart = presentationPart.AddNewPart<SlidePart>();

            // Save the new slide part.
            slide.Save(slidePart);

            // Modify the slide ID list in the presentation part.
            // The slide ID list should not be null.
            SlideIdList slideIdList = presentationPart.Presentation.SlideIdList;

            // Find the highest slide ID in the current list.
            uint maxSlideId = 1;
            SlideId prevSlideId = null;

            foreach (SlideId slideId in slideIdList.ChildElements)
            {
                if (slideId.Id > maxSlideId)
                {
                    maxSlideId = slideId.Id;
                }

                position--;
                if (position == 0)
                {
                    prevSlideId = slideId;
                }

            }

            maxSlideId++;

            // Get the ID of the previous slide.
            SlidePart lastSlidePart;

            if (prevSlideId != null)
            {
                lastSlidePart = (SlidePart)presentationPart.GetPartById(prevSlideId.RelationshipId);
            }
            else
            {
                lastSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[0])).RelationshipId);
            }

            // Use the same slide layout as that of the previous slide.
            if (null != lastSlidePart.SlideLayoutPart)
            {
                slidePart.AddPart(lastSlidePart.SlideLayoutPart);
            }

            // Insert the new slide into the slide list after the previous slide.
            SlideId newSlideId = slideIdList.InsertAfter(new SlideId(), prevSlideId);
            newSlideId.Id = maxSlideId;
            newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart);

            // Save the modified presentation.
            presentationPart.Presentation.Save();
        }

        // Get all the text in a slide.
        public static string[] GetAllTextInSlide(string presentationFile, int slideIndex)
        {
            // Open the presentation as read-only.
            using (PresentationDocument presentationDocument = PresentationDocument.Open(presentationFile, false))
            {
                // Pass the presentation and the slide index
                // to the next GetAllTextInSlide method, and
                // then return the array of strings it returns. 
                return GetAllTextInSlide(presentationDocument, slideIndex);
            }
        }
        public static string[] GetAllTextInSlide(PresentationDocument presentationDocument, int slideIndex)
        {
            // Verify that the presentation document exists.
            if (presentationDocument == null)
            {
                throw new ArgumentNullException("presentationDocument");
            }

            // Verify that the slide index is not out of range.
            if (slideIndex < 0)
            {
                throw new ArgumentOutOfRangeException("slideIndex");
            }

            // Get the presentation part of the presentation document.
            PresentationPart presentationPart = presentationDocument.PresentationPart;

            // Verify that the presentation part and presentation exist.
            if (presentationPart != null && presentationPart.Presentation != null)
            {
                // Get the Presentation object from the presentation part.
                Presentation presentation = presentationPart.Presentation;

                // Verify that the slide ID list exists.
                if (presentation.SlideIdList != null)
                {
                    // Get the collection of slide IDs from the slide ID list.
                    DocumentFormat.OpenXml.OpenXmlElementList slideIds =
                        presentation.SlideIdList.ChildElements;

                    // If the slide ID is in range...
                    if (slideIndex < slideIds.Count)
                    {
                        // Get the relationship ID of the slide.
                        string slidePartRelationshipId = (slideIds[slideIndex] as SlideId).RelationshipId;

                        // Get the specified slide part from the relationship ID.
                        SlidePart slidePart =
                            (SlidePart)presentationPart.GetPartById(slidePartRelationshipId);

                        // Pass the slide part to the next method, and
                        // then return the array of strings that method
                        // returns to the previous method.
                        return GetAllTextInSlide(slidePart);
                    }
                }
            }

            // Else, return null.
            return null;
        }
        public static string[] GetAllTextInSlide(SlidePart slidePart)
        {
            // Verify that the slide part exists.
            if (slidePart == null)
            {
                throw new ArgumentNullException("slidePart");
            }

            // Create a new linked list of strings.
            LinkedList<string> texts = new LinkedList<string>();

            // If the slide exists...
            if (slidePart.Slide != null)
            {
                // Iterate through all the paragraphs in the slide.
                foreach (DocumentFormat.OpenXml.Drawing.Paragraph paragraph in
                    slidePart.Slide.Descendants<DocumentFormat.OpenXml.Drawing.Paragraph>())
                {
                    // Create a new string builder.                    
                    StringBuilder paragraphText = new StringBuilder();

                    // Iterate through the lines of the paragraph.
                    foreach (DocumentFormat.OpenXml.Drawing.Text text in
                        paragraph.Descendants<DocumentFormat.OpenXml.Drawing.Text>())
                    {
                        // Append each line to the previous lines.
                        paragraphText.Append(text.Text);
                    }

                    if (paragraphText.Length > 0)
                    {
                        // Add each paragraph to the linked list.
                        texts.AddLast(paragraphText.ToString());
                    }
                }
            }

            if (texts.Count > 0)
            {
                // Return an array of strings.
                return texts.ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
