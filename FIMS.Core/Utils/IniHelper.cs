﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FIMS.Utils
{
    /// <summary>
    /// ini 설정 파일을 읽고 쓸 수 있습니다.
    /// </summary>
    public class IniHelper
    {
        [DllImport("kernel32.dll")]
        private static extern int GetPrivateProfileString(    // GetIniValue 를 위해
            String section,
            String key,
            String def,
            StringBuilder retVal,
            int size,
            String filePath);

        [DllImport("kernel32.dll")]
        private static extern long WritePrivateProfileString(  // SetIniValue를 위해
            String section,
            String key,
            String val,
            String filePath);


        public static string GetString(string path, string key, string def = "")
        {
            return GetString(path, "common", key, def);
        }
        public static string GetString(string path, string section, string key, string def = "")
        {
            StringBuilder temp = new StringBuilder(255);
            int i = GetPrivateProfileString(section, key, def, temp, 255, path);
            return temp.ToString();
        }
        public static int GetInt(string path, string key, int def = 0)
        {
            return GetInt(path, "common", key, def);
        }
        public static int GetInt(string path, string section, string key, int def = 0)
        {
            try
            {
                return int.Parse(GetString(path, section, key, ""));
            }
            catch
            {
                return def;
            }
        }

        public static void Set(string path, string key, object value)
        {
            Set(path, "common", key, value);
        }
        public static void Set(string path, string section, string key, object value)
        {
            WritePrivateProfileString(section, key, (value != null ? value.ToString() : ""), path);
        }

    }
}
