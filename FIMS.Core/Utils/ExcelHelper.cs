﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;       //microsoft Excel 14 object in references-> COM tab
using System.Windows.Forms;
using System.IO;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

using System.IO.Packaging;

namespace FIMS
{
    public class ExcelHelper
    {
        //엑셀파일 데이터 저장
        public object[,] data2;

        public List<Object[]> data = new List<Object[]>();

        //ReadExcelData가 종료시 true;
        public bool IsRun = false;

        
        public void ReadExcelData(string path)
        {

            IsRun = false;
            /*
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = "xlsx";
            openFile.Filter = "Excel Files(*.xlsx)|*.xlsx";
            DialogResult dresult = openFile.ShowDialog();
            if (dresult != DialogResult.OK)
            {
                return;
            }
            if (openFile.FileNames.Length > 0)
            {
                foreach (string filename in openFile.FileNames)
                {
                    //this.textBox1.Text = filename;
                }
            }
            */
            //string path = openFile.FileName;
            //string path = "";

            Excel.Application excelApp = null;
            Excel.Workbook wb = null;
            Excel.Worksheet ws = null;
            try
            {
                excelApp = new Excel.Application();
                wb = excelApp.Workbooks.Open(path);
                // path 대신 문자열도 가능합니다
                // 예. Open(@"D:\test\test.xslx");
                ws = wb.Worksheets.get_Item(1) as Excel.Worksheet;
                // 첫번째 Worksheet를 선택합니다.
                Excel.Range rng = ws.UsedRange;   // '여기'
                                                  // 현재 Worksheet에서 사용된 셀 전체를 선택합니다.
                data = rng.Value;
                
                wb.Close(true);
                excelApp.Quit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ReleaseExcelObject(ws);
                ReleaseExcelObject(wb);
                ReleaseExcelObject(excelApp);
                IsRun = true;
            }
        }

        static void ReleaseExcelObject(object obj)
        {
            try
            {
                if (obj != null)
                {
                    Marshal.ReleaseComObject(obj);
                    obj = null;
                }
            }
            catch (Exception ex)
            {
                obj = null;
                throw ex;
            }
            finally
            {
                GC.Collect();
            }
        }

        public void ReadExcelData2(string path)
        {
            try
            {
                //specify the file name where its actually exist   
                string filepath = path;

                //open the excel using openxml sdk  
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(filepath, false))
                {

                    //create the object for workbook part  
                    WorkbookPart wbPart = doc.WorkbookPart;

                    //statement to get the count of the worksheet  
                    int worksheetcount = doc.WorkbookPart.Workbook.Sheets.Count();

                    //statement to get the sheet object  
                    Sheet mysheet = (Sheet)doc.WorkbookPart.Workbook.Sheets.ChildElements.GetItem(0);

                    //statement to get the worksheet object by using the sheet id  
                    Worksheet Worksheet = ((WorksheetPart)wbPart.GetPartById(mysheet.Id)).Worksheet;

                    //Note: worksheet has 8 children and the first child[1] = sheetviewdimension,....child[4]=sheetdata  
                    int wkschildno = 4;


                    //statement to get the sheetdata which contains the rows and cell in table  
                    SheetData Rows = (SheetData)Worksheet.ChildElements.GetItem(wkschildno);


                    //getting the row as per the specified index of getitem method  
                    Row currentrow = (Row)Rows.ChildElements.GetItem(0);

                    //getting the cell as per the specified index of getitem method  
                    Cell currentcell = (Cell)currentrow.ChildElements.GetItem(0);

                    string currentcellvalue = string.Empty;


                    if (currentcell.DataType != null)
                    {
                        if (currentcell.DataType == CellValues.SharedString)
                        {
                            int id = -1;

                            if (Int32.TryParse(currentcell.InnerText, out id))
                            {
                                SharedStringItem item = GetSharedStringItemById(wbPart, id);

                                if (item.Text != null)
                                {
                                    //code to take the string value  
                                    currentcellvalue = item.Text.Text;
                                }
                                else if (item.InnerText != null)
                                {
                                    currentcellvalue = item.InnerText;
                                }
                                else if (item.InnerXml != null)
                                {
                                    currentcellvalue = item.InnerXml;
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception Ex)
            {

                //lbldisplayerrors.Text = Ex.Message;
            }
        }

        private SharedStringItem GetSharedStringItemById(WorkbookPart wbPart, int id)
        {
            return wbPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
        }

        public void ReadExcelData3(string path)
        {

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                {
                    WorkbookPart workbookPart = doc.WorkbookPart;
                    SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                    SharedStringTable sst = sstpart.SharedStringTable;

                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.FirstOrDefault();
                    Worksheet sheet = worksheetPart.Worksheet;

                    var cells = sheet.Descendants<Cell>();
                    var rows = sheet.Descendants<Row>();

                    int i = 0;
                    int j = 0;

                    foreach (Row row in rows)
                    {

                        //data.Add(new object[5]);
                        data.Add(new object[3]);
                        j = 0;

                        foreach (Cell c in row.Elements<Cell>())
                        {
                            if ((c.DataType != null) && (c.DataType == CellValues.SharedString))
                            {
                                int ssid = int.Parse(c.CellValue.Text);
                                data[i][j] = sst.ChildElements[ssid].InnerText;
                            }
                            else if (c.CellValue != null)
                            {
                                data[i][j] = c.CellValue.Text;
                            }
                            j++;
                        }
                        i++;
                    }
                }
            }
        }
    }
}
