﻿using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.Http;
using System;
using System.Net.NetworkInformation;
using System.Linq;

/// <summary>
/// 내부, 외부 아이피 혹은 도메인 등에 대한 유틸을 제공합니다.
/// </summary>
public static class IpHelper
{
    public static string InnerIP
    {
        get
        {
            //string localIP = "Not available, please check your network seetings!";
            string localIP = "127.0.0.1";
            
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                var addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();
                if (addr != null)
                {
                    if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                localIP = ip.Address.ToString();
                                break;
                            }
                        }
                    }
                }
            }
            //가상화 사용시 ip주소가 다수 나옴. 
            /*
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }*/
            return localIP;
        }
    }

    static string publicIP;
    static DateTime beforeCheckIP;
    public static string PublicIP
    {
        get
        {
            if (beforeCheckIP > DateTime.Now)
                return publicIP;
            using (HttpClient client = new HttpClient())
            {
                var task = client.GetStringAsync("http://checkip.dyndns.org");
                task.Wait();
                string[] a = task.Result.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');

                beforeCheckIP = DateTime.Now;
                return publicIP = a3[0].Trim();
            }
        }
    }

    public static string GetIpFromDomain(string domainName)
    {
        try
        {
            if (IsIpAddress(domainName))
                return domainName;

            var entry = Dns.GetHostEntry(domainName);
            if (entry != null)
            {
                for (int i = 0; i < entry.AddressList.Length; i++)
                {
                    if (entry.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        string resultIP = entry.AddressList[i].ToString();
                        switch (resultIP)
                        {
                            case "127.0.0.1":
                            case "0.0.0.0":
                            case "localhost":
                                break;
                            default:
                                return resultIP; 

                        }
                    }
                }
            }
            return InnerIP;
        }catch(Exception e)
        {
            return domainName;
        }
    }
    static bool IsIpAddress(string domain)
    {
        for(int i=0; i<domain.Length; i++)
        {
            switch (domain[i])
            {
                case '.':
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    break;
                default:
                    return false;
            }
        }
        return true;
    }

    public static object SERVER_BIND_LOCK_OBJECT = new object();
    public static bool AvailablePort(int port)
    {
        IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
        TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

        foreach (var tcpi in tcpConnInfoArray)
        {
            if (tcpi.LocalEndPoint.Port == port)
            {
                return false;
            }
        }
        return true;
    }

    static List<int> checkedAvailablePort = new List<int>();
    public static int AvailablePortRange(int portBegin, int portEnd)
    {
        // Evaluate current system tcp connections. This is the same information provided
        // by the netstat command line application, just in .Net strongly-typed object
        // form.  We will look through the list, and if our port we would like to use
        // in our TcpClient is occupied, we will set isAvailable to false.
        IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
        TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

        for (int i = portBegin; i < portEnd; i++)
        {
            if (checkedAvailablePort.Contains(i))
                continue;

            foreach (var tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == i)
                {
                    goto CONTINUE;
                }
            }
            checkedAvailablePort.Add(i);
            return i;

            CONTINUE:
            { }
        }
        return -1;
    }
}
