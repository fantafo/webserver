﻿using log4net;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FIMS
{
    /// <summary>
    /// Http 프로토콜을 이용하여 데이터를 읽어올 수 있습니다.
    /// Json형태의 데이터만 읽어올 수 있으며
    /// 결과값은 dynamic으로 출력됩니다.
    /// 
    /// 대부분의 대상이 내부환경인 점을 감안하여 timeout은 500ms로 설정 돼 있습니다.
    /// 만약 대상이 외부환경일 경우에는 알맞은 timeout을 지정해야할 것입니다.
    /// </summary>
    public class HttpHelper
    {
        public static bool RecordLog = false;

        public static ILog log = LogManager.GetLogger("HttpHelper");

        public static dynamic GetJSON(GameSession session, string url, int timeout = -1)
        {
            return GetHTTP(session.game, url, timeout);
        }
        public static dynamic GetHTTP(Game game, string url, int timeout = -1)
        {
            url = $"http://{game.ServerIP}:{game.ServerComsPort}/{url}";
            return Process(url, ((client, cancel) => client.GetAsync(url, cancel)), timeout);
        }
        public static dynamic PostJSON(GameSession session, string url, string content, int timeout = -1)
        {
            return Post(session.game, url, content, timeout);
        }
        public static dynamic Post(Game game, string url, string content, int timeout = -1)
        {
            url = $"http://{game.ServerIP}:{game.ServerComsPort}/{url}";
            if (RecordLog) log.Debug($"Process [" + content + "]");
            return Process(url, (client, cancel) =>
            {
                StringContent cnt = new StringContent(content, Encoding.UTF8);
                return client.PostAsync(url, cnt, cancel);
            }, timeout);
        }

        public static dynamic Process(string url, Func<HttpClient, CancellationToken, Task<HttpResponseMessage>> GetMsgTask, int timeout)
        {
            if (timeout == -1)
                timeout = FimsSetting.HttpTimeout;

            if (timeout < int.MaxValue * 0.49f)
                timeout = (int)(timeout * FimsSetting.HttpTimeoutScale);
            else
                timeout = int.MaxValue;

            // connect game server
            using (CancellationTokenSource cts = new CancellationTokenSource(timeout))
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    DateTime startTime = DateTime.Now;

                    if (RecordLog) log.Debug($"Make Task");
                    using (var msgTask = GetMsgTask(client, cts.Token))
                    {
                        var connection = msgTask.Result;

                        if (!connection.IsSuccessStatusCode)
                            throw new HttpServerBadResponseException(Uri.UnescapeDataString(connection.ReasonPhrase));

                        using (var task = connection.Content.ReadAsStringAsync())
                        {
                            dynamic result = new JavaScriptSerializer().Deserialize<dynamic>(task.Result);
                            //TODO: 추후 제거
                            //HACK: 이전버전의 호환성을 위해 남겨둔 코드
                            try
                            {
                                if (result["state"] != 0)
                                    throw new HttpServerBadResponseException(result["msg"]);
                            }
                            catch (HttpServerBadResponseException e)
                            {
                                throw e;
                            }
                            catch { }
                            //HACK-END

                            return result;
                        }
                    }
                }
                catch (AggregateException e)
                {
                    throw new HttpServerDontFoundException("게임서버가 응답하지 않습니다. 관리자에게 문의해주세요");
                }
                catch (TaskCanceledException e)
                {
                    throw new HttpServerDontFoundException("게임서버가 응답하지 않습니다. 관리자에게 문의해주세요");
                }
                catch (HttpServerBadResponseException e)
                {
                    throw e;
                }
                catch (HttpServerDontFoundException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    log.Error(e);
                    throw new HttpServerDontFoundException("게임서버에서 알 수 없는 에러가 발생했습니다. \r\n" + e);
                }
            }
        }
    }

    public class HttpServerException : Exception
    {
        public HttpServerException(string msg) : base(msg) { }
    }
    public class HttpServerDontFoundException : HttpServerException
    {
        public HttpServerDontFoundException(string msg) : base(msg) { }
    }
    public class HttpServerBadResponseException : HttpServerException
    {
        public HttpServerBadResponseException(string msg) : base(msg) { }
    }

}
