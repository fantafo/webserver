﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FIMS
{
    public static class QueryUnpacker
    { 
        public static Dictionary<string, object> Unpack(HttpRequestMessage request)
        {
            return Unpack(request.GetQueryNameValuePairs());
        }
        public static Dictionary<string, object> Unpack(IEnumerable<KeyValuePair<string, string>> e)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            object currVal;
            foreach(var pair in e)
            {
                if(result.TryGetValue(pair.Key, out currVal))
                {
                    if(currVal is string)
                    {
                        result[pair.Key] = new List<string>
                        {
                            (string)currVal,
                            pair.Value
                        };
                    }
                    else if(currVal is List<string>)
                    {
                        ((List<string>)currVal).Add(pair.Value);
                    }
                }
                else
                {
                    result.Add(pair.Key, pair.Value);
                }
            }
            return result;
        }
    }
}