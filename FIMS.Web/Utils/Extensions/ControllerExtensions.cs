﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FIMS
{
    public static class ControllerExtensions
    { 
        public static HttpSessionState GetSession(this Controller controller)
        {
            return HttpContext.Current.Session;
        }
        public static HttpSessionState GetSession(this ApiController controller)
        {
            return HttpContext.Current.Session;
        }
        public static long GetUserSN(this Controller controller)
        {
            return controller.GetSession()["userSN"] != null ? (long)controller.GetSession()["userSN"] : 0;
        }
        public static long GetUserSN(this ApiController controller)
        {
            return controller.GetSession()["userSN"] != null ? (long)controller.GetSession()["userSN"] : 0;
        }
        public static void SetUserSN(this ApiController controller, Member mem)
        {
            controller.GetSession()["userSN"] = (mem != null ? mem.SN : (object)null);
        }
        public static void SetUserSN(this Controller controller, Member mem)
        {
            controller.GetSession()["userSN"] = (mem != null ? mem.SN : (object)null);
        }
    }
}