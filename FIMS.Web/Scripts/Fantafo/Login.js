﻿$(document).ready(function ()
{
    $("input[type='radio']").checkboxradio({ icon: false });
    $("input[type='submit']").button();

    $("[data-selectfirst='1']").select();

    $(window).resize(OnResize);
    OnResize();
});

function OnResize() {
    var width = $(window).width() - 140;
    $(".contentWidth").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).width(width + margin);
    });
    var height = $(window).height() - 0;
    $(".contentHeight").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).height(height + margin);
    });
};

var errorIdx = -1;
function ErrorMsg(msg) {
    if (errorIdx != -1) clearTimeout(errorIdx);

    $("#ErrorMsg").text(msg).css("display", "block");
    setTimeout(function () {
        $("#ErrorMsg").css("display", "none");
    }, 5000);

}