﻿$(document).ready(function () {

    var editor = new $.fn.dataTable.Editor({

        "ajax": {
            "url": "/api/Member/Editor",
            "type": "POST",
            "datatype": 'json',
            success: function (data) {
                if (data === 'NO') {
                    alert("시도한 작업이 실패했습니다.");
                } else if (data === 'OverlapId') {
                    alert("중복된 학번입니다.");
                } else if (data === 'NoAdmin') {
                    alert("관리자 로그인이 필요합니다.");
                }
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            },
            //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
            error: function (response, state, errorCode) {
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            }
        },

        table: "#demoGrid",
        idSrc: 'SN',
        fields:
            [
                {
                    label: "SN",
                    name: "SN",
                    type: "hidden",
                    def: "1"

                },
                {
                    label: "학번:",
                    name: "Id",
                },
                {
                    label: "이름:",
                    name: "Name",
                },
                {
                    label: "전공:",
                    name: "Major",
                },
                /*
                {
                    label: "지도 교수:",
                    name: "Professor",
                },
                {
                    label: "성별:",
                    name: "Gender",
                    type: "select",
                    options: [
                        { label: "남성", value: "0" },
                        { label: "여성", value: "1" },
                    ]
                }
                */
            ],
        i18n: {
            create: {
                title: "새로운 학생 추가",
                submit: "추가"
            },
            edit: {
                title: "학생 정보 수정",
                submit: "수정"
            },
            remove: {
                title: "학생 정보 삭제",
                submit: "삭제",
                confirm: {
                    _: "%d명의 학생 정보를 삭제하겠습니까?",
                    1: "1명의 학생 정보를 삭제하겠습니까?"
                }
            },
        }

    });

    var table = $("#demoGrid").DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        //dom: 'frtip',

        buttons: [
            {
                extend: "create",
                editor: editor,
                text: "생성",
            },

            {
                extend: "edit",
                editor: editor,
                text: "수정"
            },

            {
                extend: "remove",
                editor: editor,
                text: "삭제",
                /*
                action: function () {
 
                    //DeleteData(idSrc);
                }
                */
            },

            {
                extend: 'print',
                customizeData: function () {
                    option = false;
                },
                text: '프린트',
                exportOptions: {
                    columns: [1, 2, 3]
                }
            },

            {
                extend: 'excelHtml5',

                customizeData: function (data) {
                    option = false;
                },
                text: '정보 Excel 저장',
                title: 'VR 안전교육 학생 정보',
                exportOptions: {
                    columns: [1, 2, 3]
                },
            },
            /*
            {
                text: "학생 정보 Excel파일로 업로드",
                action: function (e, dt, node, config) {
                    $.ajax({
                        "url": "/api/Member/ExcelRead",
                        "type": "POST",
                        "datatype": 'json',
                        success: function (data) {
                            if (data === 'OK') {
                                alert("성공");
                                oTable = $('#demoGrid').DataTable();
                                oTable.draw();
                            } else if (data === 'NO') {
                                alert("취소했습니다.");
                            } else if (data === 'NoAdmin') {
                                alert("관리자 로그인이 필요합니다.");
                            } else {
                                alert(data + "명 등록에 실패했습니다.");
                                oTable = $('#demoGrid').DataTable();
                                oTable.draw();
                            }
                        },
                        //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
                        error: function (response, state, errorCode) {
                            alert("실패");
                        }

                    });
                }
            }*/
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        "select": 'multi',

        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '선택하여 수정/삭제 가능합니다',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/LMS/LoadData",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
                /*{
                "targets": [4],
                "visible": false,
                "searchable": false,
                "orderable": false
            },
            */
            ],

        "columns": [
            { "data": "SN", "name": "SN" },
            { "data": "Id", "name": "Id", "autoWidth": true },
            { "data": "Name", "name": "Name", "autoWidth": true },
            { "data": "Major", "name": "Major", "autoWidth": true },
            //{ "data": "Professor", "name": "Professor", "autoWidth": true },
            //{ "data": "Gender", "name": "Gender", "autoWidth": true },
            /*
            {
                "render": function (data, type, row, meta) {
                    //return '<a class="btn btn-info" href="/LMS/Edit/' + full.Id + '">Edit</a>';
                    return "<a href='#' class='btn btn-info' onclick=EditData('" + row.Id + "'); >수정</a>";
                }
            },
            /*
            {
                data: null, render: function (data, type, row) {
                    return "<a href='#' class='btn btn-danger' onclick=DeleteData('" + row.Id + "'); >삭제</a>";
                }
            },
            */
        ]

    });



});

function AjaxCall(url) {
    $.getJSON(url, function (data) {
        if (data && data.msg) {
            alert(data.msg);
        }
    });
}


function MemberInit() {
    var Init = confirm("정말 학생 정보를 초기화 합니까?");
    if (Init) {
        Send("/api/Member/MemberInit/", null, function (data, ajax) {
            if (ajax.status === 200) {
                alert("학생 정보가 초기화 성공했습니다.");
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            } else {
                alert("관리자 로그인이 필요합니다.");
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            }
        });
    }
}

function Send(url, data, callback) {
    $.ajax(url, {
        method: "GET",
        data: data,
        xhrFields: { withCredentials: true },       //쿠키 보내기
        contentType: "application/json; charset=UTF-8",
        success: function (data, type, ajax) { callback(data, ajax); },
        error: function (ajax) { callback(null, ajax); },
    });
};