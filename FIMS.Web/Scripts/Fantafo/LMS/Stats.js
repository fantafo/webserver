﻿$(document).ready(function () {
    var table = $("#demoGrid").DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'print',
                customizeData: function () {
                    option = false;
                },
                text: '프린트',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                }
            },

            {
                extend: 'excelHtml5',
                customizeData: function (data) {
                    option = false;
                },
                text: 'Excel 저장',
                title: '안전교육VR 통계',
                exportOptions: {
                    columns: [0, 1, 2, 3]
                },
            },
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 100,
        "bLengthChange": false,
        //"bFilter": false,
        "language": {
            "search": "기간검색",
            searchPlaceholder: "2018-01-01~2019-01-01",
            info: "검색결과 _TOTAL_",
            infoEmpty: "검색결과 0",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        "ajax": {
            "url": "/LMS/LoadStats",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [
            ],

        "columns": [
            { "data": "Major", "name": "Major", "autoWidth": true },
            { "data": "Total", "name": "Total", "autoWidth": true },
            { "data": "Completion", "name": "Completion", "autoWidth": true },
            { "data": "Percent", "name": "Percent", "autoWidth": true },
        ]
    });
});