﻿$(document).ready(function () {

    var editor = new $.fn.dataTable.Editor({

        "ajax": {
            "url": "/api/Member/CompletionEditor",
            "type": "POST",
            "datatype": 'json',
            success: function (data) {
                if (data === 'NO') {
                    alert("시도한 작업이 실패했습니다.");
                } else if (data == 'NoId') {
                    alert("해당 학번이 없습니다.");
                } else if (data == 'NoAdmin') {
                    alert("관리자 로그인이 필요합니다.");
                }
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            },
            //json 형식이 아닌것이 오면 무조건 error로 가기때문에..
            error: function (response, state, errorCode) {
                editor.close();
                oTable = $('#demoGrid').DataTable();
                oTable.draw();
            }
        },

        table: "#demoGrid",
        idSrc: 'SN',
        fields:
            [
                {
                    label: "SN",
                    name: "SN",
                    type: "hidden",
                },
            /*
                {
                    label: "Ord",
                    name: "Ord",
                    type: "hidden",

                },
            */
                {
                    label: "학번:",
                    name: "Id",
                    //def: "66230",
                    fieldInfo: '존재하는 학번을 입력해주세요'
                },
                {
                    label: "이수시간:",
                    name: "PlayTime",
                    //def: "660",
                },
                {
                    label: "이수날짜:",
                    name: "Date",
                    type: "datetime",
                    format: "YYYY-MM-DD HH:mm:ss",
                    def: function () { return new Date(); },
                },
                {
                    label: "점수:",
                    name: "Score",
                    //def: "660",
                },

            ],
        i18n: {
            create: {
                title: "새로운 이수 정보 추가",
                submit: "추가"
            },
            edit: {
                title: "이수 정보 수정",
                submit: "수정"
            },
            remove: {
                title: "이수 정보 삭제",
                submit: "삭제",
                confirm: {
                    _: "%d명의 학생 이수 정보를 삭제하겠습니까?",
                    1: "1명의 학생 이수 정보를 삭제하겠습니까?"
                }
            },
        }

    });

    var table = $("#demoGrid").DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        //dom: 'frtip',
        buttons: [
            {
                extend: "create",
                editor: editor,
                text: "생성",
            },

            {
                extend: "edit",
                editor: editor,
                text: "수정"
            },

            {
                extend: "remove",
                editor: editor,
                text: "삭제",
            },

            {
                extend: 'print',
                customizeData: function () {
                    option = false;
                },
                text: '프린트',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5]
                }
            },

            {
                extend: 'excelHtml5',

                customizeData: function (data) {
                    option = false;
                },
                text: '정보 Excel 저장',
                title: '안전교육VR 이수명단',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5]
                },
            },
            
            {
                text: "수료증",
                action: function (e, dt, node, config) {
                    Send("/LMS/Certificate", null, function (data, ajax) {
                        if (data === 'NO') {
                            alert("시도한 작업이 실패했습니다.");
                        }else if (data === 'NoAdmin') {
                            alert("관리자 로그인이 필요합니다.");
                        } else {
                            window.location.href = '../' + 'file/' + encodeURIComponent(data);
                        }
                    });
                }
            }
            
        ],

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        "select": 'single',
        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '선택하여 수정/삭제 가능합니다',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_명",
            infoEmpty: "검색결과 0명",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/LMS/LoadCompletion",
            "type": "POST",
            "datatype": "json"
        },

        "columnDefs":
            [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
                
                /*{
                "targets": [4],
                "visible": false,
                "searchable": false,
                "orderable": false
            },
            {
                "targets": [5],
                "visible": false,
                "searchable": false,
                "orderable": false
            }*/],

        "columns": [
            { "data": "SN", "name": "SN", "autoWidth": true },
            //{ "data": "Ord", "name": "Ord", "autoWidth": true },
            { "data": "Id", "name": "Id", "autoWidth": true },
            { "data": "Name", "name": "Name", "autoWidth": true },
            { "data": "Major", "name": "Major", "autoWidth": true },
            //{ "data": "Professor", "name": "Professor", "autoWidth": true },
            { "data": "PlayTime", "name": "PlayTime", "autoWidth": true },
            { "data": "Date", "name": "Date", "autoWidth": true },
            /*
            {
                data: "Date",
                render: function (data, type, row) {
                    if (type === "sort" || type === "type") {
                        return data;
                    }
                    return moment(data).format("YYYY-MM-DD HH:mm");
                }
            },
            */
            { "data": "Score", "name": "Score", "autoWidth": true },
        ]
    });
    
});



function CompletionInit() {
    var Init = confirm("정말 이용 정보를 초기화 합니까?");
    if (Init) {
        Send("/api/Member/CompletionInit/", null, function (data, ajax) {
            if (ajax.status == 200) {
                alert("이용정보가 초기화 성공했습니다.");
                oTable = $('#demoGrid').DataTable();
                oTable.draw();

            } else {
                alert("관리자 로그인이 필요합니다.");
            }
        });
    }
}

function DateSearch() {
    var datetime = {
        "start": $('#datepicker1').val(),
        "end": $('#datepicker2').val()
    };
    Send("/LMS/LoadDateCompletion", datetime, function (data, ajax) { });

    //Send("/LMS/LoadDateCompletion", datetime, function (data, ajax) { });
}


function AjaxCall(url) {
    $.getJSON(url, function (data) {
        if (data && data.msg) {
            alert(data.msg);
        }
    });
}

function Send (url, data, callback) {
    $.ajax(url, {
        method: "POST",
        data: data,
        contentType: "application/json; charset=UTF-8",
        success: function (data, type, ajax) { callback(data, ajax); },
        error: function (ajax) { callback(null, ajax); },
    });
};