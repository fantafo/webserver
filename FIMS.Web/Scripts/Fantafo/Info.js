﻿$(window).load(function ()
{
    $(".inputlist input[type='radio']").checkboxradio({ icon: false });
    $(".inputlist input[type='submit']").button();
    $(".popupBox a").button();

    InitializeData();

    $(window).resize(OnResize);
    OnResize();
});

function InitializeData() {
}

function ClosePopup()
{
    $("#AdditionalBox").hide("fade");
}

function OnResize() {
    var width = $(window).width() - 140;
    $(".contentWidth").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).width(width + margin);
    });
    var height = $(window).height() - 0;
    $(".contentHeight").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).height(height + margin);
    });
};