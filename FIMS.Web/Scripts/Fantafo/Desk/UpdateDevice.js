﻿$(document).ready(function () {

    $("a").button();

    var devEditor = new $.fn.dataTable.Editor({
        "ajax": "/api/UpdateDevice/Editor",
        "table": "#deviceTable",
        "idSrc": "SN",
        "fields": [{
            label: "updateCheck:",
            name: "updateCheck",
            type: "checkbox",
            separator: "|",
            options: [
                { label: '', value: true }
            ]
        }

        ]
    });

    var childRows = null;

    var devTable = $("#deviceTable").DataTable({
        dom: 'Brtp',
        "processing": false, // for show progress bar 정렬
        "serverSide": false, // for process server side 서버ajax
        "orderMulti": false, // for disable multiple column at once
        "select": false,
        "pageLength": 10,
        "autoWidth": false,
        "ajax": {
            "url": "/api/UpdateDevice/UpdateDeviceList",
            "type": "Get",
            "datatype": "json"
        },
        "orderClasses": false,
        "language": {
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_개",
            infoEmpty: "검색결과 0개",
            "emptyTable": "연결된 기기가 없습니다.",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        buttons: [
            {
                text: '업데이트 시작',
                action: function (e, dt, node, config) {
                    swal("업데이트를 시작합니다.", {
                        button: {
                            defeat: {
                                text: "시작",
                                value: true
                            }
                        }
                    })
                        .then((value) => {
                            if (value) {
                                Fims.Send("/api/UpdateDevice/StartUpdate", null, function () {
                                    $('#deviceTable').DataTable().ajax.reload(null, false);
                                });
                            }
                        });
                }
            },
            {
                text: '업데이트 중지',
                action: function (e, dt, node, config) {
                    swal("업데이트를 중지합니다.", {
                        button: {
                            defeat: {
                                text: "중지",
                                value: true
                            }
                        }
                    }).then((value) => {
                        if (value) {
                            Fims.Send("/api/UpdateDevice/StopUpdate", null, function () {
                                $('#deviceTable').DataTable().ajax.reload(null, false);
                            });
                        }
                    });
                }
            }
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],
        "columnDefs":
            [
                {
                    "visible": false,
                    "searchable": false,
                    "targets": 0
                },
                {
                    "width": "5%",
                    className: 'text-center',
                    "targets": 1

                },
                {
                    "width": "40%",
                    className: 'dt-head-left',
                    "targets": 2
                },
                {
                    "width": "22.5%",
                    "targets": 3,
                    className: "dt-head-left"
                },
                {
                    "width": "10%",
                    "targets": 4
                },
                {
                    "width": "22.5%",
                    "targets": 5
                }
            ],
        "columns": [
            { "data": "SN", "name": "SN" },
            {
                data: "updateCheck",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return "<div class='checks'><input type='checkbox' id='{0}' class='editor-active'/><label for='{0}'/></div>".format(row.SN);
                        //return '<input type="checkbox" ' + ((data === true) ? 'checked' : '') + ' id="input' + row.SN + '" class="editor-active" />';
                    }
                    return data;
                },
                className: "dt-body-center",
                "width": "5%",
                autoWidth: false
            },
            { "data": "name", "name": "name", className: "dt-body-left fileName", "width": "40%" },
            {
                data: "progress",
                render: function (data, type, row) {
                    const progress = data.split('#');
                    if (progress[0] && progress[1]) {
                        if (progress[1] > 0) {
                            //var updateProgress = $('<progress class="update" value=' + progress[0] + ' max=' + progress[1] + '></progress>'
                            //    + '<span style="margin-left: 10px;">' + progress[0] + '/' + progress[1] + '</span>');
                            //if (progress[2] && progress[0] < progress[1]) {
                            if (Number(progress[0]) < Number(progress[1]) || progress[2] === "False") {
                                let body = $('<div></div>');
                                let loading = $('<div class="loadingio-spinner-spin-tkucd6w91js" style="margin-top: 3px; margin-bottom: -3px;"><div class="ldio-d2vdiu90ejd"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div></div>');
                                let updateProgress = $('<progress class="update" style="margin-bottom: 10px; margin-right: 8px;" value=' + progress[0] + ' max=' + progress[1] + '></progress>');
                                body.append(updateProgress);
                                body.append(loading);
                                return body.clone().wrapAll("<div/>").parent().html();
                            } else {
                                let updateProgress = $('<progress class="update" value=' + progress[0] + ' max=' + progress[1] + '></progress>');
                                return updateProgress.clone().wrapAll("<div/>").parent().html();
                            }
                        }
                        return '<progress class="update" value=' + progress[0] + ' max=' + progress[1] + '></progress>';
                    } else {
                        return '<progress class="update" value="0" max="10"></progress>';
                    }
                },
                "width": "22.5%",
                autoWidth: false,
                className: "dt-body-left",
            },
            {
                "data": "state",
                "name": "state",
                render: function (data, type, row) {
                    if (!data)
                        return '오류';

                    const state = data.split('#');
                    if (state[0] && state[1] && state[2]) {
                        //state[0] 다운로드시작, state[1]완료여부, state[2] 오류
                        if (state[2] !== UpdateErrorCode(UPDATE_ERROR_NONE)) {
                            if (state[2] === UpdateErrorCode(UPDATE_ERROR_ERROR)) {
                                return '오류 발생';
                            } else if (state[2] === UpdateErrorCode(UPDATE_ERROR_STOP)) {
                                return '중지';
                            }
                        } else {
                            if (state[1] === "True") {
                                return '완료';
                            } else if (state[0] === "True") {
                                return '다운 중';
                            } else if (state[1] === "False" && state[0] === "False") {
                                return '대기 중';
                            }
                        }
                    }
                },
                "width": "10%",
                autoWidth: false,
                className: "dt-body-center"
            },
            {
                "data": "storge",
                "name": "storge",
                render: function (data, type, row) {
                    const size = data.split('#');
                    //size[0] 총용량, size[1] 사용가능용량
                    if (size === '0#0') {
                        return '-';
                    } else {
                        const total = (size[0] / 1073741824).toFixed(2);
                        const used = ((size[0] - size[1]) / 1073741824).toFixed(2);
                        let progress = $('<progress value="{0}" max="{1}"></progress><br>{0}GB/{1}GB</br>'.format(used, total));
                        if (Number(used / total) >= 0.8) {
                            progress.addClass('red');
                        }
                        return progress.clone().wrapAll("<div/>").parent().html();
                        //return progress.prop("outerHTML");
                    }
                },
                className: "dt-body-center",
                "width": "22.5%",
                autoWidth: false,
            }
        ],
        "initComplete": function () {
            setInterval(function () {
                childRows = devTable.rows($('.shown'));
                //scrollTop = null;
                scrollTop.length = 0;

                let childrow = $('div.dataTables_scrollBody');
                $.each(childrow, function (index, v) {
                    let id = v.childNodes[0].id;
                    let top = $($(v)).scrollTop();
                    scrollTop[id] = top;
                });
                devTable.ajax.reload(null, false);
                //기기 세부정보 요청
                Fims.Send("/api/UpdateDevice/GetDeviceDetails", null, function () {
                });
            }, 3000);
        },
        "rowCallback": function (row, data) {
            // Set the checked state of the checkbox in the table
            $('.checks input.editor-active', row).prop('checked', data.updateCheck);
        }
    });

    // Handle click on "Select all" control
    $('#updateDevice-select-all').on('click', function () {
        // Get all rows with search applied
        var rows = devTable.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        //$('input.editor-UpdateCheck', rows).prop('checked', this.checked);
        devEditor
            .edit(rows, false)
            .set('updateCheck', this.checked)
            .submit();

    });

    $('#deviceTable').on('change', 'input.editor-active', function () {
        devEditor
            .edit($(this).closest('tr'), false)
            .set('updateCheck', $(this).prop('checked') ? true : false)
            .submit();
    });

    $('#deviceTable tbody').on('click', 'td.fileName', function () {
        var tr = $(this).closest('tr');
        var row = devTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            destroyChild(row);
            tr.removeClass('shown');
            //tr.removeClass('highlightExpanded');
        }
        else {
            // Open this row
            createChild(row, devTable.row(this).data().SN);
            tr.addClass('shown');
            //tr.addClass('highlightExpanded');
        }
    });

    devTable.on('draw', function () {
        console.log("devTable draw");
        if (childRows) {
            let rows = childRows;
            //let rows = Object.assign({}, childRows);
            // Reset childRows so loop is not executed each draw
            childRows = null;
            //$.each(rows[0], function (index, item) {
            //    createChild(devTable.row(item), devTable.row(item).data().SN);
            //    $(devTable.row(item)).addClass('shown');
            //});
            rows.every(function (rowIdx, tableLoop, rowLoop) {
                var row = this;
                createChild(row, devTable.row(this).data().SN);
                this.nodes().to$().addClass('shown');
            });
        }
    });
});

var scrollTop = [];

function createChild(row, id) {
    // This is the table we'll convert into a DataTable
    let ID = "child-" + id;
    console.log("ChildTable draw " + ID);
    var table = $('<table class="display" width="100%" id="{0}" style="table-layout: fixed;"/>'.format(ID));

    // Display it the child row
    row.child(table).show();

    var rowdata = row.data().detail;

    let TOP = scrollTop[ID];

    // Initialise as a DataTable
    var fileTable = table.DataTable({
        dom: 'rt<"bottom"B>',
        data: rowdata,
        pageLength: 50,
        scrollX: true,
        scrollY: '300px',
        scrollCollapse: true, paginate: false, destroy: true,
        "orderClasses": false,
        "select": 'multi',
        "autoWidth": false,
        "preDrawCallback": function (settings) {
            pageScrollPos = $('body').scrollTop();
        },
        "drawCallback": function (settings) {
            if ($('#' + ID).parents("div.dataTables_scrollBody") !== undefined) {
                $('#' + ID).parents("div.dataTables_scrollBody").scrollTop(TOP);
                console.log('scroll ' + TOP);
            }
        },
        //initComplete: function () {
        //    $('div.dataTables_scrollBody').scrollTop(TOP);
        //},
        "language": {
            "emptyTable": "파일이 없습니다."
        },
        buttons: [
            {
                text: "파일 삭제",
                action: function (e, dt, node, config) {
                    let rows = fileTable.rows({ selected: true });
                    var data = {};
                    data.fileName = [];
                    $.each(rows[0], function (index, item) {
                        data.fileName.push(fileTable.row(item).data().childName);
                    });
                    swal({
                        title: "파일을 삭제합니다.",
                        text: "모든 기기에서 삭제를 원하시면 모두 삭제를 누르세요",
                        icon: "warning",
                        buttons: {
                            defeat: {
                                text: "삭제",
                                value: "Delete"
                            },
                            catch: {
                                text: "모두 삭제",
                                value: "AllDelete"
                            },
                            cancel: "취소"
                        },
                        dangerMode: true
                    })
                        .then((willDelete) => {
                            switch (willDelete) {
                                case "AllDelete":
                                    data.sn = -1;
                                    Fims.PostSend("/api/UpdateDevice/DeleteFile2", JSON.stringify(data), function (data, ajax) {
                                    });
                                    break;
                                case "Delete":
                                    data.sn = fileTable.row(0).data().sn;
                                    Fims.PostSend("/api/UpdateDevice/DeleteFile2", JSON.stringify(data), function (data, ajax) {
                                    });
                                    break;
                                default:
                                    swal("작업을 취소하였습니다.");
                            }
                        });
                }
            },

            {
                text: "파일 전체 삭제",
                action: function (e, dt, node, config) {
                    var data = {};
                    data.sn = fileTable.row(0).data().sn;
                    swal({
                        title: "저장된 모든 콘텐츠 파일을 삭제합니다.",
                        text: '',
                        icon: "warning",
                        buttons: {
                            defeat: {
                                text: "삭제",
                                value: true
                            },
                            cancel: "취소"
                        },
                        dangerMode: true
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                swal("삭제를 진행합니다!", {
                                    icon: "success",
                                    button: "확인",
                                });
                                Fims.PostSend("/api/UpdateDevice/DeleteAllFiles", JSON.stringify(data), function (data, ajax) {
                                });
                            } else {
                                swal("삭제를 취소합니다.", {
                                    button: "확인",
                                });
                            }
                        });

                    //Fims.PostSend("/api/UpdateDevice/DeleteFile", null, function (data, ajax) {
                    //});
                }
            }
        ],
        columns: [
            {
                title: '파일명', "data": "childName", "name": "childName", className: "dt-body-left", "width": "43%", autoWidth: false
            },
            {
                title: '',
                data: "childProgress",
                "width": "21%",
                render: function (data, type, row) {
                    if (!data)
                        return '<progress class="update" value="0" max="10"></progress>';
                    const progress = data.split('#');
                    if (progress[0] && progress[1] && progress[2]) {
                        if (Number(progress[0]) >= Number(progress[1]) || Number(progress[2]) === 2)
                            return '<progress class="update" value=' + progress[0] + ' max=' + progress[1] + '></progress>';
                        else {
                            let body = $('<div></div>');
                            let loading = $('<div class="loadingio-spinner-spin-tkucd6w91js" style="margin-top: 3px; margin-bottom: -3px;"><div class="ldio-d2vdiu90ejd"><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div></div>');
                            let updateProgress = $('<progress class="update" style="margin-bottom: 10px; margin-right: 8px;" value=' + progress[0] + ' max=' + progress[1] + '></progress>');
                            body.append(updateProgress);
                            body.append(loading);
                            return body.clone().wrapAll("<div/>").parent().html();
                        }
                    } else {
                        return '<progress class="update" value="0" max="10"></progress>';
                    }
                },
                className: "dt-body-left",
                autoWidth: false
            },
            {
                title: '',
                "data": "childState",
                "width": "12%",
                render: function (data, type, row) {
                    if (!data)
                        return '오류';

                    const state = data.split('#');
                    if (state[0] && state[1] && state[2]) {
                        //state[0] 다운시작여부,  state[1] 성공, state[2] 오류
                        //state[2] 0 오류 없음.
                        if (state[2] < 0) {
                            if (state[2] === DownloadErrorCode(DOWNLOAD_FILE_EXIST)) {
                                return '존재하는 파일';
                            } else if (state[2] === DownloadErrorCode(DOWNLOAD_FILE_ENOUGH_STORAGE)) {
                                return '저장 공간 부족';
                            } else if (state[2] === DownloadErrorCode(DOWNLOAD_FILE_STOPUPDATE)) {
                                return '중지';
                            } else {
                                return '오류 발생';
                            }
                        } else if (state[0] === "False" && state[1] === "False") {
                            return '대기 중';
                        } else if (state[0] === "False" && state[1] === "True") {
                            return '완료';
                        } else if (state[1] === "False") {
                            return '진행 중';
                        } else if (state[0] === "True" && state[1] === "True") {
                            return '존재하는 파일';
                        } else {
                            return '-';
                        }
                    }
                },
                className: "dt-body-center",
                "name": "childState",
                autoWidth: false
            },
            { "data": null, "width": "19%", defaultContent: "", "autoWidth": false }
        ]
    });

    //삭제 버튼 클릭
    table.on('click', '.remove', function () {
        let data = fileTableTable.row($(this).parents('tr')).data();
        const state = data.childState.split('#');
        //이미 존재하거나 다운이 완료된 경우..
        let msg = "삭제?";
        if ((state[2] === DownloadErrorCode(DOWNLOAD_FILE_EXIST)) || (state[0] === "False" && state[1] === "True") || (state[0] === "True" && state[1] === "True")) {
            msg = "정말 삭제하시겠습니까??";
        } else {
            msg = "업데이트를 취소하겠습니까??";
        }
        if (confirm(msg) === true) {
            let obj = {};
            obj.childName = data.childName;
            obj.sn = data.sn;
            Fims.PostSend("/api/UpdateDevice/DeleteFile", JSON.stringify(obj), function () {
                $('#fileTable').DataTable().ajax.reload(null, false);
            });
        }
    });

    table.on('click', '.allRemove', function () {
        let data = fileTableTable.row($(this).parents('tr')).data();
        const state = data.childState.split('#');
        //이미 존재하거나 다운이 완료된 경우..
        let msg = "삭제?";
        if ((state[2] === DownloadErrorCode(DOWNLOAD_FILE_EXIST)) || (state[0] === "False" && state[1] === "True") || (state[0] === "True" && state[1] === "True")) {
            msg = "정말 삭제하시겠습니까??";
        } else {
            msg = "업데이트를 취소하겠습니까??";
        }
        if (confirm(msg) === true) {
            let obj = {};
            obj.childName = data.childName;
            obj.sn = -1;
            Fims.PostSend("/api/UpdateDevice/DeleteFile", JSON.stringify(obj), function () {
                $('#fileTable').DataTable().ajax.reload(null, false);
            });
        }
    });
}


function destroyChild(row) {
    var table = $("table", row.child());
    table.detach();
    table.DataTable().clear();

    // And then hide the row
    row.child.hide();
}

const UPDATE_ERROR_NONE = Symbol('updateErrorCode');
const UPDATE_ERROR_ERROR = Symbol('updateErrorCode');
const UPDATE_ERROR_STOP = Symbol('updateErrorCode');

function UpdateErrorCode(code) {

    switch (code) {
        //이미 존재하는 파일
        case UPDATE_ERROR_NONE:
            return String(0);
        //오류 발생
        case UPDATE_ERROR_ERROR:
            return String(-1);
        //업데이트 중지 이벤트
        case UPDATE_ERROR_STOP:
            return String(-2);
    }
}

const DOWNLOAD_FILE_EXIST = Symbol('DownloadErrorCode');
const DOWNLOAD_FILE_ENOUGH_STORAGE = Symbol('DownloadErrorCode');
const DOWNLOAD_FILE_STOPUPDATE = Symbol('DownloadErrorCode');

function DownloadErrorCode(code) {

    switch (code) {
        //이미 존재하는 파일
        case DOWNLOAD_FILE_EXIST:
            return String(-2);
        //저장공간 부족
        case DOWNLOAD_FILE_ENOUGH_STORAGE:
            return String(-3);
        //업데이트 중지 이벤트
        case DOWNLOAD_FILE_STOPUPDATE:
            return String(-4);
    }
}

function AjaxCall(url) {
    $.getJSON(url, function (data) {
        if (data && data.msg) {
            alert(data.msg);
        }
    });
}

Fims = new function () {

    // void Send(string url, dynamic data, Action<dynamic, ajax> callback)
    this.Send = function (url, data, callback) {
        $.ajax(url, {
            method: "GET",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };

    this.PostSend = function (url, data, callback) {
        $.ajax(url, {
            method: "POST",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };
};
