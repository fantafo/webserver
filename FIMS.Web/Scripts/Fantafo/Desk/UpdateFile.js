﻿$(document).ready(function () {

    $("a, button").button();

    var editor = new $.fn.dataTable.Editor({
        "ajax": "/api/UpdateFile/Editor",
        "table": "#fileTable",
        "idSrc": "FileName",
        "fields":
            [
                {
                    label: "UpdateCheck:",
                    name: "UpdateCheck",
                    type: "checkbox",
                    separator: "|",
                    options: [
                        { label: '', value: true }
                    ]
                },
                {
                    label: "InstallPath",
                    name: "InstallPath"
                },
                {
                    label: 'Category',
                    name: 'Category'
                }
            ]
    });

    var table = $("#fileTable").DataTable({
        dom: 'frtip',
        "processing": false, // for show progress bar
        "serverSide": false, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "bLengthChange": true, //thought this line could hide the LengthMenu
        "pageLength": 10,
        "orderClasses": false,
        scrollX: true,
        scrollCollapse: true,
        "language": {
            //"sSearch": '<a class="btn searchBtn" id="searchBtn"><i class="fa fa-search"></i></a>',
            "lengthMenu": " 표_MENU_",
            "search": "검색",
            select: {
                rows: {
                    _: '%d rows selected',
                    0: '',
                    1: ''
                }
            },
            info: "검색결과 _TOTAL_개",
            infoEmpty: "검색결과 0개",
            paginate: {
                "first": "First",
                "last": "Last",
                "next": "다음",
                "previous": "이전"
            },

        },
        lengthMenu: [
            [10, 25, 50, -1],
            ['10', '25', '50', 'All']
        ],

        "ajax": {
            "url": "/api/UpdateFile/FileList",
            "type": "Get",
            "datatype": "json"
        },

        "columnDefs":
            [
                {
                    "targets": 0,
                },
                {
                    className: 'dt-head-left',
                    type: 'natural-asc',
                    "targets": 1
                },
                {
                    "targets": 2
                },
                {
                    "targets": 3
                },
                {
                    "targets": 4
                },
                {
                    "targets": 5
                }
            ],

        "columns": [
            {
                data: "UpdateCheck",
                name: "UpdateCheck",
                render: function (data, type, row) {
                    if (type === 'display') {
                        //return '<input type="checkbox" class="editor-UpdateCheck">';
                        return '<div class="checks"><input type="checkbox" class="editor-UpdateCheck" id=' + row.FileName + '><label for=' + row.FileName + '></div>';
                    }
                    return data;
                },
                className: "dt-body-center",
                autoWidth: true
            },
            { "data": "FileName", "name": "FileName", className: "dt-body-left" },
            {
                data: "UpdateDate",
                name: "UpdateDate",
                render: function (data, type, row) {
                    return new Date(Date.parse(data)).format("yyyy년 MM월 dd일 hh시 mm분");
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                data: "Size",
                name: "Size",
                render: function (data, type, row) {
                    return (data / 1073741824).toFixed(2) + 'GB';
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                data: "Category",
                name: "Category",
                "render": function (d, t, r) {
                    let box = $('<div class="box">');
                    let select = $('<select>');
                    box.append(select);
                    //k인덱스 v 값
                    $.each(Categorys, function (k, v) {
                        let option = $('<option></option>', {
                            "text": KORCategorys[k],
                            "value": v
                        });
                        if (Categorys[d] === v) {
                            option.attr("selected", "selected");
                        }
                        select.append(option);
                    });
                    return box.prop("outerHTML");
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                "className": 'delete-icon',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            }
        ],
        "rowCallback": function (row, data) {
            // Set the checked state of the checkbox in the table
            $('.checks input.editor-UpdateCheck', row).prop('checked', data.UpdateCheck);
        }

    });


    $('#fileTable tbody').on('change', 'select', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('Category', $(this).prop('value'))
            .submit();
    });

    $('#fileTable tbody').on('change','.checks input.editor-UpdateCheck', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('UpdateCheck', $(this).prop('checked') ? true : false)
            .submit();
    });
    

    // Handle click on "Select all" control
    $('#updateFile-select-all').on('click', function () {
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        //$('input.editor-UpdateCheck', rows).prop('checked', this.checked);
        editor
            .edit(rows, false)
            .set('UpdateCheck', this.checked)
            .submit();

    });

    //삭제 버튼 클릭
    $('#fileTable tbody').on('click', 'td.delete-icon', function () {
        swal({
            title: "정말 삭제하시겠습니까?",
            text: "삭제되면 파일을 복구 할 수 없습니다! \n 기기에서는 삭제 되지 않습니다.",
            icon: "warning",
            buttons: {
                defeat: {
                    text: "삭제",
                    value: true
                },
                cancel: "취소",
            }
        })
            .then((willDelete) => {
                if (willDelete) {
                    let data = table.row($(this).parents('tr')).data();
                    let obj = {};
                    obj.FileName = data.FileName;
                    obj.Path = data.Path;
                    Fims.PostSend("/api/UpdateFile/RemoveFile", JSON.stringify(obj), function () {
                        $('#fileTable').DataTable().ajax.reload();
                    });
                    swal("삭제 되었습니다.", {
                        icon: "success",
                    });
                }
            });

        //if (confirm("정말 삭제하시겠습니까?? \n 기기에서는 삭제 되지 않습니다.") === true) {
        //    let data = table.row($(this).parents('tr')).data();
        //    let obj = {};
        //    obj.FileName = data.FileName;
        //    obj.Path = data.Path;
        //    Fims.PostSend("/api/UpdateFile/RemoveFile", JSON.stringify(obj), function () {
        //        $('#fileTable').DataTable().ajax.reload();
        //    });
        //}
    });
});


Fims = new function () {

    // void Send(string url, dynamic data, Action<dynamic, ajax> callback)
    this.Send = function (url, data, callback) {
        $.ajax(url, {
            method: "GET",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };

    this.PostSend = function (url, data, callback) {
        $.ajax(url, {
            method: "POST",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };
};


Date.prototype.format = function (f) {
    if (!this.valueOf()) return " ";

    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;

    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function ($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

String.prototype.string = function (len) { var s = '', i = 0; while (i++ < len) { s += this; } return s; };
String.prototype.zf = function (len) { return "0".string(len - this.length) + this; };
Number.prototype.zf = function (len) { return this.toString().zf(len); };
