﻿$(document).ready(function () {
    
    initializeDevices();

    var editor = new $.fn.dataTable.Editor({
        "ajax": "/api/FileUpdate/Editor",
        "table": "#updateTable",
        "idSrc": "FileName",
        "fields":
            [
                {
                    label: "UpdateCheck:",
                    name: "UpdateCheck",
                    type: "checkbox",
                    separator: "|",
                    options: [
                        { label: '', value: true }
                    ]
                },
                {
                    label: "InstallPath",
                    name: "InstallPath"
                },
                {
                    label: "RemoveCheck:",
                    name: "RemoveCheck",
                    type: "checkbox",
                    separator: "|",
                    options: [
                        { label: '', value: true }
                    ]
                }
            ]
    });

    var table = $("#updateTable").DataTable({
        dom: 'Brt',

        "processing": true, // for show progress bar
        "serverSide": true, // for process server side
        "filter": true, // this is for disable filter (search box)
        "orderMulti": false, // for disable multiple column at once
        "pageLength": 10,
        "select": {
            "style": 'os',
            "selector": 'td:not(:last-child)' // no row selection on last column
        },

        "ajax": {
            "url": "/api/FileUpdate/FileList",
            "type": "Get",
            "datatype": "json"
        },

        "columnDefs":
            [
                { className: 'text-center', targets: [0, 1, 2, 3] }
            ],

        "columns": [
            { "data": "FileName", "name": "FileName", className: "dt-body-center" },
            { "data": "UpdateDate", "name": "UpdateDate", "autoWidth": true, className: "dt-body-center" },
            {
                data: "Size",
                name: "Size",
                render: function (data, type, row) {
                    return (data / 1073741824).toFixed(2) + 'GB';
                },
                className: "dt-body-center",
                autoWidth: true
            },
            //{ "data": "Size", "name": "Size", "autoWidth": true, className: "dt-body-center" },
            {
                data: "UpdateCheck",
                name: "UpdateCheck",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-UpdateCheck">';
                    }
                    return data;
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                data: "RemoveCheck",
                name: "RemoveCheck",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" class="editor-RemoveCheck">';
                    }
                    return data;
                },
                className: "dt-body-center",
                autoWidth: true
            }
        ],
        buttons: [
            { extend: "remove", editor: editor, text: "삭제" }
        ],

        "rowCallback": function (row, data) {
            // Set the checked state of the checkbox in the table
            $('input.editor-UpdateCheck', row).prop('checked', data.UpdateCheck);
            $('input.editor-RemoveCheck', row).prop('checked', data.RemoveCheck);
        }

    });

    $('#updateTable').on('change', 'input.editor-UpdateCheck', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('UpdateCheck', $(this).prop('checked') ? true : false)
            .submit();
    });

    $('#updateTable').on('change', 'input.editor-RemoveCheck', function () {
        editor
            .edit($(this).closest('tr'), false)
            .set('RemoveCheck', $(this).prop('checked') ? true : false)
            .submit();
    });

    var dpEditor = new $.fn.dataTable.Editor({
        "ajax": "/api/UpdateDevice/Editor",
        "table": "#downloadProgressTable",
        "idSrc": "SN",
        "fields": [{
            label: "updateCheck:",
            name: "updateCheck",
            type: "checkbox",
            separator: "|",
            options: [
                { label: '', value: true }
            ]
        }

        ]
    });

    var childRows = null;

    var dpTable = $("#downloadProgressTable").DataTable({
        dom: 'rt',

        "processing": true, // for show progress bar 정렬
        "serverSide": true, // for process server side 서버ajax
        "orderMulti": false, // for disable multiple column at once
        "select": false,

        "ajax": {
            "url": "/api/UpdateDevice/UpdateDeviceList",
            "type": "Get",
            "datatype": "json"
        },
        "columnDefs":
            [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
                { className: 'text-center', targets: [1, 2, 3, 4, 5, 6] }
            ],

        "columns": [
            { "data": "SN", "name": "SN" },
            {
                data: "updateCheck",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" ' + ((data === true) ? 'checked' : '') + ' id="input' + row.id + '" class="editor-active" />';
                    }
                    return data;
                },
                className: "dt-body-center",
                autoWidth: true
            },
            { "data": "name", "name": "name", className: "dt-body-center" },
            {
                data: "progress",
                render: function (data, type, row) {
                    if (!data)
                        return '<progress value="0" max="10"></progress>';
                    const progress = data.split('#');
                    if (progress[0] && progress[1]) {
                        return '<progress value=' + progress[0] + ' max=' + progress[1] + '></progress>';
                    } else {
                        return '<progress value="0" max="10"></progress>';
                    }
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "state",
                "name": "state",
                render: function (data, type, row) {
                    if (!data)
                        return '오류';

                    const state = data.split('#');
                    if (state[0] && state[1] && state[2]) {
                        //state[0] 다운로드시작, state[1]성공, state[2] 오류
                        if (state[0] === "True") {
                            return '다운중..';
                        } else if (state[1] === "True" && state[2] === "False") {
                            return '다운 완료';
                        } else if (state[2] === "True") {
                            return '오류 발생';
                        } else {
                            return '-';
                        }
                    }
                },
                "autoWidth": true,
                className: "dt-body-center"
            },
            {
                "data": "storge",
                "name": "storge",
                render: function (data, type, row) {
                    if (data === -1) {
                        return '-';
                    }
                    return data;
                },
                "autoWidth": true,
                className: "dt-body-center"
            }
        ],
        "initComplete": function () {
            setInterval(function () {
                childRows = dpTable.rows($('.shown'));
                dpTable.ajax.reload(null, false);
            }, 5000);
        }
    });

    $('#downloadProgressTable').on('change', 'input.editor-active', function () {
        dpEditor
            .edit($(this).closest('tr'), false)
            .set('updateCheck', $(this).prop('checked') ? true : false)
            .submit();
    });

    $('#downloadProgressTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = dpTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            destroyChild(row);
            tr.removeClass('shown');
        }
        else {
            // Open this row
            createChild(row, 'child-table');    // class is for background colour
            tr.addClass('shown');
        }
    });

    dpTable.on('draw', function () {
        if (childRows) {
            let rows = childRows;
            // Reset childRows so loop is not executed each draw
            childRows = null;
            rows.every(function (rowIdx, tableLoop, rowLoop) {
                var row = this;
                createChild(row, 'child-table');
                this.nodes().to$().addClass('shown');
            });
        }
    });
});

function createChild(row) {
    // This is the table we'll convert into a DataTable
    var table = $('<table class="display" width="100%"/>');

    // Display it the child row
    row.child(table).show();

    var rowdata = row.data().detail;

    // Initialise as a DataTable
    var usersTable = table.DataTable({
        dom: 'rt',
        data: rowdata,
        columns: [
            { title: '파일명', "data": "childName", "name": "childName", className: "dt-body-center", "autoWidth": true},
            {
                title: '진행도',
                data: "childProgress",
                render: function (data, type, row) {
                    if (!data)
                        return '<progress value="0" max="10"></progress>';
                    const progress = data.split('#');
                    if (progress[0] && progress[1]) {
                        return '<progress value=' + progress[0] + ' max=' + progress[1] + '></progress>';
                    } else {
                        return '<progress value="0" max="10"></progress>';
                    }
                },
                className: "dt-body-center",
                autoWidth: true
            },
            {
                title: '상태',
                "data": "childState",
                render: function (data, type, row) {
                    if (!data)
                        return '오류';

                    const state = data.split('#');
                    if (state[0] && state[1]) {
                        //state[0] 다운시작여부,  state[1] 성공, state[2] 오류
                        //state[2] 0 오류 없음. 1 이미 존재하는 파일
                        if (state[0] === "False" && state[1] === "False" && Number(state[2]) >= 0) {
                            return '대기중..';
                        } else if (state[0] === "False" && state[1] === "True") {
                            return '다운 완료';
                        }else if (state[1] === "False" && Number(state[2]) === 0) {
                            return '다운중..';
                        } else if (state[1] === "True" && Number(state[2]) === 0) {
                            return '다운 완료';
                        } else if (Number(state[2]) === DownloadErrorCode(DOWNLOAD_FILE_EXIST)) {
                            return '이미 존재';
                        } else if (Number(state[2]) === DownloadErrorCode(DOWNLOAD_FILE_ENOUGH_STORAGE)) {
                            return '저장 공간 부족 /재다운로드';
                        } else if (Number(state[2]) < 0) {
                            return '오류 발생/재다운로드';
                        }
                    }
                },
                "name": "childState",
                className: "dt-body-center",
                "autoWidth": true
            }
        ]
    });
}


function destroyChild(row) {
    var table = $("table", row.child());
    table.detach();
    table.DataTable().destroy();

    // And then hide the row
    row.child.hide();
}


//데이터 테이블 갱신
GetUpdateFileList = function () {
    Fims.Send("/api/Fileupdate/UpdateFileList", null, function () {
        $('#updateTable').DataTable().ajax.reload();
    });
};

//업데이트 기기 테이블 갱신
dpTableReload = function () {
    $('#downloadProgressTable').DataTable().ajax.reload();
};

//업데이트 기기 모두 선택/해제
SelectAllDevice = function (check) {
    Fims.PostSend("/api/UpdateDevice/SelectAllDevice", JSON.stringify(check), function () {
        $('#downloadProgressTable').DataTable().ajax.reload();
    });
};

//업데이트 시작
StartUpdate = function () {
    Fims.Send("/api/UpdateDevice/StartUpdate", null, function () {
        //setInterval(function () {
        //    $('#downloadProgressTable').DataTable().ajax.reload();
        //}, 1000);
    });
};

const DOWNLOAD_FILE_EXIST = Symbol('DownloadErrorCode');
const DOWNLOAD_FILE_ENOUGH_STORAGE = Symbol('DownloadErrorCode');

function DownloadErrorCode(code) {

    switch (code) {
        case DOWNLOAD_FILE_EXIST:
            return -2;
        case DOWNLOAD_FILE_ENOUGH_STORAGE:
            return -3;
    }

}

Test = function () {
    let dptatable = $("#downloadProgressTable").DataTable();
    Fims.Send("/api/UpdateDevice/UpdateDeviceList", null, function (data, ajax) {
        dptatable.clear();
        dptatable.rows.add(data);
        dptatable.draw();
    });
};


function AjaxCall(url) {
    $.getJSON(url, function (data) {
        if (data && data.msg) {
            alert(data.msg);
        }
    });
}

Fims = new function () {

    // void Send(string url, dynamic data, Action<dynamic, ajax> callback)
    this.Send = function (url, data, callback) {
        $.ajax(url, {
            method: "GET",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };

    this.PostSend = function (url, data, callback) {
        $.ajax(url, {
            method: "POST",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };
};
