﻿/*

디바이스를 관리하고, 명령을 내리는 스크립트입니다.
디바이스의 정보를 갱신하고, 데이터를 보여줍니다.

*/
let updateDeviceManager;

function initializeDevices() {
    DeviceManager.Start();
}

DeviceManager = new function () {
    
    // 디바이스 목록
    this.devices = {};

    // interval을 정지하기 위한 id
    var taskId = 0;

    //연결된 디바이스 수
    var preConnectCount = 0;

    // 정보 갱신을 시작합니다.
    // 1초에 한번씩 서버에서 디바이스 정보를 얻어오고
    // 내용을 갱신합니다.
    this.Start = function () {
        if (taskId === 0) {
            taskId = setInterval(function () {
                Fims.Send("/api/Data/Devices", null, function (list, ajax) {
                    
                    // 디바이스를 최신 접속 순으로 정렬합니다.
                    list.sort(sortAlphaNum);

                    var connectCount = 0;
                    // 각 디바이스를 갱신합니다.
                    $.each(list, function (i, data) {

                        var dev = DeviceManager.devices[data.SN];
                        if (!dev) {
                            dev = new DeviceImpl();
                            DeviceManager.devices[data.SN] = dev;
                        }
                        dev.data = data;
                        if (data.ConState === 2) {
                            connectCount++;
                        }

                        if (!dev.data.Name || 0 === dev.data.Name.length) {
                            return;
                        }
                        // 갱신 시작!
                        dev.Refresh();
                    });
                    preConnectCount = connectCount;
                    if (connectCount < 1) {
                        $('#deviceCount')[0].innerHTML = '0 개';
                    } else {
                        $('#deviceCount')[0].innerHTML = connectCount + ' 개';
                    }
                });
                //UpdateDeviceInfo();
            }, 1000);
        }
    };

    // 디바이스 갱신을 중단합니다.
    this.Stop = function () {
        clearInterval(taskId);
    };

    this.UpdateStart = function () {

    };
};

DeviceImpl = function () {

    var self = this;

    this.data;
    this.case;
    this.elem;
    var name, model, temp, bat, enable;


    this.Refresh = function () {

        // 디바이스에 맞는 dom객체가 없을경우에 새로 생성합니다.
        if (!this.case) {
            //this.case = $("<div class='case'><input type='checkbox' /></div>");
            //this.case = $("<div class='case'><div class='checks'><input type='checkbox' class=agentOn' id="</div>");
            this.case = $("<div class='case'><div class='checks'><input type='checkbox' id='{0}'/><label for='{0}'/></div></div>".format(this.data.SN));
            //this.elem = $("<div class='device'><icon /><div class='name'></div><div class='model'></div><div class='temp'><div class='test'></div></div><div class='bat'><div class='gage'></div><div class='lb'></div></div></div>");
            this.elem = $("<div class='device'><icon /><div class='name'></div><div class='model'></div><div class='temp-icon'></div><div class='temp'></div><div class='bat-icon'></div><div class='bat'><div class='gage'></div><div class='lb'></div></div></div>");

            this.case[0].appendChild(this.elem[0]);
            
            name = this.elem.find(".name");
            model = this.elem.find(".model");
            tempIcon = this.elem.find(".temp-icon");
            temp = this.elem.find(".temp");
            batIcon = this.elem.find(".bat-icon");
            bat = this.elem.find(".bat");
            enable = this.case.find("input");

            name.hover(function () {
                model.css("display", "block");
            }, function () {
                model.css("display", "none");
            });

            enable.change(function () {
                Fims.Send("/api/Data/EnableDevices/" + self.data.SN, { on: (enable.prop('checked') ? 1 : 0) }, function (data, ajax) {
                });
                if (enable.prop('checked')) {
                    $('#toggleAgents').find(':input')[0].checked = true;
                }
            });
        }
        // 디바이스 목록에 추가합니다. (기존에 있던 dom의 경우는 마지막으로 붙게되며, 이게 결국은 순서가 됩니다.)
        this.case.appendTo($("#deviceWindow .devices"));

        // 내용 변경
        name.text(this.data.Name);
        model.text(this.data.Model);

        { // Temperature
            temp.text(this.data.Temp + "°C");
            if (this.data.Temp > 50) {
                tempIcon.addClass('high');
            }
            //let icon = this.elem.find(".test");
            //let className = 'norm';
            //if (!icon.hasClass(className)) {
            //    icon.removeClass(temp.data("tname"));
            //    icon.data("tname", className);
            //    icon.addClass(className);
            //}
            //var className = "norm";
            //if (this.data.Temp > 70) className = "high";
            //else if (this.data.Temp > 50) className = "mid";

            if (!temp.hasClass(className)) {
                temp.removeClass(temp.data("tname"));
                temp.data("tname", className);
                temp.addClass(className);
            }
        }

        { // Battery
            bat.find(".lb").text(this.data.Bat + "%");
            bat.find(".gage").css("width", this.data.Bat + "%");
            var className = "";
            if (this.data.Bat >= 90) className = "full";
            else if (this.data.Bat >= 80) className = "";
            else if (this.data.Bat >= 60) className = "high";
            else if (this.data.Bat >= 30) className = "low";
            else if (this.data.Bat >= 0) className = "very-low";

            if (!bat.hasClass(className)) {
                batIcon.addClass(className);
            }
            //var className = "norm";
            //if (this.data.Bat > 50) className = "high";
            //else if (this.data.Bat > 30) className = "mid";

            if (!bat.hasClass(className)) {
                bat.removeClass(bat.data("tname"));
                bat.data("tname", className);
                bat.addClass(className);
            }
        }

        { // Connection state
            if (this.data.ConState != 0) {
                if (!this.elem.hasClass("con")) {
                    this.elem.addClass("con");
                }
            } else {
                if (this.elem.hasClass("con")) {
                    this.elem.removeClass("con");
                }
            }
        }

        { // Device State
            var className = "norm";
            switch (this.data.State) {
                case 0: // Standby
                    var className = "norm";
                    break;
                case 1: // Allocated
                    var className = "alloc";
                    break;
                case 2: // Ready
                    var className = "ready";
                    break;
                case 3: // Heated
                    var className = "heat";
                    break;
                case 4: // Update
                    var className = "update";
                    break;
            }

            if (!this.elem.hasClass(className)) {
                this.elem.removeClass(this.elem.data("tname"));
                this.elem.data("tname", className);
                this.elem.addClass(className);
            }
        }

        { // Enable Checker
            if (this.data.Enabled != enable.prop('checked')) {
                enable.prop('checked', this.data.Enabled);
            }
        }
    }
}


//SelectAllDevice = function () {
//    updateDeviceManager.SelectAllDevice(true);
//};

//DeselectAllDevice = function () {
//    updateDeviceManager.SelectAllDevice(false);
//};

CheckStorageDevice = function () {
    updateDeviceManager.CheckStorageDevice();
};


//StartUpdate = function () {
//    updateDeviceManager.StartUpdate();
//};

UpdateDeviceInfo = function () {
    updateDeviceManager.UpdateDeviceInfo();
};

GetUpdateDeviceList = function () {
    //Fims.Send("/api/UpdateDevice/UpdateableDevice", null, function (list, ajax) {

    //    if (list)
    //        $('#downloadProgressTable').DataTable().ajax.reload();
        
    //});
};

var reA = /[^a-zA-Z-]/g;
var reN = /[^0-9]/g;
function sortAlphaNum(a, b) {
    /*
    if (a.ConState != b.ConState) {
        return b.ConState - a.ConState;
    }
    */
    if (!a.Name) {
        return 1;
    } else if (!b.Name) {
        return -1;
    }
    var aA = a.Name.replace(reA, "");
    var bA = b.Name.replace(reA, "");
    if (aA === bA) {
        var aN = parseInt(a.Name.replace(reN, ""), 10);
        var bN = parseInt(b.Name.replace(reN, ""), 10);
        return aN === bN ? 0 : aN > bN ? 1 : -1;
    } else {
        return aA > bA ? 1 : -1;
    }
}