﻿/*

게임 세션을 새로 생성하거나, 변경 등의 행동을 부여하는 역할을 합니다.
게임 매니저의 가장 핵심이 되는 역할을 수행한다고 봅니다.

GameSession에 대한 명령은
Desk.js의 NewSession과
Desk.js의 InitRunningSessionReader에서 이뤄집니다.

*/


initializeRooms = function () {

    // 유저목록을 sortable 리스트로 변경합니다.
    $(".userlist").sortable({
        change: function () {
            if (GameSession.current) {
                GameSession.current.SetDirty();
            }
        }
    }).disableSelection();

    // 유저 검색을 초기화합니다.
    UserFounder.Initialize();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class GameSession
GameSession = function () {

    var self = this;

    // 서버에서 전달받은 데이터
    this.data = null;
    // 현재 매니저 상에서만 변경된 데이터
    this.applyData = {};

    this.listObject = null;
    this.IsDirty = false;
    this.IsNew = true;

    // 게임ID
    this.GetGameSN = function () { return (this.applyData.game ? this.applyData.game : (this.data.game ? this.data.game.SN : 0)); };
    // 게임 맵ID
    this.GetLevel = function () { return (this.applyData.Level ? this.applyData.Level : this.data.Level); };
    //세부 내용 360영상
    this.GetDetail = function () { return (this.applyData.Detail ? this.applyData.Detail : this.data.Detail) };
    // 변경사항이 있다는걸 체크한다.
    this.SetDirty = function () { this.IsDirty = true; }

    // 현재 세션이 보여지고있는 세션인지 논리값으로 알려준다.
    this.IsShowing = function () {
        return this.listObject && this.listObject.find("input").prop("checked");
    }

    // 세션의 이름을 알려줍니다.
    this.GetName = function (make) {
        if (this.applyData.Name) return this.applyData.Name;
        if (this.data.Name) return this.data.Name;
        return make ? this.MakeName() : "";
    }

    // 세션의 이름을 생성합니다.
    this.MakeName = function () {
        if (GameSession.current == this) {
            var users = $.grep($(".teacherItem"), function (n, i) { return n.id; });
            if (users.length == 0) {
                return "---";
            } else if (users.length == 1) {
                return $(users[0]).data("member").Name;
            } else {
                return "{0} 외 {1}명".format($(users[0]).data("member").Name, users.length - 1);
            }
        }
        else {
            if (this.data.members.length == 0) {
                return "---";
            } else if (this.data.members.length == 1) {
                return this.data.members[0].member.Name;
            } else {
                return "{0} 외 {1}명".format(this.data.members[0].member.Name, this.data.members.length - 1);
            }
        }
    }

    // 상단의 세션 목록에서의 화면을 갱신합니다.
    // 우선 html DOM을 생성하며 이벤트를 부여합니다.
    // 그 후, 이미 생성된 DOM에 대해서는 정보만을 변경합니다.
    // 
    // showing이 true인 경우에는 detail로 보여줍니다.
    this.Refresh = function (showing) {

        // 목록에 보여줄 대상이 없으면 새로 생성합니다.
        if (this.listObject == null) {
            this.listObject = $("<div id='session{0}' class='item'><label for='sessradio{0}'></label><input type='radio' name='session' id='sessradio{0}' style='z-index:1;' /></div>".format(this.data.SN));
            this.listObject.data("session", this);
            $("#roomList").children().last().before(this.listObject);

            //기기 수
            var deviceCount = document.getElementById("DeviceCount");
            if (deviceCount) {
                deviceCount.innerHTML = '총 인원: 0명';
            }
            // convert radio button
            var input = this.listObject.find("input");
            input.checkboxradio({ icon: false });
            input.change(function () {
                self.Refresh();
            });
            $('#observerBtn').hide();
        }

        // Change Label
        var label = this.listObject.find("label");
        var text = "";
        {
            text += this.GetName(true) + "<br />";
            text += (this.data.game ? this.data.game.Name : "[-]") + "<br />";
            switch (this.data.State) {
                case 0: //생성중
                    text += "[생성]";
                    break;
                case 1: //단말기 할당중
                    text += "[로그인중]";
                    break;
                case 2: //실행대기
                    text += "[실행대기]";
                    break;
                case 3: //착용대기
                    text += "[실행중]";
                    break;
                case 4: //시작대기
                    text += "[시작대기]";
                    break;
                case 5: //게임중
                    var elapsed = Math.floor((new Date() - Date.parse(this.data.StartDate)) / 1000);
                    text += "[ {0}:{1} ]".format((elapsed / 60).to00(), (elapsed % 60).to00());
                    break;
                case 6: //게임엔딩
                    text += "[종료]";
                    break;
                case 7: //게임완료
                    text += "[반납]";
                    break;
            }
            this.listObject.attr("data-state", this.data.State);
        }
        label.html(text);

        if (this.data.State === 1) {
            var forcedStartBtn = $("#forcedStartBtn");
            forcedStartBtn.button("enable");
        }

        // Show Detail
        if (showing) {
            this.listObject.find("input").prop("checked", true).change();
        }

        if (this.IsShowing()) {
            $("#contentBlock").show("fade", 200);
            this.RefreshDetail();
        }
    }

    // 세션을 선택했을때 세션에 대한 상세내용에 대한 화면을 갱신합니다.
    this.RefreshDetail = function () {
        var teacherList = $("#teacherlist");
        var userList = $("#userlist");
        var self = this;

        // Refresh대상이 이전과 바뀌었을 경우
        // 즉, 다른 것을 선택하고 있다가, 해당 세션을 선택했을경우에
        // 초기화 및 데이터 설정을 바꾼다.
        var isFirst = GameSession.current != this;
        if (isFirst) {
            var fastStartBtn = $("#fastStartBtn");
            var forcedStartBtn = $("#forcedStartBtn");
            var applyBtn = $("#applyBtn");
            var actionBtn = $("#actionBtn");
            var removeBtn = $("#removeBtn");

            $(".teacherlist .teacherItem").remove();
            $(".userlist .userItem").remove();

            teacherList.find(".teacherItem").each(function (i, dom) {
                if (dom.id && $(dom).data("seed") != seed) {
                    $(dom).remove();
                }
            });
            // 삭제된 유저들 삭제
            userList.find(".userItem").each(function (i, dom) {
                if (dom.id && $(dom).data("seed") != seed) {
                    $(dom).remove();
                }
            });

            fastStartBtn.button("enable");
            fastStartBtn.data("run", false);
            forcedStartBtn.button("enable");
            forcedStartBtn.data("run", false);
            applyBtn.button("disable");
            applyBtn.data("run", false);
            actionBtn.button("enable");
            actionBtn.data("run", false);
            removeBtn.button("enable");
            removeBtn.data("run", false);

            $('#roomDeleteIcon').remove();
            //삭제버튼
            // create dom
            let img = document.createElement('img');
            img.setAttribute("id", "roomDeleteIcon");
            img.src = '../Content/Images/room_delete_icon.png';
            img.style.position = 'absolute';
            img.style.marginLeft = '-19px';
            img.style.marginTop = '-5px';
            img.onclick = function () {
                $("#removeBtn").trigger('click');
            };
            self.listObject.append(img);
        }
        if (isFirst || !this.IsDirty) {
            GameSession.current = this;

            var roomName = $("#roomName");
            if (!roomName.is(":focus")) $("#roomName").val(this.GetName(false));
            if (isFirst) $("#roomName").select();

            //게임이 시작된 경우
            if (this.data.game) {
                $("#game-" + this.GetGameSN()).prop("checked", true).change();
                if (this.data.game.Name === "Safety") {
                    var num = 0;
                    switch (this.data.Level) {
                        case 1:
                            num = 4;
                            break;
                        case 2:
                            num = 1;
                            break;
                        case 3:
                            num = 2;
                            break;
                        case 4:
                            num = 5;
                            break;
                        case 5:
                            num = 3;
                            break;
                        case 6:
                            num = 6;
                            break;
                        case 7:
                            num = 7;
                            break;
                    }
                    num = num.toString(2);
                    for (var i = 0; i < this.data.game.MaxLevel; i++) {
                        if (i <= num.length - 1) {
                            if (num.charAt(num.length - 1 - i) === "1") {
                                $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", true).change();
                            } else {
                                $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", false).change();
                            }
                        } else {
                            $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", false).change();
                        }
                    }
                } else if (this.data.game.PackageName === "com.fantafo.videoplayer") {
                    $("#level-" + this.data.Level).prop("checked", true).change();
                    if (this.data.Detail) {
                        let list = $("#detailContentRow input[name='detail']");
                        let detail = this.data.Detail;
                        $.each(list, function (index, item) {
                            if (item.value.substr(0, 10) === detail.substr(0, 10)) {
                                $($("#detailContentRow input[name='detail']")[0]).prop('checked', true);
                            }
                        });
                    }
                } else if (this.data.game.PackageName === "com.fantafo.safetyEduaction") {
                    var num = this.data.Level.toString(2);
                    for (var i = 0; i < this.data.game.MaxLevel; i++) {
                        if (i <= num.length - 1) {
                            if (num.charAt(num.length - 1 - i) === "1") {
                                $("#level-" + (i + 1)).prop("checked", true).change();
                            } else {
                                $("#level-" + (i + 1)).prop("checked", false).change();
                            }
                        } else {
                            $("#level-" + (i + 1)).prop("checked", false).change();
                        }
                    }
                }
                else {

                    $("#level-" + this.data.Level).prop("checked", true).change();
                }
            }
            //게임이 아직 시작 안된경우
            else if (this.applyData.game) {
                $("#game-" + this.GetGameSN()).prop("checked", true).change();
                if (this.applyData.game == 4) {
                    var level = this.applyData.Level;
                    var num = this.applyData.Level.toString(2);
                    for (var i = 0; i < Fims.games[4].MaxLevel; i++) {
                        if (i <= num.length - 1) {
                            if (num.charAt(num.length - 1 - i) === "1") {
                                $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", true).change();
                            } else {
                                $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", false).change();
                            }
                        } else {
                            $("#level-" + Fims.games[4].SafetyType[i]).prop("checked", false).change();
                        }
                    }
                    this.applyData.Level = level;
                }
                else if (this.applyData.game == 14) {
                    var level = this.applyData.Level;
                    var num = this.applyData.Level.toString(2);
                    for (var i = 0; i < Fims.games[14].MaxLevel; i++) {
                        if (i <= num.length - 1) {
                            if (num.charAt(num.length - 1 - i) === "1") {
                                $("#level-" + (i + 1)).prop("checked", true).change();
                            } else {
                                $("#level-" + (i + 1)).prop("checked", false).change();
                            }
                        } else {
                            $("#level-" + (i + 1)).prop("checked", false).change();
                        }
                    }
                    this.applyData.Level = level;
                }
            }
            else {
                $("#game_hide").prop("checked", true).change();
                $(".level .list").html("");
                $(".detail .list").html("");
                $("#detailContentRow").hide();
            }
        }

        //기기 수
        var deviceCount = document.getElementById("DeviceCount");
        if (this.applyData.members || this.data.members) {
            if (this.applyData.members && this.applyData.members.length > 0) {
                deviceCount.innerHTML = '총 인원: ' + this.applyData.members.length + '명';
            } else if (this.data.members.length > 0) {
                deviceCount.innerHTML = '총 인원: ' + this.data.members.length + '명';
            }
        } else {
            deviceCount.innerHTML = '총 인원: 0명';
        }

        // 유저목록 재지정
        var seed = Math.random() * 1000000;
        var RefreshUser = function (user) {

            var dom = $("#user" + user.member.SN);
            if (!dom || dom.length == 0) {
                if ($(".teacherItem").length == 0) {
                    teacherList.append("<li id='user{0}' class='teacherItem ui-state-default'><a class='remove namebtn'></a><div class='name'></div><div class='device'></div><div class='game'></div><div class='state live hide'></div><div class='state conn hide'></div><div class='state controller hide'></div><div class='state star hide'></div></li>".format(user.member.SN));
                } else {
                    userList.append("<li id='user{0}' class='userItem ui-state-default'><a class='remove namebtn'></a><div class='name'></div><div class='device'></div><div class='game'></div><div class='state live hide'></div><div class='state conn hide'></div><div class='state controller hide'></div><div class='state star hide'></div></li>".format(user.member.SN));
                }
                dom = $("#user" + user.member.SN);
                dom.data("user", user);
                dom.data("member", user.member);

                dom.find(".remove").button().on("click", function () {
                    //if (self.applyData.member.Live)
                    //    return;

                    // 추가 멤버중에 삭제하려는 대상이 있다면 추가멤버중에서 삭제한다.
                    if (self.applyData.members) {
                        var grep = $.grep(self.applyData.members, function (n, i) { return n.member.SN == user.member.SN });
                        if (grep.length > 0) {
                            Fims.Send("/api/Device/SetPickDevice/" + dom.data("member").SN, { set: false }, function (result, ajax) { });
                            self.applyData.members = $.grep(self.applyData.members, function (n, i) { return n.member.SN != user.member.SN });
                            self.SetDirty();
                            self.Refresh();
                            return;
                        }
                    }
                    //이미 실행 중인 유저를 삭제
                    var remove = true;
                    if (self.data.State >= 5) {
                        remove = confirm("현재 실행중이지만 해당 유저를 제거 하겠습니까?");
                    }
                    if (self.data.members && remove) {
                        var grep = $.grep(self.data.members, function (n, i) { return n.member.SN == user.member.SN });
                        if (grep.length > 0) {
                            Fims.Send("/api/Device/SetPickDevice/" + dom.data("member").SN, { set: false }, function (result, ajax) { });
                            var device = $.grep(self.data.members, function (n, i) { return n.member.SN != user.member.SN }, true);
                            Fims.Send("/api/Game/RemoveMember",
                                {
                                    SN: self.data.SN,
                                    deviceSN: device[0].member.SN
                                },
                                function (result, ajax) {
                                });

                            self.SetDirty();
                            self.Refresh();
                            return;
                        }
                    }
                });

                var launchButton = dom.find(".refresh");
                launchButton.button().on("click", function () {
                    // 해당 맴버의 어플을 재시작한다.
                    if (self.data.State >= 3) {

                        launchButton.button("disable");
                        setTimeout(function () {
                            launchButton.button("enable");
                        }, 5);

                        Fims.Send("/api/Game/Relaunch",
                            {
                                memberSN: dom.data("member").member.SN,
                                sessionSN: self.data.SN
                            },
                            function (data, ajax) {
                            });
                    }
                });

                var renamebtn = dom.find(".rename");
                renamebtn.button().on("click", function () {
                    var name = prompt("변경할 이름을 적어주세요.", user.Name);
                    if (name) {
                        if (name.indexOf(";") == -1) {
                            dom.data("newName", name);
                            user.Name = name;
                            RefreshName();
                        } else {
                            alert("이름에 사용할 수 없는 문자가 포함되어 있어서 적용하지 못했습니다.");
                        }
                    }
                });
                changed = true;
            }
            //--END if (!dom || dom.length == 0) 

            dom.data("seed", seed);

            // 수정
            function RefreshName() {
                var name = dom.data("newName");
                if (!name) name = user.Name;
                if (!name) name = user.Name = user.member.Name;
                dom.find(".name").text("{0}".format(name));
            };
            RefreshName();

            // 유저가 있다면??

            if (user.member.UserName) {
                dom.find(".device")
                    .text("{0}".format(user.member.UserName))
                    .css("display", "inline-block");
            }
            else {
                dom.find(".device").css("display", "none");
            }

            //컨트롤러
            if (user.member.Cont) {
                dom.find(".controller.hide").removeClass("hide");
            }
            else {
                dom.find(".controller").addClass("hide");
            }

            // 게임이 실행중인 상태
            if (user.member.Live) {
                dom.find(".live.hide").removeClass("hide");
            }
            else {
                dom.find(".live").addClass("hide");
            }

            // 게임 서버에 접속중이라면
            if (user.Connected) {
                dom.find(".conn.hide").removeClass("hide");
            }
            else {
                dom.find(".conn").addClass("hide");
            }

            if (self.data.State >= 5 && user.Score > 0) {
                dom.find(".game").text(user.Score).css("display", "inline-block");
            } else {
                dom.find(".game").css("display", "none");
            }
        }
        //--END RefreshUser()

        // 데이터속 유저들
        for (var i = 0; i < this.data.members.length; i++) {
            var user = this.data.members[i];
            if (this.data.Score < user.Score) {
                this.data.Score = user.Score;
            }
        }
        //데이터속 유저들 + 1등 아이콘 생성
        for (var i = 0; i < this.data.members.length; i++) {
            var user = this.data.members[i];
            var dom = $("#user" + user.member.SN);
            if (this.data.Score === user.Score && this.data.Score > 0) {
                dom.find(".star.hide").removeClass("hide");
            } else {
                dom.find(".star").addClass("hide");
            }
            if ($.inArray(user.member.SN, this.applyData.removeMembers) == -1) {
                RefreshUser(user);
            }
        }
        // 추가데이터속 유저들
        if (this.applyData.members) {
            for (var i = 0; i < this.applyData.members.length; i++) {
                var user = this.applyData.members[i];
                RefreshUser(user);
            }
        }
        teacherList.find(".teacherItem").each(function (i, dom) {
            if (dom.id && $(dom).data("seed") != seed) {
                $(dom).remove();
            }
        });
        // 삭제된 유저들 삭제
        userList.find(".userItem").each(function (i, dom) {
            if (dom.id && $(dom).data("seed") != seed) {
                $(dom).remove();
            }
        });

        // 세션 PlaceHoler 재지정
        $("#roomName").attr("placeholder", this.MakeName());

        var fastStartBtn = $("#fastStartBtn");
        var forcedStartBtn = $("#forcedStartBtn");
        var applyBtn = $("#applyBtn");
        var actionBtn = $("#actionBtn");
        var stopBtn = $("#stopBtn");
        var removeBtn = $("#removeBtn");
        switch (this.data.State) {
            case 0: //생성중
                {
                    fastStartBtn.css("display", "inline-block");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "none");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.hide("fade", 200);
                    applyBtn.hide("fade", 200);
                    this.SetDirty();
                }
                break;
            case 1: //할당중
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "inline-block");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "inline-block");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("로그인중..");
                    if (!actionBtn.data("run")) actionBtn.button("disable");
                }
                break;
            case 2: //실행대기
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "inline-block");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("실행");
                    if (!actionBtn.data("run")) actionBtn.button("enable");
                }
                break;
            case 3: //착용대기
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "inline-block");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("시작");
                    if (!actionBtn.data("run")) actionBtn.button("disable");
                }
                break;
            case 4: //시작대기
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "inline-block");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("시작");
                    if (!actionBtn.data("run")) actionBtn.button("enable");
                }
                break;
            case 5: //게임중
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "none");
                    stopBtn.css("display", "inline-block");
                    removeBtn.css("display", "none");

                    actionBtn.text("종료");
                    if (!actionBtn.data("run")) actionBtn.button("enable");
                }
                break;
            case 6: //게임중
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "none");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("종료");
                    if (!actionBtn.data("run")) actionBtn.button("enable");
                }
                break;
            case 7: //게임완료
                {
                    fastStartBtn.css("display", "none");
                    forcedStartBtn.css("display", "none");
                    applyBtn.css("display", "none");
                    actionBtn.css("display", "none");
                    stopBtn.css("display", "none");
                    removeBtn.css("display", "none");

                    actionBtn.text("반납처리");
                    if (!actionBtn.data("run")) actionBtn.button("enable");
                }
                break;
        }

        //삭제버튼
        // create dom
        if (!self.listObject.find($('img'))){
            let img = document.createElement('img');
            img.src = '../Content/Images/room_delete_icon.png';
            img.style.position = 'relative';
            img.style.top = '-30px';
            img.style.left = '-20px';
            img.style.zIndex = 2;
            img.style.marginLeft = '-20px';
            img.style.marginTop = '-20px';
            img.onclick = function () {
                $("#removeBtn").trigger('click');
            };
            self.listObject.append(img);
        };

        //시작버튼
        $('fastStartBtn').css("display", "inline-block");
    };

    this.Remove = function () {
        if (this.IsShowing()) {
            $("#contentBlock").hide("fade", 200);
        }

        if (this.listObject) this.listObject.remove();

        //let fastStartBtn = $("#fastStartBtn");
        //if (fastStartBtn) fastStartBtn.css("display", "none");

        if ($("#fastStartBtn"))
            $("#fastStartBtn").css("display", "none");
        if ($("#stopBtn"))
            $("#stopBtn").css("display", "none");

        this.listObject = undefined;
    };

    // 새로운 유저를 추가합니다.
    this.AddUser = function (elemClass) {

        var user = $(elemClass).data("member");
        user.Live = false;
        user.Name = null;
        user.Cont = false;
        if (!user) return;

        try {
            if (this.applyData.removeMembers) {
                var removeList = $.grep(this.applyData.removeMembers, function (n, i) { return n != user.SN; });
                if (removeList.length < this.applyData.removeMembers.length) {
                    this.applyData.removeMembers = removeList;
                    this.SetDirty();
                    return;
                }
            }

            if (!this.applyData.members) this.applyData.members = [];
        }
        finally {
            if ($(".teacherItem").length == 0) {
                this.applyData.members.unshift(user);
            } else {
                this.applyData.members.push(user);
            }
            this.SetDirty();
            this.Refresh();
        }
    };

    // 
    this.OnApply = function (fast) {

        if (!this.IsDirty) return;
        if (GameSession.IsApplying) return;

        var dt = this.data;
        var ad = this.applyData;
        var data = {};

        data.SN = this.IsNew ? 0 : this.data.SN;
        data.Name = ad.Name ? ad.Name : "";
        data.Level = this.GetLevel();
        data.Detail = this.GetDetail();
        data.Game = this.GetGameSN();
        data.Fast = fast;
        data.Members = [];
        let isobserver = $('#observerBtn').find(':input')[0].checked;
        if (isobserver) {
            data.observer = isobserver;
        } else {
            data.observer = false;
        }
        $(".teacherItem").each(function (i, n) {
            var user = $(n).data("user");
            data.Members.push(user.member.SN + ";" + user.Name);
        });

        $(".userItem").each(function (i, n) {
            var user = $(n).data("user");
            data.Members.push(user.member.SN + ";" + user.Name);
        });

        if ($(".teacherItem").length == 0) {
            alert("선생님을 추가해주세요.");
            return;
        } else if (data.Members.length == 0) {
            alert("유저를 추가해주세요.");
            return;
        }
        else if (!data.Game) {
            alert("게임을 선택해주세요.");
            return;
        }
        //else if (!data.Level || data.Level === 0) {
        //    for (var i = 0; i < Fims.games[data.Game].MaxLevel; ++i) {
        //        var test = $("#level");
        //    }
        //    if (data.Game < 8 || data.Game > 11) {
        //        alert("레벨을 선택해주세요.");
        //        return;
        //    }
        //}
        else if (!Fims.games[data.Game] || Fims.games[data.Game].PackageName == "disable") {
            alert("선택할 수 없는 게임입니다. 다른 게임을 선택 해 주세요.");
            return;
        }

        if ($("#level-1") && $("#level-1").attr('type') === "checkbox") {
            data.Level = 0;
            for (var i = 1; i <= Fims.games[data.Game].MaxLevel; ++i) {
                if ($("#level-" + i).is(":checked")) {
                    data.Level += Math.pow(2, i-1);
                }
            }
        }

        if (data.Level === 0) {
            if (Fims.games[data.Game].Name === "360 video" && !data.Detail) {
                alert("실행할 영상을 선택해 주세요.");
                return;
            }
            data.Level = 1;
        }

        var applyBtn = $("#applyBtn");
        applyBtn.button("disable");

        var fastStartBtn = $("#fastStartBtn");
        fastStartBtn.button("disable");

        var forcedStartBtn = $("#forcedStartBtn");
        forcedStartBtn.button("disable");

        GameSession.IsApplying = true;

        /* 현재 2^0 화학, 2^1 전기, 2*^2 생물
         * 1. 화학
         * 2. 전기
         * 3. 화학/전기
         * 4. 생물
         * 5. 화학/생물
         * 6. 전기/생물
         * 7. 화학/전기/생물
         * 아래로 변경 요청 ( 클라가 )
         * 1. 생물
         * 2. 화학
         * 3. 전기
         * 4. 화학/생물
         * 5. 화학/전기
         * 6. 전기/생물
         * 7. 화학/전기/생물
         * */
        if (data.Game === 4) {
            switch (data.Level) {
                case 1:
                    data.Level = 2;
                    break;
                case 2:
                    data.Level = 3;
                    break;
                case 3:
                    data.Level = 5;
                    break;
                case 4:
                    data.Level = 1;
                    break;
                case 5:
                    data.Level = 4;
                    break;
                case 6:
                    data.Level = 6;
                    break;
                case 7:
                    data.Level = 7;
                    break;
            }
        }

        Fims.Send("/api/Game/Apply/" + (this.IsNew ? 0 : this.data.SN), data, function (data, ajax) {
            GameSession.IsApplying = false;
            if (ajax.status === 200) {
                if (self.IsNew) {
                    applyBtn.button("enable");
                    Fims.sessions[data.SN] = self;
                    self.data = data;
                    self.IsUpdate = true;
                    self.IsNew = false;
                    //self.Remove();
                    self.Refresh(true);
                    self.applyData = {};
                }
            } else {
                alert(ajax.responseText);
                applyBtn.button("enable");
                fastStartBtn.button("enable");
            }
        });
    };

    this.OnStop = function () {
        Fims.Send("/api/Game/Stop/" + self.data.SN, { stopServer: 1, state: 0 }, function (data, ajax) {
            $("#fastStartBtn").button("enable");
        });
    };

    this.OnRemove = function () {
        // 아직 생성하지 않은 상태라면 그냥 페이지에서만 삭제한다.
        if (this.IsNew) {
            if (this.applyData.members) {
                for (device of this.applyData.members) {
                    Fims.Send("/api/Device/SetPickDevice/" + device.member.SN, { set: false }, function (result, ajax) { });
                }
            }
            this.Remove();
            $("#contentBlock").hide("fade", 200);
        }
        // 이미 생성된 아이라면 서버에 신호를 보낸다. 
        else {
            _OnAction(6);
        }

    };

    this.OnInit = function () {
        // 아직 생성하지 않은 상태라면 그냥 페이지에서만 삭제한다.
        if (this.IsNew) {
            if (this.applyData.members) {
                for (device of this.applyData.members) {
                    Fims.Send("/api/Device/SetPickDevice/" + device.member.SN, { set: false }, function (result, ajax) { });
                }
                self.applyData.members = [];
                self.SetDirty();
                let deviceCount = document.getElementById("DeviceCount");
                if (deviceCount) {
                    deviceCount.innerHTML = '총 인원: 0명';
                }
                self.Refresh();
            }
        }
        // 이미 생성된 아이라면 서버에 신호를 보낸다. 
        else {
            swal('방 삭제만 가능합니다.');
        }

    };

    this.OnForcedStart = function () {
        Fims.Send("/api/Game/ForcedStart/" + self.data.SN, null, function (data, ajax) {
            actionBtn.data("run", false);
            applyBtn.button("enable");
            if (ajax.status != 200) {
                alert(ajax.responseText);
            }
        });
    };

    this.OnAction = function () {
        _OnAction(this.data.State);
    };
    function _OnAction(state) {
        var fastStartBtn = $("#fastStartBtn");
        fastStartBtn.button("disable");

        var forcedStartBtn = $("#forcedStartBtn");
        forcedStartBtn.button("disable");

        var applyBtn = $("#applyBtn");
        applyBtn.button("disable");

        var actionBtn = $("#actionBtn");
        actionBtn.data("run", true);
        actionBtn.button("disable");

        var removeBtn = $("#removeBtn");

        switch (state) {
            case 2: //대기중이니 게임을 실행시키자
                Fims.Send("/api/Game/Launch/" + self.data.SN, null, function (data, ajax) {
                    actionBtn.data("run", false);
                    applyBtn.button("enable");
                    if (ajax.status !== 200) {
                        alert(ajax.responseText);
                    }
                });
                break;
            case 4: //착용이 완료됐으니 게임을 시작해보자
                Fims.Send("/api/Game/Start/" + self.data.SN, null, function (data, ajax) {
                    actionBtn.data("run", false);
                    applyBtn.button("enable");
                    if (ajax.status != 200) {
                        alert(ajax.responseText);
                    }
                });
                break;
            case 5: //뭐야? 문제가 생긴거야? 게임종료해야지
            case 6:
                removeBtn.button("disable");
                Fims.Send("/api/Game/Stop/" + self.data.SN, { stopServer: 1 }, function (data, ajax) {
                    actionBtn.data("run", false);
                    applyBtn.button("enable");
                    removeBtn.button("enable");
                    if (ajax.status === 200) {
                        Fims.Send("/api/Game/Return/" + self.data.SN, null, function (data, ajax) {
                            actionBtn.data("run", false);
                            applyBtn.button("enable");

                            if (ajax.status !== 200) {
                                alert(ajax.responseText);
                            }
                        });
                    }
                    else if (ajax.status !== 200) {
                        alert(ajax.responseText);
                    }
                });
                break;
            case 7: //반납처리를 완료해보자
                Fims.Send("/api/Game/Return/" + self.data.SN, null, function (data, ajax) {
                    actionBtn.data("run", false);
                    applyBtn.button("enable");
                    if (ajax.status != 200) {
                        alert(ajax.responseText);
                    }
                });
                break;
            default: // 기타
                actionBtn.data("run", false);
                applyBtn.button("enable");
                break;

        }
    }
};
GameSession.IsApplying = false;
GameSession.current = null;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// static class UserFounder
UserFounder = new function () {
    var self = this;
    var form = null;
    var input = null;
    var list = null;

    var isSearching = false;
    var beforeSearch = false;

    this.IsShow = function () {
        return form.css("display") != "none";
    };

    this.Open = function (teacher) {
        form.show("fade", 200);
        isSearching = false;
        beforeSearch = "";
        input.val("");
        input.select();
        list.removeChildren();

        beforeSearch = "-";
        this.Searching(teacher);
    };
    this.Close = function () {
        form.hide("fade", 200);

        var scrollPosition = $("#category").offset().top;
        //스크롤 이동
        $("#contentBlock").animate({
            scrollTop: scrollPosition
        }, 500);
    };

    this.Initialize = function () {
        form = $(".popup.search");
        input = form.find("input");
        list = form.find(".result");

        form.keyup(function (e) {
            // Esc
            if (e.keyCode == 27) {
                self.Close();
                return false;
            }

            self.Searching();
        });
    };

    function getValue() {
        var val = input.val();
        if (!val) return "";
        return val.replace("-", "");
    }

    //teacher == 1 선생님 //teacher == 0 학생
    this.Searching = function (teacher) {
        if (getValue() === beforeSearch) return;

        if (!isSearching) {
            isSearching = true;
            setTimeout(function () {
                //서버와 연결된 기기를 가져온다.
                Fims.Send("/api/Device/FindConnectDevice", null, function (data, ajax) {
                    if (data !== null)
                        data.sort(sortAlphaNum);

                    if (ajax.status == 200) {
                        list.removeChildren();

                        var Multidevice = [];
                        Multidevice[0] = 5;
                        Multidevice[1] = 10;
                        Multidevice[2] = 30;
                        //한번에 다수의 기기 선택
                        for (var i = 0; i < 3; ++i) {
                            var dom = $("<a>기기 자동 선택 0 ~ {0}</a>"
                                .format(Multidevice[i]));
                            list.append(dom);
                            dom.data("count", Multidevice[i]);

                            var aa = dom.data("count");
                            dom.button();

                            dom.click(function () {
                                var self = $(this);
                                Fims.Send("/api/Device/SetMultiPickDevice/", { count: $(self).data("count") }, function (result, ajax) {
                                    if (ajax.status == 200) {
                                        var devicelist = result;
                                        for (var i = 0; i < devicelist.length; ++i) {
                                            GameSession.current.AddUser(".search" + devicelist[i]);
                                            $(".search" + devicelist[i]).remove();
                                        }
                                    } else {
                                        //ajax.status == 400 처리
                                    }
                                });

                            });

                        }

                        let serachText = getValue();

                        for (var i = 0; i < data.length; i++) {
                            var user = data[i];

                            if (serachText && user.Name) {
                                if (user.Name.toLowerCase().indexOf(serachText.toLowerCase()) === -1)
                                    continue;
                            }
                                
                            if (user) {
                                //중복 유저 검출
                                if (GameSession.current) {
                                    var sess = GameSession.current;

                                    for (var j = 0; j < sess.data.members.length; j++) {
                                        var sessUser = sess.data.members[j];
                                        if (sessUser.member && sessUser.member.SN == user.SN) {
                                            user = null;
                                            break;
                                        }
                                    }
                                    if (user && sess.applyData && sess.applyData.members) {
                                        for (var j = 0; j < sess.applyData.members.length; j++) {
                                            var sessUser = sess.applyData.members[j];
                                            if (sessUser && sessUser.member.SN == user.SN) {
                                                user = null;
                                                break;
                                            }
                                        }
                                    }
                                }
                                // 수정
                                if (user) {
                                    var dom = $("<a class='search{0}'><div class='name'>{1}</div></a>"
                                        .format(user.SN, user.Name));
                                    list.append(dom);

                                    dom.data("member", { Name: user.Name, member: user });
                                    dom.button();

                                    dom.click(function () {
                                        var self = $(this);
                                        Fims.Send("/api/Device/SetPickDevice/" + $(self).data("member").member.SN, { set: true }, function (result, ajax) {
                                            if (ajax.status == 200) {
                                                if (result) {
                                                    GameSession.current.AddUser(".search" + $(self).data("member").member.SN);
                                                    $(self).remove();
                                                } else {
                                                    alert("다른 방에서 선택된 기기입니다.");
                                                    $(self).remove();
                                                }
                                            } else {
                                                $(self).remove();
                                                //ajax.status == 400 처리
                                            }
                                        });

                                    });
                                }
                            }
                        }
                    }
                    else {
                        list.html("검색 결과가 없습니다. 에이전트 On/Off 눌러주세요. 혹은 기기의 와이파이를 확인해주세요");
                    }
                });
                isSearching = false;
            }, 200);
        }
    };

    function To00(v) { return v > 9 ? v : "0" + v; }
};

function sortAlphaNum(a, b) {

    var aA = a.Name.replace(reA, "");
    var bA = b.Name.replace(reA, "");
    if (aA === bA) {
        var aN = parseInt(a.Name.replace(reN, ""), 10);
        var bN = parseInt(b.Name.replace(reN, ""), 10);
        return aN === bN ? 0 : aN > bN ? 1 : -1;
    } else {
        return aA > bA ? 1 : -1;
    }
}