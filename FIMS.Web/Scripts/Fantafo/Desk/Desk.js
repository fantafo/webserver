/*

FIMS 매니저의 기본적인 세팅을 하는 스크립트입니다.
실제 FIMS 시스템과 상관은 없는 부분이 크며,

매니저에서의 행동을 설정하고,
"FIMS 시스템에 긴밀한 역할을 할" 스크립트들을 실행하는 역할을 합니다.

*/

// 페이지가 로딩 완료됐을때 초기화를 시작합니다.
$(document).ready(function () {
    $("input[type=submit], a, button, .item").button();
    $("input[type='radio']").checkboxradio({
        icon: false
    });

    Fims.Initialize();
    initializeRooms();
    initializeDevices();    
});

function AjaxCall(url) {
    $.getJSON(url, function (data) {
        if (data && data.msg) {
            swal(data.msg);
        }
    });
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// class Fims
Fims = new function () {
    var self = this;

    // Dictionary<int, GameSession>
    this.sessions = {};

    // Dictionary<int, Game>
    this.games = {};

    this.idSessionReader;

    this.observerAutoStart = false;

    // void Send(string url, dynamic data, Action<dynamic, ajax> callback)
    this.Send = function (url, data, callback) {
        $.ajax(url, {
            method: "GET",
            data: data,
            contentType: "application/json; charset=UTF-8",
            success: function (data, type, ajax) { callback(data, ajax); },
            error: function (ajax) { callback(null, ajax); },
        });
    };

    // 창 크기가 바뀌었을때 바뀔 대상들을 조절합니다.
    this.OnResize = function () {
        $(".contentWidth").each(function (i, e) {
            var elem = $(e);

            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;
            //하드코딩 devicewindow 길이 ,  #contentBlock .contentRow margin 적용
            var width = ($(target).width() - 140) * 0.94;
            elem.width(width + margin);
        });
        $(".contentWidth2").each(function (i, e) {
            var elem = $(e);

            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;
            //하드코딩 devicewindow 길이 ,  #contentBlock .contentRow margin 적용
            var width = ($(target).width()) * 0.94;
            elem.width(width + margin);
        });
        $(".contentHeight").each(function (i, e) {
            var elem = $(e);

            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;

            var height = $(target).height();
            elem.height(height + margin);
        });

        $("#contentBlock").each(function (i, e) {
            var elem = $(e);
            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;

            var height = $(target).height() * 0.7;
            elem.css('max-height', height + margin);
            //elem.height(height + margin);
            elem.css('min-height', '500px');
        });

        $(".search.contentWidth").each(function (i, e) {
            var elem = $(e);

            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;
            //하드코딩 devicewindow 길이 ,  #contentBlock .contentRow margin 적용
            var width = ($(target).width()) * 0.6;
            elem.width(width + margin);
        });

        $(".search .contentHeight").each(function (i, e) {
            var elem = $(e);

            var target = elem.data("mtarget");
            if (!target) target = elem.parents(".contentWindow")[0];
            if (!target) target = window;
            if (target === "window") target = window;
            var margin = $(e).data("margin");
            if (!margin) margin = 0;

            var height = $(target).height() * 0.5;
            elem.height(height + margin);
        });
    };

    // 매니저를 초기화합니다.
    // - 서버에서 게임에 대한 정보를 가져옵니다.
    this.Initialize = function () {
        Fims.Send("/api/Data/Games", null, function (data, ajax) {
            if (ajax.status === 200) {
                for (var i = 0; i < data.length; i++) {
                    Fims.games[data[i].SN] = data[i];
                }

                initializeOnLoadCompleted();
            }
            else {
                alert("서버에 접속할 수 없습니다.");
                $(document).html("");
            }
        });
        
        //Agent on/off 값 가져오기
        Fims.Send('/api/Agent/GetToggleAgentValue', null, function (data) {
            //data value : true or false
            if (data) {
                $('#toggleAgents').find(':input')[0].checked = data;
            } else {
                $('#toggleAgents').find(':input')[0].checked = false;
            }
        });

        $('#toggleAgents').find(':input').change(function () {
            let toggle = $('#toggleAgents').find(':input')[0].checked;
            var data = {};
            data.toggle = toggle;
            Fims.Send('/api/Agent/ToggleAgents', data, function () {
            });
        });

        //잠금 화면 on/off 값 가져오기
        Fims.Send('/api/Agent/GetLockScreenValue', null, function (data) {
            //data value : true or false
            if (data) {
                $('#toggleLock').find(':input')[0].checked = data;
            } else {
                $('#toggleLock').find(':input')[0].checked = false;
            }
        });


        $('#toggleLock').find(':input').change(function () {
            let toggle = $('#toggleLock').find(':input')[0].checked;
            if (toggle) {
                Fims.Send('/api/Agent/RunBlackLockScreenApp', null, function () {
                });
            } else if (toggle === false) {
                Fims.Send('/api/Agent/StopBlackLockScreenApp', null, function () {
                });
            } 
            
        });
    }

    // 서버에서 정보를 모두 가져왔다면,
    // 가져온 데이터를 기반으로 매니저를 초기화합니다.
    function initializeOnLoadCompleted() {
        InitCreateGameAndLevel();
        InitRunningSessionReader();
        InitEvents();

        $(window).resize(self.OnResize);
        self.OnResize();
    }

    // 세션 매뉴에 게임과 맵을 생성합니다.
    function InitCreateGameAndLevel() {
        var gameList = $(".game .list");
        $.each(Fims.games, function (k, game) {
            if (!game || !game.Enabled)
                return;
            var dom;
            switch (game.Name) {
                case "cnuh":
                    dom = $("<input type='radio' name='game' id='game-{0}' value='{0}' /><label for='game-{0}'>{1}</label>".format(game.SN, "환자 VR 안내"));
                    break;
                case "180 video":
                    dom = $("<input type='radio' name='game' id='game-{0}' value='{0}' /><label for='game-{0}'>{1}</label>".format(game.SN, "180°video"));
                    break;
                case "360 video":
                    dom = $("<input type='radio' name='game' id='game-{0}' value='{0}' /><label for='game-{0}'>{1}</label>".format(game.SN, "360°video"));
                    break;
                default:
                    dom = $("<input type='radio' name='game' id='game-{0}' value='{0}' /><label for='game-{0}'>{1}</label>".format(game.SN, game.Name));
                    break;
            }
            
            gameList.append(dom);

            // input
            var gameRadio = dom.eq(0).data("game", game);
            gameRadio.checkboxradio({ icon: false });
            gameRadio.change(function () {
                if (!GameSession.current) return;

                if (game.IsObserver === true) {
                    $('#observerBtn').show();
                } else {
                    $('#observerBtn').hide();
                }

                if (game.Name === '360 video') {
                    $("#detailContentRow").show();
                    $(".level").addClass("expansion");
                } else {
                    $("#detailContentRow").hide();
                    $(".level").removeClass("expansion");
                }

                // 게임이 없는 세션이거나 세션번호가 틀릴 때만 기록한다.
                if (!GameSession.current.data.game || game.SN !== GameSession.current.data.game.SN) {
                    if (GameSession.current.applyData.game !== game.SN) {
                        GameSession.current.applyData.Level = 0;
                    }
                    GameSession.current.applyData.game = game.SN;
                    GameSession.current.SetDirty();
                }
                else {
                    delete GameSession.current.applyData.game;
                }

                // create levels
                var levelList = $(".level .list");
                levelList.removeChildren();
                for (var i = 1; i <= game.MaxLevel; i++) {
                    if (game.Name === "Safety") {
                        dom = $("<input type='checkbox' name='level' id='level-{0}' value='{1}' /><label for='level-{0}'>{0}</label>".format(game.SafetyType[i - 1], i));
                    }
                    else if (game.Name === "cnuh") {
                        dom = $("<input type='radio' name='level' id='level-{0}' value='{0}' /><label for='level-{0}'>{1}</label>".format(i, '수술 안내'));
                    }
                    else if (game.PackageName === "com.fantafo.videoplayer") {
                        dom = $("<input type='radio' name='level' id='level-{0}' value='{0}' /><label for='level-{0}'>{1}</label>".format(i, 'name'));
                        if (game.Name === "180 video") {
                            switch (i) {
                                case 1:
                                    dom[1].innerHTML = '이빨이 썩었어요';
                                    dom[0].value = 0;
                                    break;
                                case 2:
                                    dom[1].innerHTML = '위험한 장난감';
                                    dom[0].value = 1;
                                    break;
                                case 3:
                                    dom[1].innerHTML = '늑돌이의 잠자기 여행';
                                    dom[0].value = 2;
                                    break;
                                case 4:
                                    dom[1].innerHTML = '나는 뭐든지 잘 해';
                                    dom[0].value = 3;
                                    break;
                            }
                        }
                        else if (game.Name === "360 video") {
                            let str = KORCategorys[i - 1];
                            if (getTextLength(str) > 12) {
                                str = textDecrease(str);
                            }
                            dom[1].innerHTML = str;
                        }
                        else if (game.Name === "360 video2") {
                            switch (i) {
                                case 1:
                                    dom[1].innerHTML = '기억의재구성';
                                    dom[0].value = 7;
                                    break;
                                case 2:
                                    dom[1].innerHTML = '시선';
                                    dom[0].value = 8;
                                    break;
                                case 3:
                                    dom[1].innerHTML = '안나n마리';
                                    dom[0].value = 9;
                                    break;
                            }
                        }
                        else if (game.Name === "360 video3") {
                            dom[0].innerHTML = i;
                            switch (i) {
                                case 1:
                                    dom[0].value = 10;
                                    break;
                                case 2:
                                    dom[1].value = 11;
                                    break;
                            }
                        }

                    } else if (game.PackageName === "com.fantafo.safetyEduaction") {
                        dom = $("<input type='checkbox' name='level' id='level-{0}' value='{0}' /><label for='level-{0}'>{0}</label>".format(i));
                        if (game.Name === "현대제철") {
                            switch (i) {
                                case 1:
                                    dom[1].innerHTML = '인트로';
                                    dom[0].value = 1;
                                    break;
                                case 2:
                                    dom[1].innerHTML = '지게차';
                                    dom[0].value = 2;
                                    break;
                                case 3:
                                    dom[1].innerHTML = '낙하';
                                    dom[0].value = 3;
                                    break;
                            }
                        }
                    }
                    else {
                        dom = $("<input type='radio' name='level' id='level-{0}' value='{0}' /><label for='level-{0}'>{0}</label>".format(i));
                    }
                    levelList.append(dom);
                    dom.eq(0).checkboxradio({ icon: false });
                    dom.eq(0).change(function () {
                        if (game.Name === '360 video') {
                            let detailList = $(".detail .list");
                            detailList.removeChildren();
                            for (let i = 0; i < game.DetailInfo.length; ++i) {
                                const info = game.DetailInfo[i].split('#');
                                if (info[0] && info[1] && info[0] === Categorys[$(this).val() - 1]) {
                                    let detaildom = $("<input type='radio' name='detail' id='detailLevel-{0}' value='{1}' /><label for='detailLevel-{0}'>{0}</label>".format(i, info[1]));
                                    if (getTextLength(info[1]) > 10) {
                                        detaildom[1].innerHTML = textDecrease(info[1]);
                                    } else {
                                        detaildom[1].innerHTML = info[1];
                                    }
                                    //detaildom[1].innerHTML = info[1].substr(0, 10) + '..';
                                    detaildom[0].value = info[1];
                                    detailList.append(detaildom);
                                    detaildom.eq(0).checkboxradio({ icon: false });
                                    detaildom.eq(0).change(function () {
                                        var detail = String($(this).val());

                                        //레벨이 다를경우에만 입력함.
                                        if (GameSession.current.data.Detail !== detail) {
                                            GameSession.current.applyData.Detail = detail;
                                            GameSession.current.SetDirty();
                                        }
                                        else {
                                            delete GameSession.current.applyData.Detail;
                                        }
                                    });
                                }
                            }
                        }
                        if (this.type === "radio") {
                            var lv = Number($(this).val());

                            //레벨이 다를경우에만 입력함.
                            if (GameSession.current.data.Level !== lv) {
                                GameSession.current.applyData.Level = lv;
                                GameSession.current.SetDirty();
                            }
                            else {
                                delete GameSession.current.applyData.Level;
                            }
                        }

                        var scrollPosition = $("#fastStartBtn").offset().top;

                        //스크롤 이동
                        $("body").animate({
                            scrollTop: scrollPosition
                        }, 500);
                    });
                }

                var scrollPosition = $(".level.contentRow").offset().top;

                //스크롤 이동
                $("#contentBlock").animate({
                    scrollTop: scrollPosition
                }, 500);
            });
        });
    }

    // 1초에 한번씩 전체 세션 정보를 가져온다.
    // 가져온 정보로 화면을 고친다.
    function InitRunningSessionReader() {
        var pending = false;
        self.idSessionReader = setInterval(function () {
            if (pending) return;
            pending = true;
            Fims.Send("/api/Session/ActiveSessions", null, function (data, ajax) {
                pending = false;
                if (ajax.status === 200) {

                    // 이전 세션들의 업데이트 플래그 해제
                    $.each(Fims.sessions, function (k, v) {
                        if (v) v.IsUpdate = false;
                    })

                    // 업데이트된 세션들의 데이터 입력
                    for (var i = 0; i < data.length; i++) {
                        var dt = data[i];
                        var session = Fims.sessions[dt.SN];

                        // 세션이 존재하지 않는다면 새로운 세션을 만든다.
                        if (!session) {
                            Fims.sessions[dt.SN] = session = new GameSession();
                        }
                        session.IsUpdate = true;
                        session.IsNew = false;
                        session.data = dt;

                        if (session !== GameSession.current) {
                            session.applyData = { Name: dt.Name };
                            session.IsDirty = false;
                        }
                        session.Refresh();
                    }

                    // 업데이트되지 않은, 혹은 없어진 세션들 제거
                    $.each(Fims.sessions, function (k, v) {
                        if (v && !v.IsUpdate) {
                            v.Remove();
                            delete Fims.sessions[k];
                        }
                    })
                }
                else {
                    console.error(ajax);
                }
            })
        }, 1000);
    }

    // 각 버튼에 이벤트를 추가합니다.
    function InitEvents() {

        // 세션 추가버튼을 누르면,
        // 새로운 세션을 생성하고 필요 정보를 입력하여 표시합니다.
        $("#addSession").click(function () {
            var session = new GameSession();
            session.data = {
                SN: Math.random(),
                Name: "",
                State: 0,
                Level: 0,
                Stage: 1,
                Observer: false,
                members: []
            };
            session.Refresh(true);
        });

        // 룸이름의 textField가 바뀌면 현재 선택된 세션의 정보도 바뀌게끔 합니다.
        $("#roomName").change(function () {
            if (GameSession.current) {
                GameSession.current.applyData.Name = $(this).val();
                GameSession.current.SetDirty();
                GameSession.current.Refresh();
            }
        });

        // 빠른시작버튼
        $("#fastStartBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnApply(true);
            }
        });

        //강제 시작 버튼
        $("#forcedStartBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnForcedStart();
            }
        });

        // 적용버튼
        $("#applyBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnApply(false);
            }
        });

        // 실행,시작,종료 버튼
        $("#actionBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnAction();
            }
        });

        //중지 버튼
        $("#stopBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnStop();
            }
        });

        // 삭제, 반환버튼
        $("#removeBtn").click(function () {
            if (GameSession.current) {
                GameSession.current.OnRemove();
            }
        });
        // 삭제, 반환버튼
        $("#roomInit").click(function () {
            Fims.Send("/api/Device/DeviceInit", null, function (data, ajax) { });
            if (GameSession.current) {
                GameSession.current.OnInit();
            }
            swal("초기화 완료");
        });

    }
};

function doNotReload() {
    if ((event.ctrlKey === true && (event.keyCode === 78 || event.keyCode === 82)) //ctrl+N , ctrl+R 
        || (event.keyCode === 116)) // function F5
    {
        event.keyCode = 0;
        event.cancelBubble = true;
        event.returnValue = false;
    }
}
document.onkeydown = doNotReload;


//
function ToogleTheme(toggle = false) {
    let userMode = localStorage.userThemeMode || 'auto';
    const osMode = !!window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches ? 'dark' : 'light';
    if (toggle) {
        switch (userMode) {
            case 'auto':
                userMode = 'dark'; break;
            case 'dark':
                userMode = 'light'; break;
            default:
                userMode = 'auto';
        }
        localStorage.userThemeMode = userMode;
    }
    console.log(`current mode : ${userMode}`);
    window.__THEME_MODE = userMode === 'auto' ? osMode : userMode;
    document.getElementsByTagName('html')[0].classList[window.__THEME_MODE === 'dark' ? 'add' : 'remove']('darkmode');
}

var getTextLength = function (str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        if (escape(str.charAt(i)).length === 6) {
            len++;
        }
        len++;
    }
    return len;
}

var textDecrease = function (str) {
    var len = 0;
    let changeText = "";
    for (var i = 0; i < str.length; i++) {
        if (escape(str.charAt(i)).length == 6) {
            len++;
        }
        len++;
        if (len < 10) {
            changeText += str.charAt(i);
        }
    }
    if (len > 10)
        changeText += "..";
    return changeText;
}
