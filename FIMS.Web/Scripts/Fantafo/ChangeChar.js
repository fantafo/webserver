﻿$(window).ready(function ()
{
    $(".contentBox").each(function (i, n) {
        var elem = $(n);
        var type = elem.data("prefix");
        var container = elem.find(".container");
        container.html("");

        for (var i = Number(elem.data("min")), len = Number(elem.data("max")); i <= len; i++)
        {
            //var dom = $("<div class='selectBtn' data-type='{1}' data-id='{0}'><img src='/Content/Images/Char/{1}{0}.png' /></div>".format(i, elem.data("prefix")));
            var dom = $("<label for='{0}{1}'><img src='/Content/Images/Char/{0}{1}.png' /></label><input id='{0}{1}' type='radio' name='{0}' value='{1}'/>".format(type, i));
            container.append(dom);

            var input = dom.eq(1);
            input.attr("checked", avatarData[type] == i).change();
            input.checkboxradio({ icon: false });
            input.change(onClickButton);
        }
    });

    $(window).resize(OnResize);
    OnResize();
});

function OnResize() {
    var width = $(window).width() - 140;
    $(".contentWidth").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).width(width + margin);
    });
    var height = $(window).height() - 0;
    $(".contentHeight").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).height(height + margin);
    });
};

function onClickButton()
{
    var data = {
        head: $("[name='head']:checked").val(),
        body: $("[name='body']:checked").val()
    };
    $.ajax("/api/Member/ChangeChar",
        {
            data: data,
            method: "GET",
            async: true,
            contentType: "application/json; charset=UTF-8",
            success: function (data, state, response) {
            },
            error: function (response, state, errorCode) {
                console.log(response);
                alert(errorCode + " : " + state);
            }
        });
}