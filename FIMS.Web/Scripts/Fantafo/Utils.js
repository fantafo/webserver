﻿
/////////////////////////////////////////////////////////////////////
// String Helper
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}
// remove Children
if (!jQuery.fn.removeChildren) {
    jQuery.fn.removeChildren = function() {
        $(this).html("");
        //$(this).children().each(function (i, o) { $(o).remove(); });
    }
}

if (!Number.prototype.to00) {
    Number.prototype.to00 = function () {
        var num = Math.floor(this);
        if (num > 99) return 99;
        else if (num < 10) return "0" + num;
        else return num;
    }
}
