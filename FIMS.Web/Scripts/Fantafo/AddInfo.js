﻿$(window).load(function ()
{
    $(".inputlist input[type='radio']").checkboxradio({ icon: false });
    $(".inputlist input[type='submit']").button();
    $(".inputlist a, .popupBox a").button();

    InitializeData();

    $(window).resize(OnResize);
    OnResize();
});

function InitializeData() {
    $.each(userAttributes, function (i, n) {
        var value = n.Value;
        var elems = $("input[name='{0}'".format(n.Key));

        if (elems.length == 1) {
            elems.val(value);
        } else {
            elems.each(function (i, n) {
                if ($(n).val() == value)
                {
                    $(n).attr("checked", true).change();
                }
            });
        }
    });
}

function OnResize() {
    var width = $(window).width() - 140;
    $(".contentWidth").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).width(width + margin);
    });
    var height = $(window).height() - 0;
    $(".contentHeight").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).height(height + margin);
    });
};