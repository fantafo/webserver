﻿$(document).ready(function () {
    Send("/api/Admin/isLogin/", null, function (data, ajax) {
        if (ajax.status === 200) {
            //로그인중..
            if (data === true) {
                $("#AdminLoginBtn").text("관리자 로그아웃");
                $("#AdminLoginBtn").click(function () {
                    Send("/api/Admin/Logout/", null, function (data, ajax) { location.reload(true); });
                });
            }
            //비로그인중..
            else if (data === false) {
                $("#AdminLoginBtn").text("관리자 로그인");
                $("#AdminLoginBtn").click(function () {
                    AdminLogin();
                });
            }
        } else {
            alert("isLogin 오류 발생");
        }
    });
});

function AdminLogin() {
    var password = prompt('관리자 암호를 입력하세요',);
    var data = {};
    data.password = password;
    if (password != null) {
        Send("/api/Admin/check/", data , function (data, ajax) {
            if (ajax.status == 200) {
                alert("관리자 로그인 성공");
                $("#AdminLoginBtn").text("관리자 로그아웃");
                location.reload(true);
            } else {
                alert("관리자 로그인 실패");
                $("#AdminLoginBtn").text("관리자 로그인");
                location.reload(true);
            }
        });
    }
    
}
/*
window.onbeforeunload = function (e) {
    e = e || window.event;
    
    Send("/api/Admin/Logout", null, function (data, ajax) { });
    return ;
};
*/

function Send(url, data, callback) {
    $.ajax(url, {
        method: "GET",
        data: data,
        xhrFields: { withCredentials: true },
        contentType: "application/json; charset=UTF-8",
        success: function (data, type, ajax) { callback(data, ajax); },
        error: function (ajax) { callback(null, ajax); },
    });
}

function getCookie(cookieName) {
    var cookieArray = document.cookie.split(';');
    for (var i = 0; i < cookieArray.length; i++) {
        var cookie = cookieArray[i];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1);
        }
        cookieHalves = cookie.split('=');
        if (cookieHalves[0] === cookieName) {
            return cookieHalves[1];
        }
    }
    return "";
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

function cookiesEnabled() {
    var r = false;
    if (getCookie(name) !== null) {
        r = true;
    }
    return r;
}