﻿$(document).ready(function ()
{
    $("#signup").submit(OnSubmit);
    $("input[type='radio']").checkboxradio({ icon: false });
    $("input[type='checkbox']").checkboxradio({ icon: false });
    $("input[type='submit']").button();
    $("#SuccessBox a").button();

    // agreements
    var agreements = $("input[type='checkbox'].agreement");
    agreements.change(function(e) {
        var allcheck = true;
        agreements.each(function(i, n) {
            if (!$(n).prop("checked")) {
                allcheck = false;
            }
        });
        if (allcheck) {
            location = "/Member/Signup?id=1";
        }
    });
    $("#agreeBtn").button().click(function() {
        agreements.prop("checked", true).change();
    });
    $("[name=name]").change(function(e) {
        var nick = $("[name=nickname]");
        if (!nick.val()) nick.attr("placeholder", $(this).val());
    });
    $("[name=name]").select();

    $("#signup input").keydown(function(e) {
        var input = $(this);
        if (e.keyCode == 13) {
            $("[data-focusid='{0}']".format(Number(input.data("focusid")) + 1)).select();
            if (!input.data("lastfocus"))
                e.preventDefault();
        }
    });

    $(window).resize(OnResize);
    OnResize();
});

function OnResize() {
    var width = $(window).width() - 140;
    $(".contentWidth").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).width(width + margin);
    });
    var height = $(window).height() - 0;
    $(".contentHeight").each(function (i, e) {
        var margin = $(e).data("margin");
        if (!margin) margin = 0;
        $(e).height(height + margin);
    });
};

function OnSubmit(e)
{
    e.preventDefault();

    (function() {
        var signupForm = $("#signup");
        var serialized = signupForm.serialize();
        var data = {};

        function Check(name, localname, min, max, func) {
            var obj = signupForm.find("[name=" + name + "]");
            var val = obj.val();
            var errMsg = null;

            if (!val) val = obj.prop("placeholder");

            if (!val) {
                errMsg = "{0} 입력해주세요!".format(localname);
            } else if (val.length < min) {
                errMsg = "{0} {1}자 이상 입력해주세요!".format(localname, min);
            } else if (val.length > max) {
                errMsg = "{0} {1}자 이하로 입력해주세요!".format(localname, max)
            } else if (func) {
                errMsg = func(val);
            }

            if (errMsg) {
                ErrorMsg(errMsg);
                obj.select();
                return true;
            }

            data[name] = val;
        }

        if (Check("name", "이름을", 2, 15)) return;
        if (Check("email", "이메일을", 5, 50)) return;
        if (Check("mobile", "핸드폰 번호를", 8, 20)) return;

        if (Check("nickname", "닉네임을", 2, 15)) return;
        if (Check("password", "비밀번호", 8, 50, function(val) {
            if (val != signupForm.find("#confirm").val()) {
                return "비밀번호가 서로 일치하지 않습니다.";
            }
        })) return;
        if (Check("birth", "생일을", 0, 20)) return;
        {// 성별 체크 
            var obj = signupForm.find("[name=gender]");
            var checked = false;
            obj.each(function(i, n) { checked |= $(n).prop("checked"); });
            if (!checked) {
                ErrorMsg("성별을 선택해주세요!");
                return;
            }
            data.gender = obj.val();
        }

        $.ajax("/api/Member/Signup",
            {
                data: data,
                method: "GET",
                async: true,
                contentType: "application/json; charset=UTF-8",
                success: function(data, state, response) {
                    $("#SuccessBox").show("fade");
                },
                error: function(response, state, errorCode) {
                    ErrorMsg(response.responseJSON.msg);
                }
            });
    })();
    return false;
}

var errorIdx = -1;
function ErrorMsg(msg) {
    if (errorIdx != -1) clearTimeout(errorIdx);

    var box = $("#ErrorMsg");
    box.text(msg);
    box.show('slide', { direction: 'up' });

    setTimeout(function () {
        box.hide('slide', { direction: 'up' });
    }, 2000);

}