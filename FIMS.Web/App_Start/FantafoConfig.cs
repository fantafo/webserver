﻿using System.IO;
using System.Web;
using System.Web.Optimization;

namespace FIMS.Web
{
    public class FantafoConfig
    {
        /// <summary>
        /// 서버가 시작됐을 때 최초로 호출되는 시작점으로서 
        /// FIMS.Core를 초기화하는 용도로 사용된다.
        /// </summary>
        public static void Begin(System.Web.HttpApplication app)
        {
#if DEBUG
            //string currentPath = @"D:\Publish";
            string currentPath = app.Server.MapPath("~/setting");
#else
            //string currentPath = Directory.GetParent(app.Server.MapPath("~/cur.exe")).Parent.FullName;
            string currentPath = app.Server.MapPath("~/setting");
#endif
            Fims.Start(currentPath);
        }
        /// <summary>
        /// Fims와 웹서버가 모두 로딩된 뒤에 마지막 정보를 설정한다.
        /// </summary>
        public static void BeginEnd(System.Web.HttpApplication app)
        {
//            using (var db = Database.Open())
//            {
//                ServerModel sm = db.Get<ServerModel>((byte)1);
//                sm.Ip = IpHelper.InnerIP;
//#if DEBUG
//                sm.Port = 10844;
//#else
//                sm.Port = 80;
//#endif
//                using(var tr = db.BeginTransaction())
//                {
//                    db.Update(sm);
//                    tr.Commit();
//                }
//            }
        }

        /// <summary>
        /// 웹서버가 종료될 때 처리를 전달한다.
        /// </summary>
        public static void Close(System.Web.HttpApplication app)
        {
            Fims.Stop();
        }
    }
}
