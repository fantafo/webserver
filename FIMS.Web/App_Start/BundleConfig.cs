﻿using System.Web;
using System.Web.Optimization;

namespace FIMS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/Fantafo/CryptoJS.js"));

            bundles.Add(new ScriptBundle("~/bundles/utils").Include(
                        "~/Scripts/Fantafo/CryptoJS.js",
                        "~/Scripts/Fantafo/Utils.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/jquery-ui.structure.css",
                      "~/Content/jquery-ui.theme.css"
                      ));
        }
    }
}
