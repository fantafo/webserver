﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace FIMS.Web.Controllers
{
    public class HomeController : Controller
    {
        public static ILog log = LogManager.GetLogger("Web.HomeController");

        public static string CommandServerIp = "";

        /// <summary>
        /// 키오스크상태에서 유저들이 자신의 정보를 확인하고,
        /// 회원가입을 하기 위한 기본페이지
        /// </summary>
        public ActionResult Index()
        {
            var sess = this.GetSession();
            this.SetUserSN(null);

            ViewBag.Title = "Fantafo";
            return View();
        }

        /// <summary>
        /// 로그인 페이지
        /// </summary>
        public ActionResult Login()
        {
            using (var db = Database.Open())
            {
                var sess = this.GetSession();
                var redirect = (string)sess["redirect"];

                var user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user != null)
                {
                    return GotoRedirect();
                }
                else
                {
                    try
                    {
                        ViewBag.Title = "로그인";
                        ViewBag.Msg = (string)sess["msg"] ?? "";
                        ViewBag.Id = (string)sess["id"] ?? "";
                        return View();
                    }
                    finally
                    {
                        sess.Remove("id");
                        sess.Remove("msg");
                    }
                }
            }
            
        }

        /// <summary>
        /// 로그인 시도
        /// </summary>
        public ActionResult ProcLogin(string id, string password)
        {
            using (var db = Database.Open())
            {
                var sess = this.GetSession();
                var user = MemberDao.Get(db).GetByLoginID(id, Member.ConvertPassword(password));
                //var user = MemberDao.Get(db).GetByLoginID(id, password);
                if (user == null)
                {
                    sess["id"] = id;
                    sess["msg"] = "아이디 혹은 비밀번호가 틀렸습니다.";
                    return Redirect($"/Home/Login");
                }
                this.SetUserSN(user);
                return GotoRedirect();
            }
        }

        /// <summary>
        /// Agent Command Server 정보
        /// </summary>
        public string CommandServerInfo()
        {
            if (Fims.agentServer != null)
            {
                CommandServerIp = Fims.agentServer.IpAddress;
                return CommandServerIp;
            }
            else
            {
                CommandServerIp = "x";
                return "x";
            }
        }

        public static void SetCommandServerIp()
        {
            if (Fims.agentServer != null)
            {
                CommandServerIp = Fims.agentServer.IpAddress;
            }
            else
            {
                CommandServerIp = "x";
            }
        }


        /// <summary>
        ///Game Server 정보
        /// </summary>
        public string GameServerInfo(string name)
        {
            using (var db = Database.Open())
            {
                try
                {
                    //var game = FIMS.GameDao.Get(db).GetByName("safety");
                    var game = FIMS.GameDao.Get(db).Get(2);
                    string ip = game.ServerIP + ":" + game.ServerPort;
                    return ip;
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return "x";
                }
            }
        }

        /// <summary>
        /// FIMS Server의 로컬 IP
        /// 
        /// * 이걸 왜쓰는걸까.. 기억이 안나네요
        /// </summary>
        /// <returns></returns>
        public string GetLocalIP()
        {
            string localIP = "Not available, please check your network seetings!";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            return localIP;
        }

        /// <summary>
        /// 특정 엑션을 위한 뒤, redirect 할 페이지로 이동합니다.
        /// </summary>
        ActionResult GotoRedirect()
        {
            var sess = this.GetSession();
            var redirect = (string)sess["redirect"];
            try
            {
                if (redirect.IsEmpty())
                {
                    return Redirect("/");
                }
                else
                {
                    return Redirect(redirect);
                }
            }
            finally
            {
                sess.Remove("redirect");
            }
        }
    }
}
