﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace FIMS.Web.Controllers
{
    public class MemberController : Controller
    {
        public ActionResult Signup(int id = 0)
        {
            ViewBag.Title = "회원가입";

            switch (id)
            {
                case 0:
                    return View("Signup.Agreements");
                case 1:
                    return View("Signup.Form");
                default:
                    throw new Exception("Nop!");
            }
        }

        public ActionResult Info()
        {
            
            using (var db = Database.Open())
            {
                Member user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user == null)
                {
                    this.GetSession()["redirect"] = "/Member/Info";
                    return Redirect("/Home/Login");
                }
                else
                {
                    NHibernateUtil.Initialize(user.Coupons);

                    ViewBag.user = user;
                    ViewBag.needInfo = (user.Attributes.Count == 0);
                    ViewBag.Title = $"{user.NickName} ({user.Name})";

                    return View();
                }
            }
        }

        public ActionResult AdditionalInfo(string redirect = "/")
        {
            using (var db = Database.Open())
            {
                Member user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user == null)
                {
                    this.GetSession()["redirect"] = "/Member/AdditionalInfo";
                    return Redirect("/Home/Login");
                }

                ViewBag.Title = "추가정보";
                ViewBag.user = user;
                ViewBag.redirect = redirect;
                ViewBag.attributes = (user.Attributes.Count > 0) ?
                    new JavaScriptSerializer().Serialize(user.Attributes) : "[]";
                if (ViewBag.IsFirst == null)
                    ViewBag.IsFirst = false;

                return View("AdditionalInfo");
            }
        }
        public ActionResult ProcAdditionalInfo()
        {
            using (var db = Database.Open())
            {
                Member user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user == null)
                    return Redirect("/Home/Login");

                ViewBag.IsFirst = (user.Attributes.Count == 0);
                using (var trans = db.BeginTransaction())
                {
                    var keys = Request.Form.Keys;
                    for (int i = 0; i < keys.Count; i++)
                    {
                        var key = keys[i];
                        if (user.Attributes.ContainsKey(key))
                        {
                            user.Attributes[key] = Request.Form[key];
                        }
                        else
                        {
                            user.Attributes.Add(key, Request.Form[key]);
                        }
                    }
                    if(ViewBag.IsFirst)
                    {
                        if (user.Coupons == null)
                            user.Coupons = new List<Coupon>();
                        user.Coupons.Add(new Coupon
                        {
                            Name = "정보입력 쿠폰",
                            Discount = 1000,
                            RegistDate = DateTime.Now
                        });
                    }
                    db.Update(user);
                    trans.Commit();
                }
            }
            return AdditionalInfo();
        }
        public ActionResult ChangeCharacter()
        {
            using (var db = Database.Open())
            {
                Member user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user == null)
                    return Redirect("/Home/Login");

                ViewBag.Title = "캐릭터 선택";
                ViewBag.user = user;
                return View();
            }
        }
        
    }
}
