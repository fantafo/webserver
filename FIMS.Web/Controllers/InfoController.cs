﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FIMS.Web.Controllers
{
    public class InfoController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
        public ActionResult InfoLogin()
        {
            ViewBag.Title = "정보조회";
            return View();
        }
        public ActionResult Info(string id, string pw)
        {
            ViewBag.Title = "정보조회";
            return View();
        }

        public string CommandServerInfo()
        {
            if(Fims.agentServer != null)
            {
                return Fims.agentServer.IsRun ?
                    "192.168.1.2:" + Fims.agentServer.Port :
                    "x";
            }
            return "x";
        }
    }
}
