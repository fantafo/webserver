﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FIMS.Web.Controllers
{
    public class DeskController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Desk Manager";
            return View();
        }

        public ActionResult ServerCheck()
        {
            using (var db = Database.Open())
            {
                ViewBag.Title = "Server Check";
                ViewBag.Games = GameDao.Get(db).ToList();
                return View();
            }
        }

        public ActionResult UpdatePage()
        {
                ViewBag.Title = "Apps 업데이트";

                return View();
            
        }

        public ActionResult UpdateFile()
        {
            ViewBag.Title = "업데이트 파일 목록";
            return View();
        }

        public ActionResult UpdateDevice()
        {
            ViewBag.Title = "업데이트 기기 목록";
            return View();
        }
    }
}