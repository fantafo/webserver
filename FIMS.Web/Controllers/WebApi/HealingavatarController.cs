﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FIMS.Web.Controllers.WebApi
{
    public class HealingavatarController : ApiController
    {
        class Userinfo
        {
            public string DeviceIndex;
            public bool Sex;    //0: 남자, 1: 여자
            public int Age;
            public int isSmoking;  //0: 흡연경험 없음, 1: 예전에 흡연한 적이 있지만 현재는 아님, 2: 흡연중
        }

        [HttpPost]
        public HttpResponseMessage noSmokingStart([FromBody]JObject m)
        {
            // I am just returning the posted model as it is. 
            // You may do other stuff and return different response.
            // Ex : missileService.LaunchMissile(m);
            return Request.CreateResponse(HttpStatusCode.OK);
        }


    }
}
