﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using System.Net.Http.Headers;

namespace FIMS.Web.Controllers.WebApi
{

    public class AdminController : ApiController
    {

        //관리자 계정 확인
        [HttpGet]
        public HttpResponseMessage Check(string password)
        {
            using (var db = Database.Open())
            {
                Admin admin = AdminDao.Get(db).FindPassword(password);
                if (admin == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                else
                {
                    var resp = new HttpResponseMessage();
                    var cookie = new CookieHeaderValue("admin-login", "true");
                    cookie.Expires = DateTimeOffset.Now.AddDays(1);
                    cookie.Domain = Request.RequestUri.Host;
                    cookie.Path = "/";
                    cookie.HttpOnly = true;
                    cookie.MaxAge = TimeSpan.FromMinutes(60);
                    resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
                    return resp;
                }
                
            }
        }
        
        [HttpGet]
        public HttpResponseMessage Logout()
        {
            var resp = new HttpResponseMessage();
            var cookie = new CookieHeaderValue("admin-login", "");
            cookie.Expires = DateTimeOffset.Now.AddDays(1);
            cookie.Domain = Request.RequestUri.Host;
            cookie.Path = "/";
            cookie.HttpOnly = true;
            cookie.MaxAge = TimeSpan.Zero;
            resp.Headers.AddCookies(new CookieHeaderValue[] { cookie });
            return resp;
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage isLogin()
        {
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            //해더에 로그인 쿠키가 있는지 확인
            if (cookie == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, false);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetLicense(string data)
        {
            string udid = data;
            string license = data;
            return Request.CreateResponse(HttpStatusCode.OK, license);
        }
    }
}
