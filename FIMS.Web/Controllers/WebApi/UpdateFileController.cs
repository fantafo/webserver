﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace FIMS.Web.Controllers.WebApi
{
    public class UpdateFileController : ApiController
    {
        static string downloadPath = HttpContext.Current.Server.MapPath("~/file/");
        static string virtualPath = "/file/";

        struct DirectoryItem
        {
            public Uri BaseUri;

            public string AbsolutePath
            {
                get
                {
                    return string.Format("{0}/{1}", BaseUri, Name);
                }
            }

            public DateTime DateCreated;
            public bool IsDirectory;
            public string Name;
            public List<DirectoryItem> Items;
        }

        static ILog log = LogManager.GetLogger("Web." + typeof(FileUpdateController).Name);

        static string ftpAdminID = "ftpfantafoadmin";
        static string ftpAdminPassword = "fantafo";

        /// <summary>
        /// 전체 파일 목록을 불러온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage FileList()
        {
            UpdateFileList();
            using (var db = Database.Open())
            {
                var list = UpdateFileInfoDao.Get(db).GetList();
                var result = new { data = new List<FileInfo>(list) };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        //파일 갱신
        [HttpGet]
        public void UpdateFileList()
        {
            //ftp 서버 확인
            string ftpip = "";
            if (FimsSetting.isftpLocalServer)
            {
                ftpip = "ftp://" + IpHelper.InnerIP;
            }
            else
            {
                //TODO.. 로컬ftp 서버가 아닐경우..
                return;
            }

            //ftp 서버 파일 읽어오기
            var list = GetDirectoryInformation(ftpip, ftpAdminID, ftpAdminPassword);

            //새로 읽어온 파일 목록
            var newFileList = GetDirectoryFileInfo(list);

            //기존 파일
            var oldFileLIst = new List<FileInfo>();
            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                oldFileLIst = UpdateFileInfoDao.Get(db).GetList();
            }

            //기본 파일이 존재하는지 검사
            for(int i=0; i< oldFileLIst.Count; ++i)
            {
                int index = newFileList.FindIndex(x => x.FileName == oldFileLIst[i].FileName);
                if(index == -1)
                {
                    using (var db = Database.Open())
                    using (var trans = db.BeginTransaction())
                    {
                        try
                        {
                            db.Delete(oldFileLIst[i]);
                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            log.Debug(e);
                            trans.Rollback();
                        }
                    }
                }
                else
                {
                    if(oldFileLIst[i].Path != newFileList[index].Path)
                    {
                        oldFileLIst[i].Path = newFileList[index].Path;
                        using (var db = Database.Open())
                        using (var trans = db.BeginTransaction())
                        {
                            try
                            {
                                db.SaveOrUpdate(oldFileLIst[i]);
                                trans.Commit();
                            }
                            catch (Exception e)
                            {
                                log.Debug(e);
                                trans.Rollback();
                            }
                        }
                    }
                }
            }

            //새로운 파일 검사
            for (int i = 0; i < newFileList.Count; ++i)
            {
                int index = oldFileLIst.FindIndex(x => x.FileName == newFileList[i].FileName);
                if (index == -1)
                {
                    using (var db = Database.Open())
                    using (var trans = db.BeginTransaction())
                    {
                        try
                        {
                            FileInfo fileinfo = new FileInfo();
                            fileinfo.FileName = newFileList[i].FileName;
                            fileinfo.UpdateDate = DateTime.Now;
                            fileinfo.Size = newFileList[i].Size;
                            fileinfo.Path = newFileList[i].Path;
                            fileinfo.InstallPath = newFileList[i].InstallPath;
                            fileinfo.UpdateCheck = false;
                            fileinfo.Category = LargeCategory.None;
                            db.Save(fileinfo);
                            trans.Commit();
                        }
                        catch (Exception e)
                        {
                            log.Debug(e);
                            trans.Rollback();
                        }
                    }
                }
            }

        }

        //ftp 서버 디렉토리 읽어오기
        //https://social.msdn.microsoft.com/Forums/vstudio/en-US/7758e7a8-e063-4eec-a6c1-c76a59d02852/listing-all-files-from-ftp-server-using-c?forum=csharpgeneral
        // FTP MLSD 명령어 지원x
        private List<DirectoryItem> GetDirectoryInformation(string address, string username, string password)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(address);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = new NetworkCredential(username, password);
            // POTENTIAL ERROR: Was = true. But to keep active changed to = false for FileZilla Server.
            request.UsePassive = false;
            request.UseBinary = true;
            request.KeepAlive = false;

            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            foreach (string line in list)
            {
                try
                {
                    string data = line;

                    //filezilla server format
                    data = data.Remove(0, 20);

                    //size
                    long size = data.Substring(0, 15).Replace(" ", "").ToLong();
                    data = data.Remove(0, 16);

                    //date
                    string date = data.Substring(0, 12);
                    DateTime dateTime = DateTime.MinValue;
                    //dateTime= DateTime.ParseExact(date, "MMM dd HH:mm", CultureInfo.InvariantCulture);
                    data = data.Remove(0, 13);

                    //name
                    string name = data;


                    // Create directory info
                    DirectoryItem item = new DirectoryItem();
                    item.BaseUri = new Uri(address);
                    //item.DateCreated = dateTime;
                    item.IsDirectory = size == 0 ? true : false;
                    item.Name = name;
                    item.DateCreated = SettingDirectoryItemDatetime(item.AbsolutePath, username, password);

                    //Debug.WriteLine(item.AbsolutePath);
                    item.Items = item.IsDirectory ? GetDirectoryInformation(item.AbsolutePath, username, password) : null;

                    returnValue.Add(item);
                }catch(Exception e)
                {
                    log.Debug("UpdateFIleController GetDirectoryInformation Error : " + e);
                    continue;
                }
            }

            return returnValue;
        }

        //전달받은 DirectoryItem의 날짜를 설정한다.
        private DateTime SettingDirectoryItemDatetime(string AbsolutePath, string username, string password)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(AbsolutePath);
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            request.Credentials = new NetworkCredential(ftpAdminID, ftpAdminPassword);
            // POTENTIAL ERROR: Was = true. But to keep active changed to = false for FileZilla Server.
            request.Proxy = null;

            DateTime date = DateTime.MaxValue;
            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                date = response.LastModified;
            }
            return date;
        }


        private List<FileInfo> GetDirectoryFileInfo(List<DirectoryItem> list)
        {
            List<FileInfo> FileInfoList = new List<FileInfo>();
            HashSet<string> nameHashSet = new HashSet<string>();

            for (int i = 0; i < list.Count; ++i)
            {
                try
                {
                    if (list[i].IsDirectory)
                    {
                        var addList = GetDirectoryFileInfo(list[i].Items);
                        FileInfoList.AddRange(addList);
                    }
                    else
                    {
                        //특정 확장자만 업로드
                        string ext = System.IO.Path.GetExtension(list[i].Name);
                        if (!FimsSetting.UpdateExtension.Contains(ext))
                        {
                            continue;
                        }

                        //100 넘어가면 저장 안함
                        string changeName = Regex.Replace(list[i].Name.Replace(ext, ""), @"[^0-9a-zA-Z가-힣.]", "_");
                        for (int j = 0; j < 100; ++j)
                        {
                            string saveName = changeName;
                            if (j != 0)
                            {
                                saveName += ("_" + j);
                            }

                            if (nameHashSet.Add(saveName))
                            {
                                changeName = saveName + ext;
                                break;
                            }
                        }

                        if (changeName.Substring(changeName.Length - ext.Length) != ext)
                        {
                            continue;
                        }

                        FileInfo fileinfo = new FileInfo();
                        fileinfo.FileName = list[i].Name;
                        fileinfo.UpdateDate = list[i].DateCreated;
                        fileinfo.Path = list[i].AbsolutePath;
                        fileinfo.UpdateCheck = false;
                        fileinfo.RemoveCheck = false;

                        //file installpath
                        fileinfo.InstallPath = FimsSetting.InstallPath + ext.Replace(".", "") + "/";

                        //변경된 이름 적용
                        ChangeFileName(fileinfo, changeName);

                        //file size
                        FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri(fileinfo.Path));
                        request.Proxy = null;
                        request.Credentials = new NetworkCredential(ftpAdminID, ftpAdminPassword);
                        request.Method = WebRequestMethods.Ftp.GetFileSize;

                        FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                        fileinfo.Size = response.ContentLength;
                        response.Close();
                        FileInfoList.Add(fileinfo);
                    }
                }catch(Exception e)
                {
                    continue;
                }
            }
            return FileInfoList;
        }

        private void ChangeFileName(FileInfo fileinfo, string changeName)
        {
            if (fileinfo == null || changeName == null || fileinfo.FileName == changeName)
                return;

            //파일명 변경
            FtpWebRequest reqFTP;
            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(fileinfo.Path));
            //이름변경 명령 실행
            reqFTP.Method = WebRequestMethods.Ftp.Rename;
            reqFTP.RenameTo = changeName;
            reqFTP.UseBinary = true;
            reqFTP.Credentials = new NetworkCredential(ftpAdminID, ftpAdminPassword);
            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
            Stream ftpStream = response.GetResponseStream();

            ftpStream.Close();
            response.Close();

            fileinfo.Path = fileinfo.Path.Replace(fileinfo.FileName, "") + changeName;
            fileinfo.FileName = changeName;
        }

        [HttpPost]
        public HttpResponseMessage Editor([FromBody]JObject request)
        {
            //현재 액션은 edit만 있음
            var action = request.GetValue("action").ToString();
            var data = request.GetValue("data");
            //다중 수정,삭제 가능하도록 리스트로 설정.. 하지만 선택을 하나만 가능하도록 설정해서.. 
            IList<FileInfo> changedobjects = new List<FileInfo> { };

            foreach (JProperty x in data)
            {
                //initialize empty object of class
                FileInfo changedobject = (FileInfo)Activator.CreateInstance(typeof(FileInfo));

                //만약 키값이 string
                changedobject.GetType().GetProperty("FileName").SetValue(changedobject, x.Name.ToString());
                //만약 키값이 int면
                //changedobject.GetType().GetProperty("SN").SetValue(changedobject, Int32.Parse(x.Name.ToString()));

                //loop through each of JSON object sub properties, change the property name from camel case to pascal case and map to object's properties
                foreach (JProperty y in x.Value)
                {
                    string propertyname = char.ToUpper(y.Name[0]) + y.Name.Substring(1);

                    if (changedobject.GetType().GetProperty(propertyname) == null)
                        continue;

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "Boolean")
                    {
                        try
                        {
                            //js에서 UpdateCheck가 false일 경우 "" 로 넘어옴, true 일 경우 "true"
                            bool check = false;
                            if (!y.Value.ToString().IsEmpty() && y.Value.ToString() == "true")
                                check = true;

                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, check);
                        }
                        catch (Exception e)
                        {
                            log.Debug($"{e}");
                        }
                    }

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                    {
                        try
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value.ToString());
                        }
                        catch (Exception e)
                        {
                            log.Debug($"{e}");
                        }
                    }

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "LargeCategory")
                    {
                        try
                        {
                            FIMS.LargeCategory value = (FIMS.LargeCategory)Enum.Parse(typeof(FIMS.LargeCategory), y.Value.ToString());
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, value);
                        }
                        catch (Exception e)
                        {
                            log.Debug($"{e}");
                        }
                    }

                }
                //add object to changed objects List
                changedobjects.Add(changedobject);
            }

            if (action == "edit")
            {
                //edit 액션에는 FileName, UpdateCheck, InstallPath, RemoveCheck 정보 있음
                using (var db = Database.Open())
                using(var trans = db.BeginTransaction())
                {
                    try
                    {
                        foreach (FileInfo newinfo in changedobjects)
                        {
                            var updatefileinfo = UpdateFileInfoDao.Get(db).GetByName(newinfo.FileName);
                            updatefileinfo.UpdateCheck = newinfo.UpdateCheck;
                            updatefileinfo.Category = newinfo.Category;
                            db.SaveOrUpdate(updatefileinfo);
                        }
                        trans.Commit();
                    }
                    catch(Exception e)
                    {
                        log.Debug(e);
                        trans.Rollback();
                    }
                    
                }

            }
            else if (action == "remove")
            {
                using (var db = Database.Open())
                using (var trans = db.BeginTransaction())
                {
                    try
                    {
                        for (int i = 0; i < changedobjects.Count; ++i)
                        {
                            var fileinfo = UpdateFileInfoDao.Get(db).GetByName(changedobjects[i].FileName);
                            db.Delete(fileinfo);
                            //ftp 파일 삭제
                            FtpWebRequest Ftprequest = (FtpWebRequest)FtpWebRequest.Create(changedobjects[i].Path);
                            Ftprequest.Credentials = new NetworkCredential(ftpAdminID, ftpAdminPassword);
                            Ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                            Ftprequest.GetResponse();
                        }
                        trans.Commit();
                    }catch(Exception e)
                    {
                        log.Debug(e);
                        trans.Rollback();
                    }
                }
            }
            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                var list = UpdateFileInfoDao.Get(db).GetList();
                var result = new { data = new List<FileInfo>(list) };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }

        [HttpPost]
        public IHttpActionResult RemoveFile([FromBody]JObject request)
        {
            string fileName = request.GetValue("FileName").ToString() != null ? request.GetValue("FileName").ToString() : "";
            string path = request.GetValue("Path").ToString() != null ? request.GetValue("Path").ToString() : "";

            if (fileName.IsEmpty())
                return Ok("fileName empty");

            if (path.IsEmpty())
                return Ok("path empty");

            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                try
                {
                    var fileinfo = UpdateFileInfoDao.Get(db).GetByName(fileName);
                    db.Delete(fileinfo);
                    trans.Commit();
                    
                    //ftp 파일 삭제
                    FtpWebRequest Ftprequest = (FtpWebRequest)FtpWebRequest.Create(path);
                    Ftprequest.Credentials = new NetworkCredential(ftpAdminID, ftpAdminPassword);
                    Ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                    Ftprequest.GetResponse();
                    return Ok("");
                }
                catch(Exception e)
                {
                    trans.Rollback();
                    return BadRequest("delete failed");
                }
            }

        }
    }
}
