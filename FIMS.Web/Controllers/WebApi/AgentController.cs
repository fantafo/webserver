﻿using FIMS.Manager;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

using System.Web;

namespace FIMS.Web.Controllers.WebApi
{
    enum WIFI { local = 1, global }

    /// <summary>
    /// 매니저에서 에이전트와 고나련된 설정을 진행합니다.
    /// </summary>
    public class AgentController : ApiController
    {
        static bool lockscrean = false;

        static ILog log = LogManager.GetLogger("Web."+typeof(DataController).Name);

        public class SettingFile
        {
            public string name { get; set; }
            public bool delete { get; set; }
            public string details { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage DownloadAgentSettings()
        {
            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach(var client in clients)
            {
                try
                {
                    client.Command("downloadSettings");
                }
                catch { }
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{clients.Count}개의 단말기에게 설정 다운로드 신호를 보냈습니다." });
        }
        

        [HttpGet]
        public HttpResponseMessage GetToggleAgentValue()
        {
            var list = DeviceDao.Get.ToList();
            int num = 0;
            foreach (var device in list)
            {
                if (device.Enabled)
                    num++;
            }
            if( num > 0 )
            {
                return Request.CreateResponse(HttpStatusCode.OK,true);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK,false);
            }
        }

        [HttpGet]
        public HttpResponseMessage ToggleAgents(bool toggle)
        {
            //using (var db = Database.Open())
            {
                var list = DeviceDao.Get.ToList();
                //int num = 0;
                
                //foreach(var device in list)
                //{
                //    if (device.Enabled)
                //        num++;
                //    else
                //        num--;
                //}

                //bool enabled = num >=0 ? false : true;

                //using (var trans = db.BeginTransaction())
                {
                    foreach (var device in list)
                    {
                        device.Enabled = toggle;
                        if (!toggle && Fims.agentServer != null)
                        {
                            var cmd = Fims.agentServer.GetClient(device.SN);
                            if (cmd != null)
                            {
                                cmd.Disconnect();
                            }
                        }
                        //db.Update(device);
                    }
                    //trans.Commit();

                    //Fims.agentServer.ClearAgents();
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        //디바이스가 agent on/off인지 확인하기 위한 url
        //on 일시, sock을 생성하라고 string값을 보내줌.
        //off일시, sock을 생성하지말라고 string값을 보내줌.
        [HttpGet]
        public HttpResponseMessage AgentConnect([FromUri]string uuid, string name, string model)
        {
            if (name == "" || name == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, $"agentconnect connect={false} ipaddress={"x"}");
            }

            using (var db = Database.Open())
            {
                var dev = DeviceDao.Get.FindByUUID(uuid);

                //등록되지 않은 uuid는 해당기기를 DB에 추가할것.
                if (dev == null)
                {
                    dev = new Device();
                    dev.UUID = uuid;
                    dev.Name = name;
                    dev.Model = model;
                    dev.IPAddress = "";
                    dev.Cont = false;
                    dev.Live = false;
                    using (var trans = db.BeginTransaction())
                    {
                        try
                        {
                            db.SaveOrUpdate(dev);
                            trans.Commit();
                        }
                        catch
                        {
                            log.Debug("Device 등록 실패");
                        }
                    }

                    //디비에서 sn값을 자동으로 설정, device에 추가하기 위해 정보를 가져온다.
                    Device db_device = DeviceDao.Get.FindByUUIDInDB(uuid);
                    DeviceDao.Get.Add(db_device);
                }
                //이름, 모델명이 변경 되었을 경우.
                else
                {
                    if (dev.Name != name || dev.Model != model) {
                        using (var trans = db.BeginTransaction())
                        {
                            try
                            {
                                dev.Name = name;
                                dev.Model = model;
                                db.Update(dev);
                                trans.Commit();
                            }
                            catch
                            {
                                log.Debug("Device 업데이트 실패");
                            }
                        }
                    }
                    DeviceDao.Get.ChangeName(dev, name);
                }
                FIMS.Web.Controllers.HomeController.SetCommandServerIp();
                string ipaddress = FIMS.Web.Controllers.HomeController.CommandServerIp;
                return Request.CreateResponse(HttpStatusCode.OK, $"agentconnect connect={dev.Enabled} ipaddress={ipaddress}");
            }
        }


        //wifi.list , agent.url 파일 내용을 변경하기 위한 코드
        //만약 그외 파일을 추가하고 싶다면 agent앱을 수정해야함.
        [HttpGet]
        public HttpResponseMessage ChangeSettingFile([FromUri]SettingFile file)
        {
            string msg = $"SettingFile name={file.name} delete={file.delete} details={file.details}";

            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach (var client in clients)
            {
                try
                {
                    client.Command(msg);
                }
                catch { }
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{clients.Count}개의 단말기에게 wifi설정을 보냈습니다." + msg});
        }

        //agent를 재시작하기 위한 함수
        [HttpGet]
        public HttpResponseMessage AgentReStart()
        {
            string msg = $"ReStart";
            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach (var client in clients)
            {
                try
                {
                    client.Command(msg);
                }
                catch { }
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{clients.Count}개의 단말기에게 재시작 신호를 보냈습니다."});
        }
        
        /*wifi를 agent에게 변경하라고 신호를 보내다.
         * 로컬과 클라우드를 동시에 사용시 사용
         * 충남대 한정으로 사용할것으로 예상하고 제작
         */
        [HttpGet]
        public HttpResponseMessage wifiSetting(string name, string password = "", string id = "")
        {
            //wifi name에 공백문자가 들어갈 경우,, agent에서 @@@을 공백으로 변경
            string msg = $"wifiSetting name={name.Replace(" ", "@@@")} password={password} id={id}";

            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach (var client in clients)
            {
                try
                {
                    client.Command(msg);
                }
                catch { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{originClients.Count}개의 기기에 wifi 변경 신호를 보냈습니다." });
        }

        [HttpGet]
        public HttpResponseMessage displayLock()
        {
            //using (var db = Database.Open())
            {
                var list = DeviceDao.Get.ToList();
                int num = 0;

                foreach (var device in list)
                {
                    if (device.Enabled)
                        num++;
                    else
                        num--;
                }

                bool enabled = num >= 0 ? false : true;

                //using (var trans = db.BeginTransaction())
                {
                    foreach (var device in list)
                    {
                        device.Enabled = enabled;
                        if (!enabled && Fims.agentServer != null)
                        {
                            var cmd = Fims.agentServer.GetClient(device.SN);
                            if (cmd != null)
                            {
                                cmd.Disconnect();
                            }
                        }
                        //db.Update(device);
                    }
                    //trans.Commit();

                    //Fims.agentServer.ClearAgents();
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }


        //잠금 화면 값 가져오기
        [HttpGet]
        public HttpResponseMessage GetLockScreenValue()
        {
            return Request.CreateResponse(HttpStatusCode.OK, SettingDao.BlackLockScreenApp);
        }

        //BlackLockScreenApp을 실행하기 위해
        [HttpGet]
        public HttpResponseMessage RunBlackLockScreenApp()
        {
            SettingDao.BlackLockScreenApp = true;

            string msg = $"RunBlackLockScreenApp";

            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach (var client in clients)
            {
                try
                {
                    client.Command(msg);
                }
                catch { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{originClients.Count}개의 기기에 RunBlackLockScreenApp 메세지를 보냈습니다." });
        }

        //BlackLockScreenApp을 종료하기 위해
        [HttpGet]
        public HttpResponseMessage StopBlackLockScreenApp()
        {
            SettingDao.BlackLockScreenApp = false;

            string msg = $"StopBlackLockScreenApp";

            List<AgentClient> clients = new List<AgentClient>();
            var originClients = Fims.agentServer.clients;
            lock (originClients)
            {
                foreach (var clientPair in originClients)
                {
                    clients.Add(clientPair.Value);
                }
            }
            foreach (var client in clients)
            {
                try
                {
                    client.Command(msg);
                }
                catch { }
            }

            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"{originClients.Count}개의 기기에 RunBlackLockScreenApp 메세지를 보냈습니다." });
        }

    }
}
