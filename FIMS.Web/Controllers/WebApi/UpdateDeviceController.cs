﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FIMS.Web.Controllers.WebApi
{
    public class UpdateDeviceController : ApiController
    {
        static ILog log = LogManager.GetLogger("Web." + typeof(UpdateDeviceController).Name);

        //업데이트 가능 기기를 목록을알려준다.
        [HttpGet]
        public HttpResponseMessage UpdateableDevice()
        {
            //하나의 리스트에 다수의 접근이 가능함.. 이후 변경
            {
                //다운로드 디바이스 리스트에 있지만 연결이 끊어진경우
                var updateDeviceList = UpdateDeviceDao.Get.GetList();
                for (int i = 0; i < updateDeviceList.Count; ++i)
                {
                    Device dev = DeviceDao.Get.DeviceGet(updateDeviceList[i].SN);
                    if(dev.IsConnected == false)
                    {
                        UpdateDeviceDao.Get.Remove(dev.SN);
                    }
                }

                IList<Device> deviceList = DeviceDao.Get.FindUpdateableDevice();
                if (deviceList != null && deviceList.Count > 0)
                {
                    for (int i = 0; i < deviceList.Count; i++)
                    {
                        //다운로드 디바이스 리스트에 없지만 연결이 된경우
                        if (UpdateDeviceDao.Get.FindBySN(deviceList[i].SN) == null)
                        {
                            UpdateDevice dev = new UpdateDevice();
                            dev.SN = deviceList[i].SN;
                            dev.name = deviceList[i].Name;
                            dev.updateCheck = false;
                            dev.storge = -1;
                            UpdateDeviceDao.Get.Add(dev);
                        }
                    }
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        //테이블 정보 가져오기
        [HttpGet]
        public HttpResponseMessage UpdateDeviceList()
        {
            UpdateableDevice();
            var list = UpdateDeviceDao.Get.GetList();
            List<Object> result = new List<object>();

            for(int i=0; i<list.Count; ++i)
            {
                result.Add(list[i].ToSerializeObject());
            }            
            return Request.CreateResponse(HttpStatusCode.OK, new { data = new List<Object>(result) });
        }

        [HttpPost]
        public HttpResponseMessage Editor([FromBody]JObject request)
        {
            //현재 액션은 edit만 있음
            var action = request.GetValue("action").ToString();
            var data = request.GetValue("data");
            //다중 수정,삭제 가능하도록 리스트로 설정.. 하지만 선택을 하나만 가능하도록 설정해서.. 
            List<UpdateDevice> changedobjects = new List<UpdateDevice> { };
            foreach (JProperty x in data)
            {
                //initialize empty object of class
                UpdateDevice changedobject = (UpdateDevice)Activator.CreateInstance(typeof(UpdateDevice));

                //만약 키값이 string
                //changedobject.GetType().GetProperty("FileName").SetValue(changedobject, x.Name.ToString());
                //만약 키값이 int면
                changedobject.GetType().GetProperty("SN").SetValue(changedobject, Int32.Parse(x.Name.ToString()));

                //loop through each of JSON object sub properties, change the property name from camel case to pascal case and map to object's properties
                foreach (JProperty y in x.Value)
                {
                    string propertyname = y.Name;

                    if (changedobject.GetType().GetProperty(propertyname) == null)
                        continue;

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "Boolean")
                    {
                        try
                        {
                            //js에서 UpdateCheck가 false일 경우 "" 로 넘어옴, true 일 경우 "true"
                            bool check = false;
                            if (!y.Value.ToString().IsEmpty() && y.Value.ToString() == "true")
                                check = true;

                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, check);
                        }
                        catch (Exception e)
                        {
                            //log.Debug($"{e}");
                        }
                    }

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                    {
                        try
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value.ToString());
                        }
                        catch (Exception e)
                        {
                            //log.Debug($"{e}");
                        }
                    }

                }
                //add object to changed objects List
                changedobjects.Add(changedobject);
            }

            if (action == "edit")
            {

                foreach (UpdateDevice dev in changedobjects)
                {
                    UpdateDeviceDao.Get.UpdateCheckChange(dev.SN, dev.updateCheck);
                }
            }

            var list = UpdateDeviceDao.Get.GetList();
            List<Object> result = new List<object>();

            for (int i = 0; i < list.Count; ++i)
            {
                result.Add(list[i].ToSerializeObject());
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { data = new List<Object>(result) });
            
        }


        //업데이트 기기 updatecheck 항목 변경
        [HttpPost]
        public HttpResponseMessage SelectAllDevice([FromBody]bool check)
        {
            var list = UpdateDeviceDao.Get.GetList();
            for(int i=0; i< list.Count; ++i)
            {
                UpdateDeviceDao.Get.UpdateCheckChange(list[i].SN, check);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public void StartUpdate()
        {
            var list = UpdateDeviceDao.Get.GetList();
            //item1 파일경로 item2 파일 사이즈 item3 설치경로 - 설치할 파일
            List<Tuple<string, long, string>> updatefileList = new List<Tuple<string, long, string>>();
            //삭제할 파일 item1 이름 item2 설치경로 
            List<Tuple<string, string>> removefileList = new List<Tuple<string, string>>();

            //var FileInfoList = FileInfoDao.Get.GetList();

            List<FileInfo> FileInfoList;
            using (var db = Database.Open())
            {
                FileInfoList = UpdateFileInfoDao.Get(db).GetList();
            }

            if (FileInfoList.Count == 0)
                return;
            
            foreach (FileInfo file in FileInfoList)
            {
                if (file.UpdateCheck)
                    updatefileList.Add(new Tuple<string, long, string>(file.Path, file.Size, file.InstallPath));
                if (file.RemoveCheck)
                    removefileList.Add(new Tuple<string, string>(file.FileName, file.InstallPath));
            }


            //보여줄 데이터 초기화
            for (int i = 0; i < list.Count; ++i)
            {
                if (list[i].updateCheck == false)
                    continue;

                list[i].completed = false;
                list[i].error = 0;
                list[i].isDownloadStart = false;
                list[i].downloadCount = 0;
                list[i].totalDownloadCount = 0;
                list[i].detail.Clear();


                //설치할 데이터 추가
                for (int j = 0; j < updatefileList.Count; ++j)
                {
                    list[i].totalDownloadCount++;
                    UpdateFileDetailInfo info = new UpdateFileDetailInfo();
                    info.name = (updatefileList[j].Item1).Substring(updatefileList[j].Item1.LastIndexOf('/') + 1);
                    info.downloadsize = 0;
                    info.totalsize = updatefileList[j].Item2;
                    info.error = 0;
                    info.success = false;
                    info.isDownloadStart = false;
                    list[i].detail.Add(info);
                }
                
                DeviceDao.Get.DeviceGet(list[i].SN).SendFile(updatefileList);

            }
        }

        [HttpGet]
        public void StopUpdate()
        {
            var list = UpdateDeviceDao.Get.GetList();
            for(int i=0; i<list.Count; ++i)
            {
                DeviceDao.Get.DeviceGet(list[i].SN).StopUpdate();
            }
            return;
        }

        //연결된 기기가 가진 파일 정보를 요청한다.
        [HttpGet]
        public void GetDeviceDetails()
        {
            var devList = DeviceDao.Get.FindUpdateableDevice();
            for(int i = 0; i < devList.Count; ++i)
            {
                devList[i].GetDeviceDetails();
            }
        }

        [HttpPost]
        public void DeleteFile([FromBody]JObject request)
        {
            //현재 액션은 edit만 있음
            string fileName = request.GetValue("childName").ToString();
            int sn = Int32.Parse(request.GetValue("sn").ToString());
            
            if (fileName == null)
            {
                log.Debug($"fileName null");
                return;
            }

            //삭제할 파일 item1 이름 item2 설치경로 
            List<Tuple<string, string>> removefileList = new List<Tuple<string, string>>();

            var fileInfo = FileInfoDao.Get.GetByName(fileName);
            if (fileInfo == null)
            {
                removefileList.Add(new Tuple<string, string>(fileName, ""));
            }
            else
            {
                removefileList.Add(new Tuple<string, string>(fileInfo.FileName, fileInfo.InstallPath));
            }
            if (sn == -1)
            {
                var devList = DeviceDao.Get.FindUpdateableDevice();
                for (int i = 0; i < devList.Count; ++i)
                {
                    devList[i].DeleteFile(removefileList);
                }
            }
            else if(sn >= 0)
            {
                var dev = DeviceDao.Get.DeviceGet(sn);
                if (dev.IsConnected)
                {
                    dev.DeleteFile(removefileList);
                }
            }
        }

        [HttpPost]
        public void DeleteFile2([FromBody]JObject request)
        {
            //현재 액션은 edit만 있음
            //string data = request.GetValue("fileName").ToString();
            JArray data = (JArray)request.GetValue("fileName");
            int sn = Int32.Parse(request.GetValue("sn").ToString());

            if (data.Count < 1)
            {
                log.Debug($"fileName null");
                return;
            }

            //삭제할 파일 item1 이름 item2 설치경로 
            List<Tuple<string, string>> removefileList = new List<Tuple<string, string>>();
            List<FileInfo> list;
            using (var db = Database.Open())
            {
                list = UpdateFileInfoDao.Get(db).GetList();
            }
            using (var db = Database.Open())
            {
                for (int i = 0; i < data.Count; ++i)
                {
                    string fileName = data[i].ToString();
                    var fileInfo = UpdateFileInfoDao.Get(db).GetByName(fileName);
                    if (fileInfo == null)
                    {
                        removefileList.Add(new Tuple<string, string>(fileName, ""));
                    }
                    else
                    {
                        removefileList.Add(new Tuple<string, string>(fileInfo.FileName, fileInfo.InstallPath));
                    }
                    if (sn == -1)
                    {
                        var devList = DeviceDao.Get.FindUpdateableDevice();
                        for (int j = 0; j < devList.Count; ++j)
                        {
                            devList[j].DeleteFile(removefileList);
                        }
                    }
                    else if (sn >= 0)
                    {
                        var dev = DeviceDao.Get.DeviceGet(sn);
                        if (dev.IsConnected)
                        {
                            dev.DeleteFile(removefileList);
                        }
                    }
                }
            }
        }

        [HttpPost]
        public void DeleteAllFiles([FromBody]JObject request)
        {
            int sn = Int32.Parse(request.GetValue("sn").ToString());

            if (sn >= 0)
            {
                var dev = DeviceDao.Get.DeviceGet(sn);
                if (dev.IsConnected)
                {
                    dev.DeleteAllFiles();
                }
                var detailList = UpdateDeviceDao.Get.FindBySN(sn).detail;
                detailList.Clear();
            }
        }
    }
}
