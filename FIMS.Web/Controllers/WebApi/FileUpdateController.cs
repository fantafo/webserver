﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace FIMS.Web.Controllers.WebApi
{
    //사용안함 -> updateFileController 사용
    //
    //카테고리 추가로 디비사용으로 변경
    public class FileUpdateController : ApiController
    {
        static string downloadPath = HttpContext.Current.Server.MapPath("~/file/");
        static string virtualPath = "/file/";

        struct DirectoryItem
        {
            public Uri BaseUri;

            public string AbsolutePath
            {
                get
                {
                    return string.Format("{0}/{1}", BaseUri, Name);
                }
            }

            public DateTime DateCreated;
            public bool IsDirectory;
            public string Name;
            public List<DirectoryItem> Items;
        }

        static ILog log = LogManager.GetLogger("Web." + typeof(FileUpdateController).Name);


        /// <summary>
        /// 전체 파일 목록을 불러온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage FileList()
        {
            var list = FileInfoDao.Get.GetList();
            var result = new { data = new List<FileInfo>(list) };
            return Request.CreateResponse(HttpStatusCode.OK, result);
            //using (var db = Database.Open())
            //{
            //    var list = FileInfoDao.Get(db).GetList();
            //    var result = new { data = new List<FileInfo>(list) };
            //    return Request.CreateResponse(HttpStatusCode.OK, result);
            //}
        }

        //[HttpPost]
        //public HttpResponseMessage UploadFile()
        //{
        //    // Check if the request contains multipart/form-data.
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    HttpResponseMessage result = null;
        //    var httpRequest = HttpContext.Current.Request;
        //    ApkHelper apkHelper = new ApkHelper();

        //    using (var db = Database.Open())
        //    using (var trans = db.BeginTransaction())
        //    {
        //        if (httpRequest.Files.Count > 0)
        //        {
        //            log.Debug("파일 수 " + httpRequest.Files.Count);
        //            foreach (string file in httpRequest.Files)
        //            {
        //                var postedFile = httpRequest.Files[file];
        //                string ext = System.IO.Path.GetExtension(postedFile.FileName);
        //                //특정 확장자만 업로드
        //                if (!FimsSetting.UpdateExtension.Contains(ext))
        //                {
        //                    continue;
        //                }

        //                var filePath = downloadPath + ext.Replace(".", "") + "/" + postedFile.FileName;
        //                postedFile.SaveAs(filePath);
        //                FileInfo new_file = FileInfoDao.Get(db).GetBypkgName(postedFile.FileName);
        //                if (new_file == null)
        //                {
        //                    new_file = new FileInfo();
        //                }

        //                new_file.FileName = postedFile.FileName;
        //                if (new_file.FileName.IsEmpty())
        //                {
        //                    log.Error("fileName is Empty");
        //                }
        //                new_file.Size = postedFile.ContentLength;
        //                new_file.Path = virtualPath + ext.Replace(".", "") + "/" + postedFile.FileName;
        //                new_file.UpdateDate = DateTime.Now;
        //                new_file.UpdateCheck = true;
        //                if (ext == ".apk")
        //                {
        //                    var info = apkHelper.GetInfo(filePath);
        //                    new_file.pkg = info.Value.Item1;
        //                    new_file.Version = info.Value.Item2;
        //                }
        //                try
        //                {
        //                    db.SaveOrUpdate(new_file);
        //                    trans.Commit();
        //                    result = Request.CreateResponse(HttpStatusCode.OK, new { msg = "정상적으로 파일이 등록되었습니다." });
        //                }
        //                catch (Exception e)
        //                {
        //                    log.Error(e);
        //                    log.Debug("파일 정보 등록 실패\n" + e);
        //                    result = Request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "파일 등록이 실패하였습니다." });
        //                }
        //            }
        //        }
        //        else
        //        {
        //            result = Request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "파일 등록이 실패하였습니다." });
        //        }
        //    }
        //    return result;
        //}

        //파일 갱신
        [HttpGet]
        public void UpdateFileList()
        {
            //ftp 서버 확인
            string ftpip = "";
            if (FimsSetting.isftpLocalServer)
            {
                ftpip = "ftp://" + IpHelper.InnerIP;
            }
            else
            {
                //TODO.. 로컬ftp 서버가 아닐경우..
                return;
            }
            
            //ftp 서버 파일 읽어오기
            var list = GetDirectoryInformation(ftpip, "", "");

            var oldFileList = FileInfoDao.Get.GetList();

            var newFileList = GetDirectoryFileInfo(list);
        
            //초기화
            FileInfoDao.Get.Clear();

            //비교하여 데이터 갱신
            for (int i=0; i< newFileList.Count; ++i)
            {
                //경로, 이름 
                FileInfo fileinfo = oldFileList.Find(x => x.FileName == newFileList[i].FileName && x.Path == newFileList[i].Path);
                if (fileinfo != null)
                {
                    newFileList[i].UpdateCheck = fileinfo.UpdateCheck;
                    newFileList[i].RemoveCheck = fileinfo.RemoveCheck;
                }

                //이미 동일한 파일명이 있을경우 이름 변경
                if (FileInfoDao.Get.GetByName(newFileList[i].FileName) != null)
                {
                    for (int j = 1; j <= 10; ++j)
                    {
                        string newName = System.IO.Path.GetFileNameWithoutExtension(newFileList[i].FileName) + "_" + j + System.IO.Path.GetExtension(newFileList[i].FileName);
                        if (FileInfoDao.Get.GetByName(newName) == null)
                        {
                            newFileList[i].FileName = newName;

                            //파일명 변경
                            FtpWebRequest reqFTP;
                            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(newFileList[i].Path));
                            //이름변경 명령 실행
                            reqFTP.Method = WebRequestMethods.Ftp.Rename;
                            reqFTP.RenameTo = newFileList[i].FileName;
                            reqFTP.UseBinary = true;
                            //reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                            FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                            Stream ftpStream = response.GetResponseStream();

                            ftpStream.Close();
                            response.Close();

                            FileInfoDao.Get.Add(newFileList[i]);
                            break;
                        }
                        else
                            continue;
                    }

                }
                else
                {
                    FileInfoDao.Get.Add(newFileList[i]);
                }
            }
        }

        //ftp 서버 디렉토리 읽어오기
        //https://social.msdn.microsoft.com/Forums/vstudio/en-US/7758e7a8-e063-4eec-a6c1-c76a59d02852/listing-all-files-from-ftp-server-using-c?forum=csharpgeneral
        // FTP MLSD 명령어 지원x
        private List<DirectoryItem> GetDirectoryInformation(string address, string username, string password)
        {
            FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(address);
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            //request.Credentials = new NetworkCredential(username, password);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;

            List<DirectoryItem> returnValue = new List<DirectoryItem>();
            string[] list = null;

            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                list = reader.ReadToEnd().Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            foreach (string line in list)
            {
                // Windows FTP Server Response Format
                // DateCreated    IsDirectory    Name
                string data = line;

                // Parse date
                string date = data.Substring(0, 17);
                //DateTime dateTime = DateTime.Parse(date);
                DateTime dateTime = DateTime.ParseExact(date, "MM-dd-yy  hh:mmtt", CultureInfo.InvariantCulture);
                data = data.Remove(0, 24);

                // Parse <DIR>
                string dir = data.Substring(0, 5);
                bool isDirectory = dir.Equals("<dir>", StringComparison.InvariantCultureIgnoreCase);
                data = data.Remove(0, 5);
                data = data.Remove(0, 10);

                // Parse name
                string name = data;

                // Create directory info
                DirectoryItem item = new DirectoryItem();
                item.BaseUri = new Uri(address);
                item.DateCreated = dateTime;
                item.IsDirectory = isDirectory;
                item.Name = name;

                //Debug.WriteLine(item.AbsolutePath);
                item.Items = item.IsDirectory ? GetDirectoryInformation(item.AbsolutePath, username, password) : null;

                returnValue.Add(item);
            }

            return returnValue;
        }

        private List<FileInfo> GetDirectoryFileInfo(List<DirectoryItem> list)
        {
            List<FileInfo> FileInfoList = new List<FileInfo>();

            for(int i=0; i < list.Count; ++i)
            {
                if (list[i].IsDirectory)
                {
                    var addList = GetDirectoryFileInfo(list[i].Items);
                    FileInfoList.AddRange(addList);
                }
                else
                {
                    //특정 확장자만 업로드
                    string ext = System.IO.Path.GetExtension(list[i].Name);
                    if (!FimsSetting.UpdateExtension.Contains(ext))
                    {
                        continue;
                    }

                    FileInfo fileinfo = new FileInfo();
                    fileinfo.FileName = list[i].Name;
                    fileinfo.UpdateDate = list[i].DateCreated;
                    fileinfo.Path = list[i].AbsolutePath;
                    fileinfo.UpdateCheck = true;
                    fileinfo.RemoveCheck = false;

                    //file installpath
                    fileinfo.InstallPath = FimsSetting.InstallPath + ext.Replace(".", "") + "/";

                    //file size
                    FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri(fileinfo.Path));
                    request.Proxy = null;
                    //request.Credentials = new NetworkCredential("user", "password");
                    request.Method = WebRequestMethods.Ftp.GetFileSize;

                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    fileinfo.Size = response.ContentLength;
                    response.Close();
                    FileInfoList.Add(fileinfo);
                }
            }

            return FileInfoList;
        }

        [HttpPost]
        public HttpResponseMessage Editor([FromBody]JObject request)
        {
            //현재 액션은 edit만 있음
            var action = request.GetValue("action").ToString();
            var data = request.GetValue("data");
            //다중 수정,삭제 가능하도록 리스트로 설정.. 하지만 선택을 하나만 가능하도록 설정해서.. 
            IList<FileInfo> changedobjects = new List<FileInfo> { };

            foreach (JProperty x in data)
            {
                //initialize empty object of class
                FileInfo changedobject = (FileInfo)Activator.CreateInstance(typeof(FileInfo));

                //만약 키값이 string
                changedobject.GetType().GetProperty("FileName").SetValue(changedobject, x.Name.ToString());
                //만약 키값이 int면
                //changedobject.GetType().GetProperty("SN").SetValue(changedobject, Int32.Parse(x.Name.ToString()));

                //loop through each of JSON object sub properties, change the property name from camel case to pascal case and map to object's properties
                foreach (JProperty y in x.Value)
                {
                    string propertyname = char.ToUpper(y.Name[0]) + y.Name.Substring(1);

                    if (changedobject.GetType().GetProperty(propertyname) == null)
                        continue;

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "Boolean")
                    {
                        try
                        {
                            //js에서 UpdateCheck가 false일 경우 "" 로 넘어옴, true 일 경우 "true"
                            bool check = false;
                            if (!y.Value.ToString().IsEmpty() && y.Value.ToString() == "true")
                                check = true;

                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, check);
                        }
                        catch (Exception e)
                        {
                            log.Debug($"{e}");
                        }
                    }

                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                    {
                        try
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value.ToString());
                        }
                        catch (Exception e)
                        {
                            log.Debug($"{e}");
                        }
                    }

                }
                //add object to changed objects List
                changedobjects.Add(changedobject);
            }

            if (action == "edit")
            {
                //edit 액션에는 FileName, UpdateCheck, InstallPath, RemoveCheck 정보 있음

                foreach (FileInfo newinfo in changedobjects)
                {
                    var oldinfo = FileInfoDao.Get.GetByName(newinfo.FileName);
                    if(oldinfo.UpdateCheck != newinfo.UpdateCheck)
                    {
                        //설치 변경
                        if (newinfo.UpdateCheck)
                        {
                            //설치 o 변경 -> 삭제 해제
                            FileInfoDao.Get.UpdateCheckChange(newinfo.FileName, true);
                            FileInfoDao.Get.RemoveCheckChange(newinfo.FileName, false);
                        }
                        else
                        {
                            //설치 x 변경
                            FileInfoDao.Get.UpdateCheckChange(newinfo.FileName, false);
                        }

                    }else if (oldinfo.RemoveCheck != newinfo.RemoveCheck)
                    {
                        //삭제 변경
                        if (newinfo.RemoveCheck)
                        {
                            //삭제 o 변경 -> 설치 해제
                            FileInfoDao.Get.UpdateCheckChange(newinfo.FileName, false);
                            FileInfoDao.Get.RemoveCheckChange(newinfo.FileName, true);
                        }
                        else
                        {
                            //삭제 x 변경
                            FileInfoDao.Get.RemoveCheckChange(newinfo.FileName, false);
                        }
                    }
                }
            }else if(action == "remove")
            {
                for(int i = 0; i< changedobjects.Count; ++i)
                {
                    //ftp 파일 삭제
                    FtpWebRequest Ftprequest = (FtpWebRequest)FtpWebRequest.Create(changedobjects[i].Path);
                    //request.Credentials = new NetworkCredential(username, password);
                    Ftprequest.Method = WebRequestMethods.Ftp.DeleteFile;
                    Ftprequest.GetResponse();
                    FileInfoDao.Get.RemoveByName(changedobjects[i].FileName);
                }
            }

            var list = FileInfoDao.Get.GetList();
            var result = new { data = new List<FileInfo>(list) };
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
