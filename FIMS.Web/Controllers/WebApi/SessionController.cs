﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace FIMS.Web.Controllers.WebApi
{

    public class SessionController : ApiController
    {
        static ILog log = LogManager.GetLogger("Web." + typeof(SessionController).Name);

        /// <summary>
        /// 게임이 끝나지 않은 세션들을 가져온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage ActiveSessions()
        {
            using (var db = Database.Open())
            {
                IList<GameSession> sessions = Fims.gameSessionManager.GetActiveSessions(db);
                List<object> result = new List<object>();
                for (int i = 0; i < sessions.Count; i++)
                {
                    result.Add(sessions[i].ToSerializeObject());
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
        }
    }
}
