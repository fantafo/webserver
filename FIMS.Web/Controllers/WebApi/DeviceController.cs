﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace FIMS.Web.Controllers.WebApi
{
    public class DeviceController : ApiController
    {
        //연결된 기기를 목록을알려준다.
        [HttpGet]
        public HttpResponseMessage FindConnectDevice()
        {
            //using (var db = Database.Open())
            {
                IList<Device> device = DeviceDao.Get.FindConnectDeviceName();
                if (device != null && device.Count > 0)
                {
                    List<object> result = new List<object>(device.Count);
                    for (int i = 0; i < device.Count; i++)
                    {
                        result.Add(device[i].ToSerializeObject());
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        //Device의 select항목을 set로 변경
        [HttpGet]
        public HttpResponseMessage SetPickDevice(int sn, bool set)
        {
            //using (var db = Database.Open())
            {
                Device device = DeviceDao.Get.DeviceGet(sn);
                bool result = false;
                if (device != null && device.Pick != set)
                {
                    //using (var trans = db.BeginTransaction())
                    {
                        device.Pick = set;
                        if (!set)
                        {
                            device.Cont = set;
                            device.Live = set;
                            device.UserName = System.String.Empty;

                        }
                        //db.Update(device);
                        //trans.Commit();
                    }
                    result = true;
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
        }

        //Device의 select항목을 T로 변경
        [HttpGet]
        public HttpResponseMessage SetPickDevice(List<int> sn, bool set)
        {
            //using (var db = Database.Open())
            {
                foreach (var x in sn)
                {
                    Device device = DeviceDao.Get.DeviceGet(x);
                    if (device != null && device.Pick != set)
                    {
                        //using (var trans = db.BeginTransaction())
                        {
                            device.Pick = set;
                            if (!set)
                            {
                                device.Cont = set;
                                device.Live = set;
                                device.UserName = System.String.Empty;
                            }
                            //db.Update(device);
                            //trans.Commit();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }
        public static IList<Device> NaturalSort(IList<Device> list)
        {
            int maxLen = list.Select(s => s.Name.Length).Max();
            Func<string, char> PaddingChar = s => char.IsDigit(s[0]) ? ' ' : char.MaxValue;

            return list
                    .Select(s =>
                        new
                        {
                            OrgStr = s,
                            SortStr = Regex.Replace(s.Name, @"(\d+)|(\D+)", m => m.Value.PadLeft(maxLen, PaddingChar(m.Value)))
                        })
                    .OrderBy(x => x.SortStr)
                    .Select(x => x.OrgStr)
                    .ToList<Device>();
        }
        //다수의 기기를 선택
        [HttpGet]
        public HttpResponseMessage SetMultiPickDevice(int count)
        {
            //using (var db = Database.Open())
            {
                IList<Device> device = DeviceDao.Get.FindConnectDeviceName();
                /*
                device = (from c in device
                          orderby Regex.Replace(c.Name, @"\d", ""), Regex.Replace(c.Name, @"\D", "")
                          select c).ToList();
                          */
                device = NaturalSort(device);

                IList<int> result = new List<int>();
                //using (var trans = db.BeginTransaction())
                {
                    for (int num = 0; num < count && num < device.Count(); ++num)
                    {
                        device[num].Pick = true;
                        result.Add(device[num].SN);
                        //db.Update(device[num]);
                    }
                    //trans.Commit();
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
        }

        [HttpGet]
        public HttpResponseMessage Test()
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage DeviceInit()
        {
            //using (var db = Database.Open())
            {
                IList<Device> devices = DeviceDao.Get.ToList();
                //using (var trans = db.BeginTransaction())
                {
                    foreach (var device in devices)
                    {
                        if (device.Pick)
                        {
                            device.Pick = false;
                            //db.Update(device);
                        }
                    }
                    //trans.Commit();
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, new { msg = $"초기화 완료." });
        }

        [HttpGet]
        public int DevicConnectCount()
        {
            //using (var db = Database.Open())
            {
                return DeviceDao.Get.FindConnectDeviceCount();
            }
        }

        public class updateDevice
        {
            public int SN { get; set; }
        }

        public class updateDevicObject
        {
            public List<updateDevice> updateDevice { get; set; }
        }

        [HttpPost]
        public void CheckStorage(List<updateDevice> updateDevices)
        {
            //item1 파일이름 item2 파일 사이즈 item3 설치경로
            List<Tuple<string, long, string>> updatefileList = new List<Tuple<string, long, string>>();

            //var FileInfoList = FileInfoDao.Get.GetUpdateList();

            List<FileInfo> FileInfoList;

            using (var db = Database.Open())
            {
                FileInfoList = UpdateFileInfoDao.Get(db).GetUpdateCheckList();
            }

            foreach (FileInfo file in FileInfoList)
            {
                updatefileList.Add(new Tuple<string, long, string>(file.FileName, file.Size, file.InstallPath));
            }

            if (FileInfoList.Count == 0)
                return;

            foreach (updateDevice device in updateDevices)
            {
                DeviceDao.Get.DeviceGet(device.SN).CheckStorage(updatefileList);
            }
        }
    }
}
