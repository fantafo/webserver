﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using Newtonsoft.Json.Linq;
using System.IO;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Ajax;
using System.Web.UI.WebControls;
using System.Net.Http.Headers;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO.Packaging;
using System.Linq;
using log4net;

namespace FIMS.Web.Controllers.WebApi
{
    public class MemberController : ApiController
    {
        static ILog log = LogManager.GetLogger("Web." + typeof(MemberController).Name);

        [HttpGet]
        public HttpResponseMessage Signup()
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            foreach (var pair in Request.GetQueryNameValuePairs())
            {
                map.Add(pair.Key, pair.Value);
            }

            using (var db = Database.Open())
            {
                if (MemberDao.Get(db).ContainsByEmail(map["email"]))
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new { id = "email", msg = "이미 가입된 이메일주소입니다." });
                }

                Member member = new Member()
                {
                    Email = map["email"].Trim(),
                    Password = Member.ConvertPassword(map["password"]),
                    Name = map["name"].Trim(),
                    NickName = map["nickname"].Trim(),
                    Gender = (GenderType)int.Parse(map["gender"]),
                    Mobile = new Regex(@"(-|\)|\+|\-)").Replace(map["mobile"].Trim(), ""),
                    RegistDate = DateTime.Now,
                    Id = Member.ConvertPassword(map["password"])
                };

                using (var transaction = db.BeginTransaction())
                {
                    MemberDao.Get(db).Save(member);
                    if (member.SN != 0)
                    {
                        transaction.Commit();
                        this.SetUserSN(member);
                        return Request.CreateResponse(HttpStatusCode.OK, new { msg = "정상적으로 가입됐습니다." });
                    }
                    else
                    {
                        transaction.Rollback();
                        return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "알 수 없는 오류가 발생했습니다." });
                    }
                }
            }
        }

        //사용안함
        [HttpGet]
        public HttpResponseMessage FindName([FromUri] string name)
        {
            using (var db = Database.Open())
            {
                if (name != null && name.Length > 0)
                {
                    IList<Member> members = MemberDao.Get(db).FindName(name);
                    if (members != null && members.Count > 0)
                    {
                        List<object> result = new List<object>(members.Count);
                        for (int i = 0; i < members.Count; i++)
                        {
                            result.Add(members[i].ToSerializeObject());
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public HttpResponseMessage ChangeChar([FromUri] short head, [FromUri] short body)
        {
            Dictionary<string, string> map = new Dictionary<string, string>();
            foreach (var pair in Request.GetQueryNameValuePairs())
            {
                map.Add(pair.Key, pair.Value);
            }

            using (var db = Database.Open())
            {
                Member user = MemberDao.Get(db).Get(this.GetUserSN());
                if (user == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

                using (var trans = db.BeginTransaction())
                {
                    user.AvatarHead = head;
                    user.AvatarBody = body;

                    db.Update(user);
                    trans.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }

        [HttpGet]
        public HttpResponseMessage dummy()
        {
            using (var db = Database.Open())
            {
                using (var trans = db.BeginTransaction())
                {
                    IList<Member> members = MemberDao.Get(db).ToList();
                    Random r = new Random();
                    for (int i = 0; i < 10; ++i)
                    {
                        Member user = members[r.Next(0, members.Count)];
                        if (user == null)
                        {
                            --i;
                            continue;
                            //return Request.CreateResponse(HttpStatusCode.BadRequest);
                        }
                        Completion num = new Completion();
                        num.Score = 100;
                        num.PlayTime = 30;
                        user.Completions.Add(num);
                        db.Update(user);
                    }
                    trans.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }

        [HttpPost]
        public string Editor([FromBody]JObject request)
        {
            //관리자 로그인 필요
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return ("NoAdmin");
            }

            var action = request.GetValue("action").ToString();
            var data = request.GetValue("data");

            //다중 수정,삭제 가능하도록 리스트로 설정.. 하지만 선택을 하나만 가능하도록 설정해서.. 
            IList<Member> changedobjects = new List<Member> { };

            if (action == "create")
            {
                string datastring = JsonConvert.SerializeObject(data);
                datastring = datastring.Trim(new char[] { '[', ']' });
                JObject dataJobject = new JObject();
                var DeserializeObject = (JObject)JsonConvert.DeserializeObject(datastring);
                dataJobject.Add("0", DeserializeObject);
                data = dataJobject;
            }

            foreach (JProperty x in data)
            {
                //initialize empty object of class
                Member changedobject = (Member)Activator.CreateInstance(typeof(Member));

                //만약 키값이 string
                //changedobject.GetType().GetProperty("Id").SetValue(changedobject, x.Name.ToString());
                //만약 키값이 int면
                changedobject.GetType().GetProperty("SN").SetValue(changedobject, Int32.Parse(x.Name.ToString()));

                //loop through each of JSON object sub properties, change the property name from camel case to pascal case and map to object's properties
                foreach (JProperty y in x.Value)
                {
                    string propertyname = char.ToUpper(y.Name[0]) + y.Name.Substring(1);

                    var aa = changedobject.GetType().GetProperty(propertyname).PropertyType.Name;
                    if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                    {
                        changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value.ToString());
                    }
                    else if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "Integer")
                    {
                        changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, Int32.Parse(y.Value.ToString()));
                    }
                    else if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "GenderType")
                    {
                        if (y.Value.Equals("남성") || y.Value.ToString().Equals("0"))
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, (GenderType)Enum.ToObject(typeof(GenderType), 0));
                        }
                        else if (y.Value.Equals("여성") || y.Value.ToString().Equals("1"))
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, (GenderType)Enum.ToObject(typeof(GenderType), 1));
                        }
                        else
                        {
                            //changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value);
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, null);
                        }
                    }

                }
                //add object to changed objects List
                changedobjects.Add(changedobject);
            }


            if (action == "create")
            {
                foreach (Member member in changedobjects)
                {
                    using (var db = Database.Open())
                    {
                        if (MemberDao.Get(db).FindID(member.Id) != null)
                        {
                            return ("OverlapId");
                        }
                        Member user = new Member { };
                        user.Id = member.Id;
                        user.Name = member.Name;
                        user.Major = member.Major;
                        user.Professor = member.Professor;
                        user.Gender = member.Gender;

                        using (var transaction = db.BeginTransaction())
                        {
                            MemberDao.Get(db).Save(member);
                            if (member.SN != 0 && member.Id != null && member.Name != null)
                            {
                                transaction.Commit();
                                this.SetUserSN(member);
                                return ("OK");
                                //return Request.CreateResponse(HttpStatusCode.OK, new { msg = "정상적으로 가입됐습니다." });
                            }
                            else
                            {
                                transaction.Rollback();
                                return ("NO");
                                //return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "알 수 없는 오류가 발생했습니다." });
                            }
                        }
                    }
                }
            }
            else if (action == "edit")
            {
                foreach (Member member in changedobjects)
                {
                    using (var db = Database.Open())
                    {
                        Member edit = MemberDao.Get(db).Get(member.SN);
                        //만약 이미 존재하는 학번으로 변경 시 막기 위해서
                        Member check = MemberDao.Get(db).FindID(member.Id);
                        if (check != null && edit.SN != check.SN)
                        {
                            return ("NO");
                        }
                        edit.Id = member.Id;
                        edit.Name = member.Name;
                        edit.Major = member.Major;
                        edit.Professor = member.Professor;
                        edit.Gender = member.Gender;

                        using (var trans = db.BeginTransaction())
                        {
                            db.Update(edit);
                            trans.Commit();
                            //return ("OK");
                            //return Request.CreateResponse(HttpStatusCode.OK);
                        }
                    }
                }
                return ("OK");

            }
            else if (action == "remove")
            {
                foreach (Member member in changedobjects)
                {
                    using (var db = Database.Open())
                    {
                        Member user = MemberDao.Get(db).FindID(member.Id.ToString());
                        if (user == null)
                        {
                            return ("OK");
                            //return Request.CreateResponse(HttpStatusCode.BadRequest, new { msg = "이미 삭제된 정보입니다." });
                        }

                        using (var trans = db.BeginTransaction())
                        {
                            db.Delete(user);
                            trans.Commit();
                            return ("OK");
                            //return Request.CreateResponse(HttpStatusCode.OK);
                        }
                    }
                }
            }
            return ("NO");
            //return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [HttpPost]
        public string CompletionEditor([FromBody]JObject request)
        {
            //관리자 로그인 필요
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return ("NoAdmin");
            }

            var action = request.GetValue("action").ToString();
            var data = request.GetValue("data");

            //다중 수정,삭제 가능하도록 리스트로 설정.. 하지만 선택을 하나만 가능하도록 설정해서.. 
            IList<Member> changedobjects = new List<Member> { };

            if (action == "create")
            {
                string datastring = JsonConvert.SerializeObject(data);
                datastring = datastring.Trim(new char[] { '[', ']' });
                JObject dataJobject = new JObject();
                var DeserializeObject = (JObject)JsonConvert.DeserializeObject(datastring);
                dataJobject.Add("0", DeserializeObject);
                data = dataJobject;
            }

            foreach (JProperty x in data)
            {
                //initialize empty object of class
                Member changedobject = (Member)Activator.CreateInstance(typeof(Member));
                changedobject.Completions = new List<Completion>();
                Completion completion = (Completion)Activator.CreateInstance(typeof(Completion));
                //만약 키값이 string
                //changedobject.GetType().GetProperty("Id").SetValue(changedobject, x.Name.ToString());
                //만약 키값이 int면
                changedobject.GetType().GetProperty("SN").SetValue(changedobject, Int32.Parse(x.Name.ToString()));

                //loop through each of JSON object sub properties, change the property name from camel case to pascal case and map to object's properties
                foreach (JProperty y in x.Value)
                {
                    string propertyname = char.ToUpper(y.Name[0]) + y.Name.Substring(1);

                    if (propertyname == "Id" || propertyname == "SN")
                    {
                        if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, y.Value.ToString());
                        }
                        else if (changedobject.GetType().GetProperty(propertyname).PropertyType.Name == "Int32")
                        {
                            changedobject.GetType().GetProperty(propertyname).SetValue(changedobject, Int32.Parse(y.Value.ToString()));
                        }
                    }
                    else if (propertyname == "Score" || propertyname == "Date" || propertyname == "PlayTime" || propertyname == "Ord")
                    {
                        if (completion.GetType().GetProperty(propertyname).PropertyType.Name == "String")
                        {
                            completion.GetType().GetProperty(propertyname).SetValue(completion, y.Value.ToString());
                        }
                        else if (completion.GetType().GetProperty(propertyname).PropertyType.Name == "Int32")
                        {
                            completion.GetType().GetProperty(propertyname).SetValue(completion, Int32.Parse(y.Value.ToString()));
                        }
                        else if (completion.GetType().GetProperty(propertyname).PropertyType.Name == "DateTime")
                        {
                            completion.GetType().GetProperty(propertyname).SetValue(completion, DateTime.Parse(y.Value.ToString()));
                            //DateTime datetime = y.Value.ToString();
                            //string datetime = Regex.Replace(y.Value.ToString(), @"\D", "");
                            //completion.GetType().GetProperty(propertyname).SetValue(completion, DateTime.Parse(datetime));
                        }
                    }
                    //그외 데이터는 필요없어서 따로 저장안함
                }
                //add object to changed objects List
                changedobject.Completions.Add(completion);
                changedobjects.Add(changedobject);
            }


            if (action == "create")
            {
                foreach (Member member in changedobjects)
                {
                    using (var db = Database.Open())
                    {
                        Member user = MemberDao.Get(db).FindID(member.Id);
                        if (user == null)
                        {
                            return ("NoId");
                        }
                        foreach (Completion completion in member.Completions)
                        {
                            user.Completions.Add(completion);
                        }
                        using (var trans = db.BeginTransaction())
                        {
                            db.Update(user);
                            trans.Commit();
                            return ("OK");
                        }
                    }
                }

            }
            else if (action == "edit")
            {
                using (var db = Database.Open())
                {
                    for (int i = 0; i < changedobjects.Count; ++i)
                    {
                        //Member member = MemberDao.Get(db).Get(changedobjects[i].SN);
                        for (int j = 0; j < changedobjects[i].Completions.Count; ++j)
                        {
                            int index = changedobjects[i].Completions[j].Ord;
                            Member member = MemberDao.Get(db).Get(changedobjects[i].SN);
                            member.Completions[index] = changedobjects[i].Completions[j];
                            db.Update(member);
                        }
                    }
                    using (var trans = db.BeginTransaction())
                    {
                        trans.Commit();
                        return ("OK");

                    }
                }
            }
            else if (action == "remove")
            {
                using (var db = Database.Open())
                {
                    for (int i = 0; i < changedobjects.Count; ++i)
                    {
                        //Member member = MemberDao.Get(db).Get(changedobjects[i].SN);
                        for (int j = 0; j < changedobjects[i].Completions.Count; ++j)
                        {
                            Member member = MemberDao.Get(db).Get(changedobjects[i].SN);
                            member.Completions.RemoveAt(changedobjects[i].Completions[j].Ord);
                            db.Update(member);
                        }
                    }
                    using (var trans = db.BeginTransaction())
                    {
                        trans.Commit();
                        return ("OK");

                    }
                }
            }
            return ("NO");
            //return Request.CreateResponse(HttpStatusCode.OK);
        }
        
        //엑셀파일 저장
        public class CustomMultipartFormDataStreamProvider : MultipartFormDataStreamProvider
        {
            public CustomMultipartFormDataStreamProvider(string path) : base(path) { }

            public override string GetLocalFileName(HttpContentHeaders headers)
            {
                return headers.ContentDisposition.FileName.Replace("\"", string.Empty);
            }
        }

        [HttpGet]
        [HttpPost]
        public async Task<HttpResponseMessage> PostFormData()
        {
            //관리자 로그인 필요
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "관리자 로그인이 필요합니다." });
            }

            //전체 삭제
            String FolderName = HttpContext.Current.Server.MapPath("~/file");
            var FullFileNameList = new List<string>();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(FolderName);
            if (di != null)
            {
                foreach (System.IO.FileInfo File in di.GetFiles())
                {
                    if (File.Extension.ToLower().CompareTo(".xlsx") == 0)
                    {
                        File.Delete();
                    }
                }
            }

            // 요청이 multipart/form-data를 담고 있는지 확인합니다.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/file");
            //var provider = new MultipartFormDataStreamProvider(root);
            CustomMultipartFormDataStreamProvider provider = new CustomMultipartFormDataStreamProvider(root);

            try
            {
                // 폼 데이터 읽기.
                await Request.Content.ReadAsMultipartAsync(provider);
                //return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
            return ExcelRead();
        }

        [HttpGet]
        [HttpPost]
        public HttpResponseMessage ExcelRead()
        {
            ExcelHelper helper = new ExcelHelper();
            String FolderName = HttpContext.Current.Server.MapPath("~/file");
            var FullFileNameList = new List<string>();
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(FolderName);
            if (di == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "파일이 없습니다." });
            }
            foreach (System.IO.FileInfo File in di.GetFiles())
            {
                if (File.Extension.ToLower().CompareTo(".xlsx") == 0)
                {
                    FullFileNameList.Add(File.FullName);
                }
            }
            if (FullFileNameList.Count <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "파일이 없습니다." });
            }
            
            helper.ReadExcelData3(FullFileNameList[0]);

            if (helper.data == null)
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "엑셀 파일 내용을 확인해주세요" });

            for (int i = 0; i < FullFileNameList.Count; ++i)
            {
                System.IO.FileInfo fi = new System.IO.FileInfo(FullFileNameList[i]);
                try
                {
                    fi.Delete();
                }
                catch (System.IO.IOException e)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, e);
                }
            }

            //엑셀 파일 읽은 데이터 저장
            List<Member> memberList = new List<Member>();
            //1열 학번, 2열 이름, 3열 전공, 4열 지도교수 5열 성별
            for (int r = 2; r < helper.data.Count; r++)
            {
                if (helper.data[r][0] == null)
                {
                    continue;
                }
                
                Member member = new Member
                {
                    Id = helper.data[r][0].ToString(),
                    Name = helper.data[r][1].ToString(),
                };

                /*
                GenderType gender = 0;
                if (helper.data[r][4] == null)
                    gender = GenderType.Male;
                else
                {
                    switch (helper.data[r][4].ToString())
                    {
                        case "남":
                        case "남자":
                        case "남성":
                            {
                                gender = GenderType.Male;
                                break;
                            }
                        case "여":
                        case "여자":
                        case "여성":
                            {
                                gender = GenderType.Female;
                                break;
                            }
                        default:
                            {
                                gender = GenderType.Male;
                                break;
                            }
                    }
                }
                */
                member.Major = (helper.data[r][2] == null) ? null : helper.data[r][2].ToString();
                //member.Professor = (helper.data[r][3] == null) ? null : helper.data[r][3].ToString();
                //member.Gender = gender;
                //member.AvatarBody = (short)gender;
                //member.AvatarHead = (short)gender;

                memberList.Add(member);
                /*
                for (int c = 1; c <= helper.data.GetLength(1); c++)
                {
                    if (helper.data[r, c] == null)
                    {
                        continue;
                    }
                    var num = helper.data[r, c];
                    // Data 빼오기
                    // data[r, c] 는 excel의 (r, c) 셀 입니다.
                    // data.GetLength(0)은 엑셀에서 사용되는 행의 수를 가져오는 것이고,
                    // data.GetLength(1)은 엑셀에서 사용되는 열의 수를 가져오는 것입니다.
                    // GetLength와 [ r, c] 의 순서를 바꿔서 사용할 수 있습니다.
                }
                */
            }


            //db 수정
            using (var db = Database.Open())
            {

                var overlapcount = 0;
                var error = 0;
                for (int i = 0; i < memberList.Count; ++i)
                {
                    Member member = MemberDao.Get(db).FindID(memberList[i].Id);
                    if (member != null)
                    {
                        overlapcount++;
                        using (var transaction = db.BeginTransaction())
                        {
                            member.Name = memberList[i].Name;
                            member.Major = memberList[i].Major;
                            db.Update(member);
                            transaction.Commit();
                        }
                        continue;
                    }

                    using (var transaction = db.BeginTransaction())
                    {
                        MemberDao.Get(db).Save(memberList[i]);
                        if (memberList[i].SN != 0)
                        {
                            transaction.Commit();
                            this.SetUserSN(memberList[i]);
                        }
                        else
                        {
                            error++;
                            transaction.Rollback();
                            //return Request.CreateResponse(HttpStatusCode.InternalServerError, new { msg = "알 수 없는 오류가 발생했습니다." });
                        }
                    }
                }
                if (overlapcount > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, new { msg = overlapcount + "명 정보 수정, " + (memberList.Count-overlapcount) + "명 등록 성공" });
                else
                    return Request.CreateResponse(HttpStatusCode.OK, new { msg = "등록 성공" });
            }
        }

        private SharedStringItem GetSharedStringItemById(WorkbookPart wbPart, int id)
        {
             return wbPart.SharedStringTablePart.SharedStringTable.Elements < SharedStringItem > ().ElementAt(id);
        }

        public string saveFile(FileUpload fu)
        {
            //서버에 디렉토리가 존재하는 확인하고 디렉토리가 없다면 디렉토리 생성
            DirectoryInfo dInfo = new DirectoryInfo(@"C:\Users\leon\Desktop\YunKyu\FIMS_v0.31\FIMS\FIMS.Web\file");
            if (!dInfo.Exists)
            {
                dInfo.Create();
            }

            //새로 업로드 할 파일과 동일한 파일명이 서버에 존재 할 경우
            //파일명에 언더바(_)와 함께 순서 번호를 붙여서 파일 중복을 피한다.
            string fileName = fu.FileName; //클라이언트 측 파일 경로가 제거된 순수 파일명
            string fileFullName = @"C:\Users\leon\Desktop\YunKyu\FIMS_v0.31\FIMS\FIMS.Web\file" + fileName; //업로드될 서버측 경로와 파일명
            System.IO.FileInfo fInfo = new System.IO.FileInfo(fileFullName);

            string newFileName = string.Empty; //파일 중복을 피하기위해 새로 생성된 파일명

            //서버에 같은 이름의 파일이 존재 하는지 확인
            if (fInfo.Exists)
            {
                //같은 이름의 파일이 있다면
                int fIndex = 0; //파일 순서
                string fExtension = fInfo.Extension; //파일의 확장자 추출
                string fRealName = fileName.Replace(fExtension, ""); //파일명의 확장자 제거
                                                                     //파일명이 중복되지 않을때까지 루프를 돌면서 검사
                do
                {
                    fIndex++;//파일 번호 증가
                             //새로운 파일 이름 (확장자 제거된 파일명 + _ + 순서번호 + 확장자)
                    newFileName = fRealName + "_" + fIndex.ToString() + fExtension;
                    //새로운 파일 이름으로 파일정보 생성
                    fInfo = new System.IO.FileInfo(@"C:\Users\leon\Desktop\YunKyu\FIMS_v0.31\FIMS\FIMS.Web\file" + newFileName);
                } while (fInfo.Exists); //파일이 존재 한다면 다시 반복

                fileFullName = @"C:\Users\leon\Desktop\YunKyu\FIMS_v0.31\FIMS\FIMS.Web\file" + newFileName; //서버측 업로드 경로 + 새로운 파일명

                fu.PostedFile.SaveAs(fileFullName); //만들어진 경로와 파일명으로 파일을 서버측에 업로드

                return newFileName; //새로 만들어진 파일명 만 반환
            }
            else
            {
                //같은 이름의 파일이 없다면
                fu.PostedFile.SaveAs(fileFullName); //최초 정해진 경로와 파일명으로 서버측에 업로드

                return fileName; //파일명 반환
            }
        }



        //Member 정보 제거
        [HttpGet]
        public HttpResponseMessage MemberInit()
        {
            //관리자 로그인 필요
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            using (var db = Database.Open())
            {
                using (var trans = db.BeginTransaction())
                {
                    db.Delete("from Member o");
                    db.Flush();
                    trans.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }

        //Completion 정보 제거
        [HttpGet]
        public HttpResponseMessage CompletionInit()
        {
            //관리자 로그인 필요
            CookieHeaderValue cookie = Request.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            using (var db = Database.Open())
            {
                using (var trans = db.BeginTransaction())
                {
                    //db.Delete("from Member o where Completion");
                    //db.Flush();
                    IList<Member> member = MemberDao.Get(db).ToList();
                    foreach (var x in member)
                    {
                        x.Completions.Clear();
                        db.Update(x);
                    }
                    trans.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }

        [HttpGet]
        public HttpResponseMessage IDLogin(long deviceSN, String id)
        {
            try
            {
                using (var db = Database.Open())
                //using (var trans = db.BeginTransaction())
                {
                    var dev = DeviceDao.Get.DeviceGet((int)deviceSN);
                    if (dev == null)
                    {
                        log.Debug("deviceSN " + deviceSN + " 기기가 없습니다.");
                        //TODO.. 게임서버로 불일치 정보 보내기
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    
                    var member = MemberDao.Get(db).FindID(id);
                    if (member == null)
                    {
                        //불일치 정보 전송
                        log.Debug("로그인 시도 실패 ID 없음.. DeviceSN : " + deviceSN + " id: " + id);
                        var sess = GameSessionDao.Get(db).FindCurrentSession((int)deviceSN);
                        sess.LoginFailed(dev);
                    }
                    else
                    {
                        //로그인 성공
                        log.Debug("로그인 시도 성공 DeviceSN : " + deviceSN + " id: " + id);
                        dev.State = DeviceState.Allocated;
                        dev.UserName = member.Name;
                        dev.User = id;
                        //db.Update(dev);
                        //trans.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("Id Packet Error deviceSN :" + deviceSN +"e.msg: " + e);
                throw e;
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        public HttpResponseMessage FastLogin(long deviceSN)
        {
            //long deviceSN = Int64.Parse(request.GetValue("deviceSN").ToString());
            log.Debug("빠른 로그인 시도 DeviceSN : " + deviceSN);
            try
            {
                var dev = DeviceDao.Get.DeviceGet((int)deviceSN);
                if (dev == null)
                {
                    log.Debug("deviceSN " + deviceSN + " 기기가 없습니다.");
                    //TODO.. 게임서버로 불일치 정보 보내기
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                dev.State = DeviceState.Allocated;

                //이름 추가... 락을 걸어서 중복을 없어야 되지만.. 중복 한두개는 괜찮다고 하심
                var guestNames = GuestNameDao.Get.ToList();
                int index = Member.random.Next(0, guestNames.Count);
                dev.UserName = guestNames[index].Name;

                //RunableDeviceConnector은 게임이 진행이 안된 세션(SessionState.GameLaunch)을 찾고
                //DeviceState.Ready 일경우 UserName전송

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception e)
            {
                log.Error("Id Packet Error deviceSN :" + deviceSN);

                throw e;
            }
        }

        [HttpGet]
        public HttpResponseMessage LogOut(long deviceSN)
        {
            try
            {
                //using (var db = Database.Open())
                //using (var trans = db.BeginTransaction())
                {
                    var dev = DeviceDao.Get.DeviceGet((int)deviceSN);
                    if (dev == null)
                    {
                        log.Debug("deviceSN " + deviceSN + " 기기가 없습니다.");
                        //TODO.. 게임서버로 불일치 정보 보내기
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                    dev.State = DeviceState.IdCheck;
                    dev.UserName = "";
                    //db.Update(dev);
                    //trans.Commit();
                }
            }
            catch (Exception e)
            {
                log.Error("Id Packet Error deviceSN :" + deviceSN + "e.msg: " + e);
                throw e;
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
    
}
