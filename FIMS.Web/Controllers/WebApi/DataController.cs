﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace FIMS.Web.Controllers.WebApi
{
    public class DataController : ApiController
    {
        static ILog log = LogManager.GetLogger("Web."+typeof(DataController).Name);

        /// <summary>
        /// 전체 게임 목록을 불러온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Games()
        {
            using (var db = Database.Open())
            {
                var data = GameDao.Get(db).ToList().ToList<Game>();
                //TODO.. 360비디오 외, 더 세부 항목이 생긴다면 변경할것.
                int index = data.FindIndex(x => x.Name == "360 video");
                var filelist = UpdateFileInfoDao.Get(db).GetList();
                //var filelist = FileInfoDao.Get.GetList();
                for(int i=0; i<filelist.Count; ++i)
                {
                    data[index].DetailInfo.Add(filelist[i].Category.ToString() + '#' + filelist[i].FileName);
                }
                //if (filelist.Count > 0)
                //{
                //    using (var trans = db.Transaction)
                //    {
                //        try
                //        {
                //            db.SaveOrUpdate(data);
                //            trans.Commit();
                //        }
                //        catch (Exception e)
                //        {
                //            trans.Rollback();
                //        }
                //    }
                //}
                return Request.CreateResponse(HttpStatusCode.OK, GameDao.Get(db).ToList());
            }
        }

        /// <summary>
        /// 전체 디바이스 목록을 불러온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Devices()
        {
            //using (var db = Database.Open())
            {
                try
                {
                    var list = DeviceDao.Get.ToList();
                    var result = new List<dynamic>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        Device dev = list[i];
                        if (dev != null)
                        {
                            result.Add(new
                            {
                                SN = dev.SN,
                                Enabled = dev.Enabled,
                                Name = dev.Name,
                                Model = dev.Model,
                                ConState = dev.ConnectionState,
                                State = dev.State,
                                Temp = dev.Thermal,
                                Bat = dev.Battery,
                                Cont = dev.Cont,
                                Live = dev.Live,
                                AcceptTime = dev.AcceptTime,
                                Pick = dev.Pick
                            });
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
            }
        }

        /// <summary>
        /// 전체 게임 목록을 불러온다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage EnableDevices(int sn, byte on)
        {
            //using (var db = Database.Open())
            {
                var dev = DeviceDao.Get.DeviceGet(sn);
                if (dev == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "장비를 찾을 수 없습니다.");

                //using (var trans = db.BeginTransaction())
                {
                    bool enabled = (on == 1);

                    dev.Enabled = enabled;
                    //db.Update(dev);
                    //trans.Commit();

                    log.Debug("Set Enable Device " + sn + ":" + on);

                    if (!enabled && Fims.agentServer != null)
                    {
                        var cmd = Fims.agentServer.GetClient(sn);
                        if (cmd != null)
                        {
                            cmd.Disconnect();
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, "OK");
            }
        }
    }
}
