﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FIMS.Web.Controllers.WebApi
{
    public class SettingController : ApiController
    {
        //옵저버 자동 실행 변경
        [HttpGet]
        public HttpResponseMessage ToggleObserver()
        {
            FimsSetting.ObserverAutoStart = !FimsSetting.ObserverAutoStart;
            string msg = FimsSetting.ObserverAutoStart.ToString();

            return Request.CreateResponse(HttpStatusCode.OK, new { chageObserverbtn = msg });
        }
    }
}
