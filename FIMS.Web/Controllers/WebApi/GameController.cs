﻿using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Threading;

namespace FIMS.Web.Controllers.WebApi
{
    /// <summary>
    /// 게임을 실행하기 위해 세션을 생성하거나,
    /// 적용, 실행, 중지, 반환 등의 역할을 명령하는 컨트롤러다.
    /// 
    /// 실제 작동은 FIMS.Core에서 이뤄지지만,
    /// 이곳에서 전달된 데이터를 수집하여 명령을 내리는 역할만을 수행한다.
    /// </summary>
    public class GameController : ApiController
    {
        static ILog log = LogManager.GetLogger("Web." + typeof(GameController).Name);
        

        /// <summary>
        /// 게임세션의 수정된 데이터들을 입력한다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GameServerSetting(string ip, int port, int http)
        {
            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                try
                {
                    var games = GameDao.Get(db).GetUseCommon();
                    foreach (var game in games)
                    {
                        game.ServerIP = ip;
                        //log.Debug("ip: " + ip);
                        game.ServerPort = port;
                        game.ServerComsPort = http;
                        game.LastCheckTime = DateTime.Now;
                        db.Update(game);
                    }
                    trans.Commit();
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        /// <summary>
        /// 해당하는 게임의 commomserver ip주소와 port를 알려준다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetIPAddress(string pkgName)
        {
            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                string ipaddress = "";
                try
                {
                    var game = GameDao.Get(db).GetByPackageName(pkgName);
                    ipaddress = game.ServerConvertIP + ":" + game.ServerPort;
                }
                catch (Exception e)
                {
                    log.Error(e);
                }
                return Request.CreateResponse(HttpStatusCode.OK, ipaddress);
            }
        }

        /// <summary>
        /// zid 서버의 정보를 알려줍니다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage ZID()
        {
            using (var db = Database.Open())
            {
                try
                {
                    var server = GameDao.Get(db).Get(1);

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        ip = server.ServerConvertIP,
                        port = server.ServerPort
                    });
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }

        /// <summary>
        /// 게임세션의 수정된 데이터들을 입력한다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Apply(int sn)
        {
            bool observer = false;
            using (var db = Database.Open())
            using (var trans = db.BeginTransaction())
            {
                try
                {   
                    //-- 세션을 읽어오고, 세션이 없다면 새로 생성한다.
                    GameSession session = GameSessionDao.Get(db).Get(sn);
                    if (session == null)
                    {
                        session = new GameSession();
                        session.members = new List<GameSessionUser>();
                    }
                    
                    var beforeMemberList = session.members;
                    session.members = new List<GameSessionUser>();

                    //-- 쿼리값들을 세션에 적용한다. 읽어온다.
                    foreach (var pair in Request.GetQueryNameValuePairs())
                    {
                        switch (pair.Key)
                        {
                            case "Name":
                                {
                                    session.Name = pair.Value;
                                }
                                break;
                            case "Level":
                                {
                                    /*
                                     현재
                                     1. 화학
                                     2. 전기
                                     3. 전기/생명
                                     4. 생명
                                     5. 화학/생물
                                     6. 화학/전기
                                     7. 화학/전기/생명

                                     아래로 변경 요청 ( 클라가 )
                                     1. 생물
                                     2. 화학
                                     3. 전기
                                     4. 화학/생물
                                     5. 화학/전기
                                     6. 전기/생물
                                     7. 화학/전기/생물
                                     */
                                    session.Level = short.Parse(pair.Value);
                                }
                                break;
                            case "Detail":
                                {
                                    session.Detail = pair.Value;
                                }
                                break;
                            case "Game":
                                {
                                    session.game = GameDao.Get(db).Get(int.Parse(pair.Value));
                                }
                                break;
                            case "Fast":
                                {
                                    session.Fast = bool.Parse(pair.Value);
                                    break;
                                }
                            case "observer":
                                {
                                    observer = bool.Parse(pair.Value);
                                    break;
                                }
                            case "Members":
                                {
                                    //여기의 Members는 GameSessionUser의 값이 저장되어있다
                                    //GameSessionUser의 Member의 값이 아니다.(많이 헷갈림 조심)
                                    var vals = pair.Value.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries);
                                    long usn = long.Parse(vals[0]);
                                    string uname = vals[1];

                                    var user = new GameSessionUser();
                                    Device device = DeviceDao.Get.DeviceGet((int)usn);
                                    if (device != null)
                                        user.device = device;
                                    else
                                        break;
                                    user.SessionSN = session.SN;
                                    //var user = beforeMemberList != null 
                                    //    ? beforeMemberList.Where(m => m.member.SN == usn).FirstOrDefault() 
                                    //    : null;

                                    //if(user != null)
                                    //{
                                    //    beforeMemberList.Remove(user);
                                    //}
                                    //else
                                    //{
                                    //    user = new GameSessionUser();
                                    //    Device device = DeviceDao.Get.DeviceGet((int)usn);
                                    //    if (device != null)
                                    //        user.device = device;
                                    //    else
                                    //        break;
                                    //    user.SessionSN = session.SN;
                                    //    //user.member = MemberDao.Get(db).Get(usn);
                                    //}
                                    user.Name = uname;

                                    session.members.Add(user);
                                }
                                break;
                        }
                    }

                    //-- 삭제된 유저들의 디바이스를 반납한다.
                    if (beforeMemberList != null)
                    {
                        for (int i = 0; i < beforeMemberList.Count; i++)
                        {
                            if (beforeMemberList[i] != null && beforeMemberList[i].device != null)
                            {
                                beforeMemberList[i].device.State = DeviceState.Standby;
                                db.Update(beforeMemberList[i].device);
                            }
                        }
                    }

                    if(session.game == null || session.game.PackageName == "disable")
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "실행 할 수 없는 패키지명 입니다.");
                    }

                    //GameSessionManager Apply실행
                    Fims.gameSessionManager.Apply(db, trans, session, observer);

                    return Request.CreateResponse(HttpStatusCode.OK, session.ToSerializeObject());
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.BadRequest, e.Message);
                }
            }
        }

        /// <summary>
        /// 핸드폰에 게임을 실행한다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Launch(long sn)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameLaunch : " + sn);

                GameSession session = GameSessionDao.Get(db).Get(sn);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    Fims.gameSessionManager.Launch(db, session);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }

        /// <summary>
        /// 핸드폰에 게임을 실행한다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Launch(long memberSN, long sessionSN)
        {
            using (var db = Database.Open())
            {
                log.Debug($"GameLaunch for User : {memberSN}");

                GameSession session = GameSessionDao.Get(db).Get(sessionSN);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    for(int i=0; i<session.members.Count; i++)
                    {
                        if(session.members[i].member.SN == memberSN)
                        {
                            session.members[i].device.Launch(session, session.members[i]);
                            return Request.CreateResponse(HttpStatusCode.OK);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.NotFound, "유저를 찾을 수 없습니다.");
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }

        /// <summary>
        /// 게임을 시작시킨다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Start(long sn)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameStart : " + sn);

                GameSession session = GameSessionDao.Get(db).Get(sn);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    //몇초뒤 시작.
                    int time = 3000;

                    //로그아웃 금지
                    for (int i = 0; i < session.members.Count; i++)
                    {
                        session.members[i].device.Start(time);
                    }
                    //카운트 시간 만큼 대기
                    Thread.Sleep(time);
                    //게임 시작
                    Fims.gameSessionManager.Start(db, session);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }

        /// <summary>
        /// 게임을 강제로 시작시킨다.
        /// </summary>
        [HttpGet]
        public HttpResponseMessage ForcedStart(long sn)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameStart : " + sn);

                GameSession session = GameSessionDao.Get(db).Get(sn);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    //몇초뒤 시작.
                    int time = 3000;

                    //로그아웃 금지
                    for (int i = 0; i < session.members.Count; i++)
                    {
                        session.members[i].device.Start(time);
                    }
                    //카운트 시간 만큼 대기
                    Thread.Sleep(time);
                    //게임 시작
                    Fims.gameSessionManager.ForcedStart(db, session);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }

        /// <summary>
        /// 특정 맴버를 제거
        /// </summary>
        ///
        [HttpGet]
        public HttpResponseMessage RemoveMember([FromUri]long SN, [FromUri]int deviceSN)
        {
            using (var db = Database.Open())
            {
                log.Debug("RemoveMember : session " + SN +"   deviceSN " + deviceSN);
                GameSession session = GameSessionDao.Get(db).Get(SN);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    //특정 멤버 제거 + 로그아웃도 진행
                    Fims.gameSessionManager.RemoveMember(db, session, (int)deviceSN);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }

        }


        /// <summary>
        /// 게임을 강제중지한다.
        /// </summary>
        /// <param name="stopServer">1(true)일 경우 서버에서 게임을 종료하게 유도한다.</param>
        [HttpGet]
        public HttpResponseMessage Stop(long sn, [FromUri] int stopServer = 1 , int state = (int)SessionState.GameEnd)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameStop : " + sn);
                GameSession session = GameSessionDao.Get(db).Get(sn);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    for (int i = 0; i < session.members.Count; ++i)
                    {
                        Device dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                        dev.Pick = false;
                        dev.State = DeviceState.Standby;
                        //db.Update(session.members[i].device);
                    }


                    //점수처리 Stop 함수에서
                    Fims.gameSessionManager.Stop(db, session, stopServer != 0, state);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }

        }
        
        [HttpGet]
        public HttpResponseMessage dummy()
        {
            using (var db = Database.Open())
            {
                using (var trans = db.BeginTransaction())
                {
                    IList<Member> members = MemberDao.Get(db).ToList();
                    Random r = new Random();
                    for (int i = 0; i < 10; ++i)
                    {
                        Member user = members[r.Next(0, members.Count)];
                        if (user == null)
                        {
                            --i;
                            continue;
                            //return Request.CreateResponse(HttpStatusCode.BadRequest);
                        }
                        Completion num = new Completion();
                        num.Score = 100;
                        num.PlayTime = 30;
                        user.Completions.Add(num);
                        db.Update(user);
                    }
                    trans.Commit();
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
        }
        

        /// <summary>
        /// 세션을 반환한다
        /// </summary>
        [HttpGet]
        public HttpResponseMessage Return(long sn)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameInfo : " + sn);
                GameSession session = GameSessionDao.Get(db).Get(sn);
                if (session == null)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "세션이 존재하지 않습니다.");

                try
                {
                    for (int i = 0; i < session.members.Count; ++i)
                    {
                        Device dev = DeviceDao.Get.DeviceGet(session.members[i].device.SN);
                        dev.Pick = false;
                        dev.State = DeviceState.Standby;
                        //db.Update(session.members[i].device);
                    }


                    Fims.gameSessionManager.Release(db, session);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                catch (Exception e)
                {
                    log.Error(e);
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
                }
            }
        }
        //게임서버가 웹서버로 전달해주는것.
        [HttpGet]
        public HttpResponseMessage RoomEnd(long sn)
        {
            using (var db = Database.Open())
            {
                log.Debug("GameEnding : " + sn);
                var sessions = GameSessionDao.Get(db).ToActiveList();
                foreach(var session in sessions)
                {
                    if(session.RoomNum == sn)
                    {
                        Fims.gameSessionManager.RoomEnd(db, session);
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                }
                //오류처리
                return Request.CreateResponse(HttpStatusCode.OK);


            }
            
        }
    }
}
