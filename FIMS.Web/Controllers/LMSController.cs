﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using static System.Math;

using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Core;

using DocumentFormat.OpenXml.Presentation;
using DocumentFormat.OpenXml.Packaging;
using Drawing = DocumentFormat.OpenXml.Drawing;
using System.IO;
using System.Text;
using System.Drawing;

using log4net;
using System.Net.Http.Headers;
using System.Net.Http;

namespace FIMS.Web.Controllers
{
    public class LmsController : Controller
    {

        static ILog log = LogManager.GetLogger("LmsController");

        public static DateTime startTime = DateTime.MinValue;
        public static DateTime endTime = DateTime.MaxValue;
        //json형태로 변환하기 전 표 정보.. 이수증 만들려고 만듬. 
        public static List<miniCompletion> tableDatas = null;

        public ActionResult Test()
        {
            ViewBag.Title = "VR 안전교육 학생 정보";
            return View();
        }

        // GET: LMS
        public ActionResult Index()
        {
            ViewBag.Title = "VR 안전교육 학생 정보";
            return View();
        }

        public ActionResult  LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                using (var db = Database.Open())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;


                    // Getting all Customer data  
                    var customerData = MemberDao.Get(db).ToList();
                    List<miniMember> result = new List<miniMember>();
                    //Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        foreach (var x in customerData)
                        {
                            //if (x.Id == searchValue || x.Name == searchValue || x.Major == searchValue || x.Professor == searchValue)
                            if (x.Id == searchValue || x.Name == searchValue || x.Major == searchValue)
                            {
                                result.Add(miniMember.ToSerializeDetail(x));
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < customerData.Count; i++)
                        {
                            result.Add(miniMember.ToSerializeDetail(customerData[i]));
                        }
                    }


                    //Sorting  
                    List<miniMember> sortResult = new List<miniMember>();
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        var parameter = Expression.Parameter(typeof(miniMember), "c");
                        var member = Expression.PropertyOrField(parameter, sortColumn);
                        var cast = Expression.Convert(member, typeof(IComparable));
                        var lambda = Expression.Lambda<Func<miniMember, IComparable>>(cast, parameter);
                        var func = lambda.Compile();

                        sortResult = sortColumnDir == "asc"
                            ? result.OrderBy(func).ToList<miniMember>()
                            : result.OrderByDescending(func).ToList<miniMember>();
                    }

                    //total number of rows count   
                    recordsTotal = sortResult.Count();

                    if (pageSize == -1)
                        pageSize = recordsTotal;

                    //Paging   
                    var data = sortResult.Skip(skip).Take(pageSize).ToList();

                    //관리자 비로그인시 정보 일부만 보이게
                    HttpRequestMessage httpRequestMessage = HttpContext.Items["MS_HttpRequestMessage"] as HttpRequestMessage;
                    CookieHeaderValue cookie = httpRequestMessage.Headers.GetCookies("admin-login").FirstOrDefault();
                    if (cookie == null)
                    {
                        for (int i = 0; i < data.Count; ++i)
                        {
                            data[i].Name = data[i].Name[0] + "**";
                            data[i].Id = data[i].Id.Substring(0, 3) + "****";
                        }
                    }
                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
                }
               
            }
            catch (Exception)
            {
                throw;
            }

        }

        public ActionResult Completion()
        {
            ViewBag.Title = "VR 안전교육 이용 정보";
            return View();
        }
        

        public void LoadDateCompletion(DateTime start, DateTime end)
        {
            startTime = start;
            endTime = end;
        }

        public ActionResult LoadCompletion()
        {
            try
            {
                //Creating instance of DatabaseContext class
                using (var db = Database.Open())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;
                    
                    // Getting all Customer data  
                    var customerData = MemberDao.Get(db).ToList();
                    List<miniCompletion> result = new List<miniCompletion>();
                    
                    //Search
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        foreach (var x in customerData)
                        {
                            //int count = 0;
                            foreach(var z in x.Completions)
                            {
                                if (z == null || z.Date < startTime || endTime < z.Date)
                                    continue;
                                
                                if (x.Id == searchValue ||x.Name == searchValue || x.Major == searchValue || x.Professor == searchValue)
                                {
                                    //z.Ord = count++;
                                    result.Add(miniCompletion.ToSerializeDetail(x, z));
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < customerData.Count; i++)
                        {
                            //int count = 0;
                            foreach (var z in customerData[i].Completions)
                            {
                                if (z == null || z.Date < startTime || endTime < z.Date)
                                    continue;
                                //z.Ord = count++;
                                result.Add(miniCompletion.ToSerializeDetail(customerData[i], z));
                            }
                        }
                    }
                    List<miniCompletion> sortresult = new List<miniCompletion>();
                    //Sorting   
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        
                        var parameter = Expression.Parameter(typeof(miniCompletion), "c");
                        var member = Expression.PropertyOrField(parameter, sortColumn);
                        var cast = Expression.Convert(member, typeof(IComparable));
                        var lambda = Expression.Lambda<Func<miniCompletion, IComparable>>(cast, parameter);
                        var func = lambda.Compile();
                        sortresult = sortColumnDir == "asc" 
                            ? result.OrderBy(func).ToList<miniCompletion>() 
                            : result.OrderByDescending(func).ToList<miniCompletion>();
                    }



                    //total number of rows count   
                    recordsTotal = result.Count();

                    if (pageSize == -1)
                        pageSize = recordsTotal;

                    //Paging   
                    var data = sortresult.Skip(skip).Take(pageSize).ToList();
                    //var data2 = result.Skip(skip).Take(pageSize).ToList();
                    tableDatas = result.Skip(skip).Take(pageSize).ToList();

                    //관리자 비로그인시 정보 일부만 보이게
                    HttpRequestMessage httpRequestMessage = HttpContext.Items["MS_HttpRequestMessage"] as HttpRequestMessage;
                    CookieHeaderValue cookie = httpRequestMessage.Headers.GetCookies("admin-login").FirstOrDefault();
                    if (cookie == null)
                    {
                        for (int i=0; i<data.Count; ++i)
                        {
                            data[i].Name = data[i].Name[0] + "**";
                            data[i].Id = data[i].Id.Substring(0, 3) + "****";
                        }
                    }
                    List<Object> finalData = new List<object>();
                    for(int i = 0; i<data.Count; ++i)
                    {
                        finalData.Add(data[i]);
                    }

                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = finalData });
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        public ActionResult Stats()
        {
            ViewBag.Title = "VR 안전교육 통계";
            return View();
        }

        public ActionResult LoadStats()
        {
            
            try
            {
                //Creating instance of DatabaseContext class
                using (var db = Database.Open())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault();
                    var start = Request.Form.GetValues("start").FirstOrDefault();
                    var length = Request.Form.GetValues("length").FirstOrDefault();
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // Getting all Customer data  
                    var customerData = MemberDao.Get(db).ToList();
                    List<miniStats> result = new List<miniStats>();

                    //Search
                    searchValue = searchValue.Trim();
                    HttpRequestMessage httpRequestMessage = HttpContext.Items["MS_HttpRequestMessage"] as HttpRequestMessage;
                    CookieHeaderValue cookie = httpRequestMessage.Headers.GetCookies("admin-login").FirstOrDefault();
                    if (!string.IsNullOrEmpty(searchValue) && (searchValue.Count() == 21) && cookie != null)
                    {
                        DateTime startTime = Convert.ToDateTime(searchValue.Substring(0, 10));
                        DateTime endTime = Convert.ToDateTime(searchValue.Substring(11, 10));
                        
                        foreach (var x in customerData)
                        {
                            bool bk = true;
                            foreach (var y in result)
                            {
                                if (y.Major == x.Major)
                                {
                                    y.Total++;
                                    if (x.Completions.Count > 0)
                                    {
                                        foreach(var z in x.Completions)
                                        {
                                            if(z.Date > startTime && z.Date < endTime)
                                            {
                                                y.Completion++;
                                                break;
                                            }
                                        }
                                    }
                                    bk = false;
                                    break;
                                }
                            }
                            if (bk)
                            {
                                miniStats stats = new miniStats();
                                stats.Major = x.Major;
                                stats.Total = 1;
                                stats.Completion = 0;
                                if (x.Completions.Count > 0)
                                {
                                    stats.Completion++;
                                }
                                result.Add(stats);
                            }
                        }
                    }
                    foreach (var x in result)
                    {
                        x.Percent = ((float)x.Completion / x.Total * 100).ToString("n2") + "%";
                    }

                    
                    List<miniStats> sortresult = new List<miniStats>();
                    //Sorting   
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {

                        var parameter = Expression.Parameter(typeof(miniStats), "c");
                        var member = Expression.PropertyOrField(parameter, sortColumn);
                        var cast = Expression.Convert(member, typeof(IComparable));
                        var lambda = Expression.Lambda<Func<miniStats, IComparable>>(cast, parameter);
                        var func = lambda.Compile();
                        sortresult = sortColumnDir == "asc"
                            ? result.OrderBy(func).ToList<miniStats>()
                            : result.OrderByDescending(func).ToList<miniStats>();
                    }



                    //total number of rows count   
                    recordsTotal = result.Count();
                    
                    //Paging   
                    var data = sortresult.Skip(skip).Take(recordsTotal).ToList();
                    //var data = result.Skip(skip).Take(pageSize).ToList();

                    List<Object> finalData = new List<object>();
                    for (int i = 0; i < data.Count; ++i)
                    {
                        finalData.Add(data[i]);
                    }

                    //Returning Json Data  
                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = finalData });
                }

            }
            catch (Exception)
            {
                throw;
            }

        }
        
        public string Certificate()
        {
            //관리자 로그인 필요
            HttpRequestMessage httpRequestMessage = HttpContext.Items["MS_HttpRequestMessage"] as HttpRequestMessage;
            CookieHeaderValue cookie = httpRequestMessage.Headers.GetCookies("admin-login").FirstOrDefault();
            if (cookie == null)
            {
                return ("NoAdmin");
            }

            if (tableDatas != null)
            {
                //ReadSlide2();
                PowerPointHelper helper = new PowerPointHelper();
                string filePath = Server.MapPath("~/file/") + "certificate.pptx";
                string new_fileName = DateTime.Now.ToString("yyyy년MM월dd일HH시mm분_") + "수료증.pptx";
                string new_filePath = Server.MapPath("~/file/") + new_fileName;
                PowerPointHelper.data = tableDatas;
                PowerPointHelper.SaveAs(filePath, new_filePath);
                for (int i = 0; i < tableDatas.Count-1; ++i)
                {
                    PowerPointHelper.Copy6(filePath, 1, new_filePath);
                }
                
                PowerPointHelper.replaceTextInSlide(new_filePath);
                return new_fileName;
            }
            return "NO";
        }

        public void ReadSlide2()
        {
            int slideCount = 0;
            string filePath = @"~/file/certificate.pptx";
            string filePath2 = @"C:/Users/leon/Desktop/YunKyu/FIMS_v0.4/FIMS/FIMS.Web/file/certificate.pptx";
            

            using (var doc = PresentationDocument.Open(filePath2, false))
            {
                PresentationPart presentationPart = doc.PresentationPart;
                slideCount = presentationPart.SlideParts.Count();
            }
        }
        /*
        public void ReadSlide()
        {
            //miniCompletion 맴버변수 이름
            String[] data = { "Major", "Id", "Name"};
            String date = null;
            String Today = DateTime.Now.ToString("yyyy-MM-dd");
            //pptx 읽어오기
            string filePath = @"~/file/certificate.pptx";
            Application pptApplication = new Application();
            Presentation createPPTX = pptApplication.Presentations.Add(MsoTriState.msoTrue);
            Slides newSlides = createPPTX.Slides;
            for (int i = 0; i < tableDatas.Count; ++i)
            {
                newSlides.InsertFromFile(filePath, 0, 1);
            }
            
            for (int j = 1; j <= tableDatas.Count; ++j)
            {
                for (int i = 1; i <= newSlides[j].Shapes.Count; ++i)
                {
                    var shape = (Microsoft.Office.Interop.PowerPoint.Shape)newSlides[j].Shapes[i];
                    if (shape.HasTextFrame == MsoTriState.msoTrue)
                    {
                        TextRange textRange;
                        string text;
                        if (shape.TextFrame.HasText == MsoTriState.msoTrue)
                        {
                            textRange = shape.TextFrame.TextRange;
                            text = textRange.Text;
                            if(tableDatas[j-1].Major != null)
                                text = text.Replace("Major", tableDatas[j - 1].Major);
                            if (tableDatas[j - 1].Id != null)
                                text = text.Replace("Id", tableDatas[j - 1].Id);
                            if (tableDatas[j - 1].Name != null)
                                text = text.Replace("Name", tableDatas[j - 1].Name);
                            if (tableDatas[j - 1].Date != null)
                                text = text.Replace("Date", tableDatas[j - 1].Date);
                            text = text.Replace("Today", Today);
                            shape.TextFrame.TextRange.Text = text;
                            newSlides[j].Shapes[i].TextFrame.TextRange.Text = text;
                        }
                    }
                }
            }
            //string filename = @"D:\수료증\" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "수료증.pptx";
            createPPTX.SaveAs(@"~/file/"+ DateTime.Now.ToString("yyyy -MM-dd-HH-mm-ss") + "수료증.pptx", PpSaveAsFileType.ppSaveAsDefault, MsoTriState.msoTrue);
            //createPPTX.Close();
            pptApplication.Quit();
            /*
            {
                FileInfo fileInfo = new FileInfo(filename);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileInfo.Name);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.Flush();
                Response.TransmitFile(fileInfo.FullName);
                Response.End();
            }
            */
            /*
            Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(filePath, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);
            
            //pptx 슬라이드1.. 수료증 서식
            var Slide = presentation.Slides[1];

            //수료증 만들기
            Application pptApplication = new Application(); // 어플리케이션 생성

            Slides slides;      // 슬라이드 묶음(파일)
            _Slide slide;       // 낱장 슬라이드

            // 프레젠테이션 파일 생성
            Presentation pptPresentation = pptApplication.Presentations.Add(MsoTriState.msoTrue);
            CustomLayout customLayout = pptPresentation.SlideMaster.CustomLayouts[PpSlideLayout.ppLayoutText];

            // 새 슬라이드 생성
            slides = pptPresentation.Slides;
            //slide = slides.AddSlide(1, customLayout);
            slide = presentation.Slides[1];

            //슬라이드1 읽기
            //foreach (var item in presentation.Slides[1].Shapes)
            for(int i = 0; i<slide.Shapes.Count; ++i)
            {
                var shape = (Microsoft.Office.Interop.PowerPoint.Shape)slide.Shapes[i];
                if (shape.HasTextFrame == MsoTriState.msoTrue)
                {
                    if (shape.TextFrame.HasText == MsoTriState.msoTrue)
                    {
                        var textRange = shape.TextFrame.TextRange;
                        var text = textRange.Text;

                        text = text.Replace("Major", tableDatas[0].Major);
                        text = text.Replace("Id", tableDatas[0].Id);
                        text = text.Replace("Name", tableDatas[0].Name);

                        DateTime dateTime = Convert.ToDateTime(tableDatas[0].Date);
                        if (dateTime.Month < 9) {
                            date = $"{dateTime.Year}.01.01 ~ {dateTime.Year}.06.30";
                        }
                        else
                        {
                            date = $"{dateTime.Year}.07.01 ~ {dateTime.Year}.12.30";
                        }
                        text = text.Replace("Date", date);
                        text = text.Replace("Today", Today);
                        shape.TextFrame.TextRange.Text = text;
                        //slide.Shapes[i] = shape;
                    }
                }
            }
            
            pptPresentation.SaveAs(@"C:\Users\leon\Desktop\YunKyu\ggg.pptx", PpSaveAsFileType.ppSaveAsDefault, MsoTriState.msoTrue);
            */
            //pptPresentation.Close();
            //pptApplication.Quit();
        //}
        
    }
}