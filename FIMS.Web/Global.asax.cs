﻿using System;
using System.IO;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FIMS.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            FantafoConfig.Begin(this);
            {
                /* 웹서버 게시하는 방법
                 * https://docs.microsoft.com/ko-kr/visualstudio/deployment/tutorial-import-publish-settings-iis?view=vs-2019
                 */
                GlobalConfiguration.Configure(WebApiConfig.Register);
                FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
                RouteConfig.RegisterRoutes(RouteTable.Routes);
                BundleConfig.RegisterBundles(BundleTable.Bundles);
                HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
            }
            FantafoConfig.BeginEnd(this);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            FantafoConfig.Close(this);
        }
    }
}
